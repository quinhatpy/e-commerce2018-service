<?php

namespace App\Providers;

use App\Policies\AttributePolicy;
use App\Policies\BannerPolicy;
use App\Policies\ArticlePolicy;
use App\Policies\BranchPolicy;
use App\Policies\BrandPolicy;
use App\Policies\CategoryBannerPolicy;
use App\Policies\CategoryPolicy;
use App\Policies\CustomerPolicy;
use App\Policies\DistrictPolicy;
use App\Policies\GalleryPolicy;
use App\Policies\MenuPolicy;
use App\Policies\OrderPolicy;
use App\Policies\ProductPolicy;
use App\Policies\ProvincePolicy;
use App\Policies\RolePolicy;
use App\Policies\UserPolicy;
use App\Policies\WardPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('user.index', UserPolicy::class . '@index');
        Gate::define('user.show', UserPolicy::class . '@show');
        Gate::define('user.create', UserPolicy::class . '@create');
        Gate::define('user.update', UserPolicy::class . '@update');
        Gate::define('user.delete', UserPolicy::class . '@delete');

        Gate::define('role.index', RolePolicy::class . '@index');
        Gate::define('role.show', RolePolicy::class . '@show');
        Gate::define('role.create', RolePolicy::class . '@create');
        Gate::define('role.update', RolePolicy::class . '@update');
        Gate::define('role.delete', RolePolicy::class . '@delete');

        Gate::define('customer.index', CustomerPolicy::class . '@index');
        Gate::define('customer.show', CustomerPolicy::class . '@show');
        Gate::define('customer.create', CustomerPolicy::class . '@create');
        Gate::define('customer.update', CustomerPolicy::class . '@update');
        Gate::define('customer.delete', CustomerPolicy::class . '@delete');

        Gate::define('category.index', CategoryPolicy::class . '@index');
        Gate::define('category.show', CategoryPolicy::class . '@show');
        Gate::define('category.create', CategoryPolicy::class . '@create');
        Gate::define('category.update', CategoryPolicy::class . '@update');
        Gate::define('category.delete', CategoryPolicy::class . '@delete');

        Gate::define('category_banner.index', CategoryBannerPolicy::class . '@index');
        Gate::define('category_banner.show', CategoryBannerPolicy::class . '@show');
        Gate::define('category_banner.create', CategoryBannerPolicy::class . '@create');
        Gate::define('category_banner.update', CategoryBannerPolicy::class . '@update');
        Gate::define('category_banner.delete', CategoryBannerPolicy::class . '@delete');

        Gate::define('attribute.index', AttributePolicy::class . '@index');
        Gate::define('attribute.show', AttributePolicy::class . '@show');
        Gate::define('attribute.create', AttributePolicy::class . '@create');
        Gate::define('attribute.update', AttributePolicy::class . '@update');
        Gate::define('attribute.delete', AttributePolicy::class . '@delete');

        Gate::define('banner.index', BannerPolicy::class . '@index');
        Gate::define('banner.show', BannerPolicy::class . '@show');
        Gate::define('banner.create', BannerPolicy::class . '@create');
        Gate::define('banner.update', BannerPolicy::class . '@update');
        Gate::define('banner.delete', BannerPolicy::class . '@delete');

        Gate::define('brand.index', BrandPolicy::class . '@index');
        Gate::define('brand.show', BrandPolicy::class . '@show');
        Gate::define('brand.create', BrandPolicy::class . '@create');
        Gate::define('brand.update', BrandPolicy::class . '@update');
        Gate::define('brand.delete', BrandPolicy::class . '@delete');

        Gate::define('province.index', ProvincePolicy::class . '@index');
        Gate::define('province.show', ProvincePolicy::class . '@show');
        Gate::define('province.create', ProvincePolicy::class . '@create');
        Gate::define('province.update', ProvincePolicy::class . '@update');
        Gate::define('province.delete', ProvincePolicy::class . '@delete');

        Gate::define('district.index', DistrictPolicy::class . '@index');
        Gate::define('district.show', DistrictPolicy::class . '@show');
        Gate::define('district.create', DistrictPolicy::class . '@create');
        Gate::define('district.update', DistrictPolicy::class . '@update');
        Gate::define('district.delete', DistrictPolicy::class . '@delete');

        Gate::define('ward.index', WardPolicy::class . '@index');
        Gate::define('ward.show', WardPolicy::class . '@show');
        Gate::define('ward.create', WardPolicy::class . '@create');
        Gate::define('ward.update', WardPolicy::class . '@update');
        Gate::define('ward.delete', WardPolicy::class . '@delete');

        Gate::define('branch.index', BranchPolicy::class . '@index');
        Gate::define('branch.show', BranchPolicy::class . '@show');
        Gate::define('branch.create', BranchPolicy::class . '@create');
        Gate::define('branch.update', BranchPolicy::class . '@update');
        Gate::define('branch.delete', BranchPolicy::class . '@delete');

        Gate::define('menu.index', MenuPolicy::class . '@index');
        Gate::define('menu.show', MenuPolicy::class . '@show');
        Gate::define('menu.create', MenuPolicy::class . '@create');
        Gate::define('menu.update', MenuPolicy::class . '@update');
        Gate::define('menu.delete', MenuPolicy::class . '@delete');

        Gate::define('gallery.index', GalleryPolicy::class . '@index');
        Gate::define('gallery.show', GalleryPolicy::class . '@show');
        Gate::define('gallery.create', GalleryPolicy::class . '@create');
        Gate::define('gallery.update', GalleryPolicy::class . '@update');
        Gate::define('gallery.delete', GalleryPolicy::class . '@delete');

        Gate::define('article.index', ArticlePolicy::class . '@index');
        Gate::define('article.show', ArticlePolicy::class . '@show');
        Gate::define('article.create', ArticlePolicy::class . '@create');
        Gate::define('article.update', ArticlePolicy::class . '@update');
        Gate::define('article.delete', ArticlePolicy::class . '@delete');

        Gate::define('product.index', ProductPolicy::class . '@index');
        Gate::define('product.show', ProductPolicy::class . '@show');
        Gate::define('product.create', ProductPolicy::class . '@create');
        Gate::define('product.update', ProductPolicy::class . '@update');
        Gate::define('product.delete', ProductPolicy::class . '@delete');

        Gate::define('order.index', OrderPolicy::class . '@index');
        Gate::define('order.show', OrderPolicy::class . '@show');
        Gate::define('order.create', OrderPolicy::class . '@create');
        Gate::define('order.update', OrderPolicy::class . '@update');
        Gate::define('order.delete', OrderPolicy::class . '@delete');
    }
}
