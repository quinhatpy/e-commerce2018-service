<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\CategoryBanner;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CategoryBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'nullable|integer|exists:categories,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $offset = $request->offset ?? 0;
        $limit = $request->limit ?? PHP_INT_MAX;
        $sort = $request->sort_by ?? '';
        $orderArray = [];

        if ($sort) {
            $sortArray = explode(',', $sort);

            foreach ($sortArray as $sortItem) {
                if (strpos($sortItem, '-') === 0) {
                    $orderArray[substr($sortItem, 1)] = 'DESC';
                } else {
                    $orderArray[trim($sortItem)] = 'ASC';
                }
            }
        }

        $categoryBannerQuery = CategoryBanner::with(['category' => function ($query) {
            $query->select('id', 'name');
        }]);

        if (!empty($request->category_id)) {
            $categoryBanner = $categoryBannerQuery->whereCategoryId($request->category_id)->offset($offset)->limit($limit);
            $totalRows = CategoryBanner::whereCategoryId($request->category_id)->get()->count();
        } else {
            $categoryBanner = $categoryBannerQuery->offset($offset)->limit($limit);
            $totalRows = CategoryBanner::get()->count();
        }

        foreach ($orderArray as $orderColumn => $orderValue) {
            $categoryBanner->orderBy($orderColumn, $orderValue);
        }

        $categoryBanner = $categoryBanner->get()->makeHidden('category_id');

        return sendResponse([
            'totalRows' => $totalRows,
            'category_banners' => $categoryBanner
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'image' => 'required|file|image',
//            'link' => 'nullable|url',
            'ordinal' => 'nullable|integer',
            'is_show' => ['nullable', 'integer', Rule::in([0, 1])],
            'is_main' => ['nullable', 'integer', Rule::in([0, 1])],
            'category_id' => 'required|integer|exists:categories,id'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $imageFile = $request->file('image');

            if ($request->hasFile('image')) {
                $name = time() . '-' . $data['category_id'] . '-' . $imageFile->getClientOriginalName();
                $data['image'] = $name;
            }

            if (empty($data['ordinal'])) {
                $ordinalMax = CategoryBanner::whereCategoryId($data['category_id'])->max('ordinal');
                $ordinalMax = $ordinalMax ?? 0;
                $data['ordinal'] = $ordinalMax + 1;
            }

            $categoryBanner = CategoryBanner::create($data);

            if ($categoryBanner && $request->hasFile('image')) {
                $imageFile->move('public/images/categories/banners', $data['image']);
            }

            $categoryBannerCreated = CategoryBanner::with(['category' => function ($query) {
                $query->select('id', 'name');
            }])->find($categoryBanner->id)->makeHidden('category_id');

            return sendResponse($categoryBannerCreated, 'Create category banner successfully', 201);
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categoryBanner = CategoryBanner::with(['category' => function ($query) {
            $query->select('id', 'name');
        }])->find($id);

        if (!$categoryBanner) return sendError('Not found');

        return sendResponse($categoryBanner->makeHidden('category_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categoryBanner = CategoryBanner::find($id);
        if (!$categoryBanner) return sendError('Category banner not found', [], 400);

        $data = $request->all();

        $validator = Validator::make($data, [
            'image' => 'nullable|file|image',
//            'link' => 'nullable|url',
            'ordinal' => 'nullable|integer',
            'is_show' => ['nullable', 'integer', Rule::in([0, 1])],
            'is_main' => ['nullable', 'integer', Rule::in([0, 1])],
            'category_id' => 'required|integer|exists:categories,id'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $imageFile = $request->file('image');
            $imageCurrent = '';

            if ($request->hasFile('image')) {
                $imageCurrent = $categoryBanner->getOriginal('image');
                $name = time() . '-' . $data['category_id'] . '-' . $imageFile->getClientOriginalName();

                $data['image'] = $name;
            }

            if (array_key_exists('ordinal', $data) && is_null($data['ordinal'])) {
                $ordinalMax = CategoryBanner::whereCategoryId($data['category_id'])->whereNotIn('id', [$id])->max('ordinal');
                $ordinalMax = $ordinalMax ?? 0;
                $data['ordinal'] = $ordinalMax + 1;
            }

            $result = $categoryBanner->update($data);

            if ($result && $request->hasFile('image')) {
                if (file_exists(public_path() . '/images/categories/banners/' . $imageCurrent)) {
                    File::delete(public_path() . '/images/categories/banners/' . $imageCurrent);
                }

                $imageFile->move('public/images/categories/banners', $data['image']);
            }

            $categoryBannerUpdated = CategoryBanner::with(['category' => function ($query) {
                $query->select('id', 'name');
            }])->find($categoryBanner->id)->makeHidden('category_id');

            return sendResponse($categoryBannerUpdated, 'Update category banner successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categoryBanner = CategoryBanner::find($id);
        if (!$categoryBanner) return sendError('Category banner not found', [], 400);

        try {
            $imageCurrent = $categoryBanner->getOriginal('image');

            $result = $categoryBanner->delete();

            if ($result) {
                if (file_exists(public_path() . '/images/categories/banners/' . $imageCurrent)) {
                    File::delete(public_path() . '/images/categories/banners/' . $imageCurrent);
                }
            }

            return sendResponse([], 'Delete category banner successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    public function getCategoryBannersInHome(Request $request)
    {
        $parentCategories = explode(',', $request->parent_categories);
        $categories = Category::select('id', 'parent_id')->whereIsShow(1)->get();

        if ($parentCategories[0] === "") $parentCategories = [];

        foreach ($parentCategories as $parentCategory) {

            $children[$parentCategory] = getAllChildren($categories, $parentCategory);
        }

        $data = [];

        foreach ($children as $categoryId => $child) {
            $categoryIdArray = [];

            foreach ($child as $item) {
                $categoryIdArray[] = $item->id;
            }


            $data[$categoryId] = CategoryBanner::select('id', 'image', 'link')->whereIn('category_id', $categoryIdArray)
                ->whereIsShow(1)->whereIsMain(1)->orderBy('ordinal')->first();
        }
        return sendResponse($data);
    }

    public function getCategoryBannerBySlug(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $limit = $request->limit ?? PHP_INT_MAX;

        $category = Category::select('id')->whereSlug($request->slug)->first();
        $categories = Category::select('id', 'parent_id')->whereIsShow(1)->get();

        $categoryChildren = getAllChildren($categories, $category->id);
        $categoryIdArray = [$category->id];

        foreach ($categoryChildren as $item) {
            $categoryIdArray[] = $item->id;
        }

        $categoryBanner = CategoryBanner::select('id', 'image', 'link')->whereIn('category_id', $categoryIdArray)
            ->whereIsShow(1)->orderBy('ordinal')->limit($limit)->get();

        return sendResponse($categoryBanner);
    }

}
