<?php

namespace App\Http\Controllers;

use App\Models\District;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'province_id' => 'nullable|integer|exists:provinces,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $offset = $request->offset ?? 0;
        $limit = $request->limit ?? PHP_INT_MAX;
        $sort = $request->sort_by ?? '';
        $orderArray = [];

        if ($sort) {
            $sortArray = explode(',', $sort);

            foreach ($sortArray as $sortItem) {
                if (strpos($sortItem, '-') === 0) {
                    $orderArray[substr($sortItem, 1)] = 'DESC';
                } else {
                    $orderArray[trim($sortItem)] = 'ASC';
                }
            }
        }

        $districtsQuery = District::with(['province' => function ($query) {
            $query->select('id', 'name', 'type');
        }]);
        if (!empty($request->province_id)) {
            $districts = $districtsQuery->whereProvinceId($request->province_id)->offset($offset)->limit($limit);
            $totalRows = District::whereProvinceId($request->province_id)->get()->count();
        } else {
            $districts = $districtsQuery->offset($offset)->limit($limit);
            $totalRows = District::get()->count();
        }

        foreach ($orderArray as $orderColumn => $orderValue) {
            $districts->orderBy($orderColumn, $orderValue);
        }

        $districts = $districts->get()->makeHidden('province_id');

        return sendResponse([
            'totalRows' => $totalRows,
            'districts' => $districts
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required',
            'type' => 'required',
            'province_id' => 'required|integer|exists:provinces,id'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $district = District::create($data);
            $districtCreated = District::with(['province' => function ($query) {
                $query->select('id', 'name', 'type');
            }])->find($district->id)->makeHidden('province_id');

            return sendResponse($districtCreated, 'Create district successfully', 201);
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $district = District::with(['province' => function($query) {
            $query->select('id', 'name', 'type');
        }])->find($id)->makeHidden('province_id');
        if (!$district) return sendError('Not found');
        return sendResponse($district);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $district = District::find($id);
        if (!$district) return sendError('District not found', [], 400);

        $data = $request->all();

        $validator = Validator::make($data, [
            'province_id' => 'nullable|integer|exists:provinces,id'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $district->update($data);
            $districtUpdated = District::with(['province' => function($query) {
                $query->select('id', 'name', 'type');
            }])->find($district->id)->makeHidden('province_id');

            return sendResponse($districtUpdated, 'Update district successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $district = District::find($id);
        if (!$district) return sendError('District not found', [], 400);

        try {
            $district->delete();

            return sendResponse([], 'Delete district successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }
}
