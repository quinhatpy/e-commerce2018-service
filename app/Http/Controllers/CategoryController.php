<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $offset = $request->offset ?? 0;
        $limit = $request->limit ?? PHP_INT_MAX;
        $keyword = $request->keyword ?? '';
        $sort = $request->sort_by ?? '';
        $orderArray = [];

        if ($sort) {
            $sortArray = explode(',', $sort);

            foreach ($sortArray as $sortItem) {
                if (strpos($sortItem, '-') === 0) {
                    $orderArray[substr($sortItem, 1)] = 'DESC';
                } else {
                    $orderArray[trim($sortItem)] = 'ASC';
                }
            }
        }

        $category = Category::with(['parent' => function ($query) {
            $query->select('id', 'name');
        }])->where('name', 'LIKE', "%$keyword%")->offset($offset)->limit($limit);

        foreach ($orderArray as $orderColumn => $orderValue) {
            $category->orderBy($orderColumn, $orderValue);
        }

        $category = $category->get()->makeHidden('parent_id');

        $totalRows = Category::where('name', 'LIKE', "%$keyword%")->get()->count();

        return sendResponse([
            'totalRows' => $totalRows,
            'categories' => $category
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required',
            'slug' => 'required|max:150|unique:categories,slug',
            'icon' => 'nullable|file|image',
            'parent_id' => 'nullable|exists:categories,id',
            'is_show' => ['nullable', 'integer', Rule::in([0, 1])],
            'seo_title' => 'nullable|max:60'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $iconFile = $request->file('icon');

            if ($request->hasFile('icon')) {
                $name = time() . '-' . str_slug($data['name']) . '-' . $iconFile->getClientOriginalName();
                $data['icon'] = $name;
            }

            $category = Category::create($data);

            if ($category && $request->hasFile('icon')) {
                $iconFile->move('public/images/categories/icons', $data['icon']);
            }

            $categoryCreated = Category::with(['parent' => function ($query) {
                $query->select('id', 'name');
            }])->find($category->id)->makeHidden('parent_id');

            return sendResponse($categoryCreated, 'Create category successfully', 201);
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::with(['parent' => function ($query) {
            $query->select('id', 'name');
        }])->find($id);
        if (!$category) return sendError('Not found');
        return sendResponse($category->makeHidden('parent_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        if (!$category) return sendError('Category not found', [], 400);

        $data = $request->all();

        $validator = Validator::make($data, [
            'slug' => 'nullable|max:150|unique:categories,slug,' . $category->id,
            'icon' => 'nullable|file|image',
            'parent_id' => 'nullable|exists:categories,id',
            'is_show' => ['nullable', 'integer', Rule::in([0, 1])],
            'seo_title' => 'nullable|max:60'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $iconFile = $request->file('icon');
            $imageCurrent = '';

            if ($request->hasFile('icon')) {
                $imageCurrent = $category->getOriginal('icon');
                $name = time() . '-' . str_slug($data['name']) . '-' . $iconFile->getClientOriginalName();

                $data['icon'] = $name;
            }

            $result = $category->update($data);

            if ($result && $request->hasFile('icon')) {
                if (file_exists(public_path() . '/images/categories/icons/' . $imageCurrent)) {
                    File::delete(public_path() . '/images/categories/icons/' . $imageCurrent);
                }

                $iconFile->move('public/images/categories/icons', $data['icon']);
            }

            $categoryUpdated = Category::with(['parent' => function ($query) {
                $query->select('id', 'name');
            }])->find($category->id)->makeHidden('parent_id');

            return sendResponse($categoryUpdated, 'Update category successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        if (!$category) return sendError('Category not found', [], 400);

        try {
            $imageCurrent = $category->getOriginal('icon');

            $result = $category->delete();

            if ($result) {
                if (file_exists(public_path() . '/images/categories/icons/' . $imageCurrent)) {
                    File::delete(public_path() . '/images/categories/icons/' . $imageCurrent);
                }
            }

            return sendResponse([], 'Delete category successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    public function getCategories()
    {
        $categories = Category::select(['id', 'name', 'slug', 'icon', 'parent_id'])->whereIsShow(1)->orderBy('updated_at')->get();
        return sendResponse($categories);
    }

    public function getCategoryBySlug(Request $request)
    {
        $category = Category::select(['id', 'name', 'slug', 'icon', 'parent_id'])->whereSlug($request->slug)->whereIsShow(1)->first();
        return sendResponse($category);
    }

    public function getBreadcrumb(Request $request)
    {
        $category = Category::select(['id', 'name', 'parent_id', 'seo_title', 'seo_description'])->whereSlug($request->slug)->first();
        $categories = Category::select(['id', 'name', 'slug', 'parent_id'])->whereIsShow(1)->get();

        $categoryParents = getCategoryParents($categories, $category->parent_id);
        $categoryParents = array_reverse($categoryParents);
        $categoryParents[] = $category;

        return sendResponse($categoryParents);
    }

}
