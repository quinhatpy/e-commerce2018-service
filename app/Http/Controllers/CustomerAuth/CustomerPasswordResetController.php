<?php

namespace App\Http\Controllers\CustomerAuth;

use App\Models\Customer;
use App\Models\CustomerPasswordReset;
use App\Notifications\CustomerPasswordResetRequest;
use App\Notifications\CustomerPasswordResetSuccess;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CustomerPasswordResetController extends Controller
{
    /**
     * Create token password reset
     *
     * @param  [string] email
     * @return [string] message
     */
    public function create(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'email' => 'required|string|email',
        ]);

        if ($validate->fails())
            return sendError('Validation Error', $validate->errors(), 422);

        $customer = Customer::where('email', $request->email)->first();
        if (!$customer)
            return sendError("We can't find a user with that e-mail address.", [], 404);

        $passwordReset = CustomerPasswordReset::updateOrCreate(
            ['email' => $customer->email],
            [
                'email' => $customer->email,
                'token' => str_random(60)
            ]
        );

        if ($customer && $passwordReset)
            $customer->notify(new CustomerPasswordResetRequest($passwordReset->token));

        return sendResponse([], 'We have e-mailed your password reset link!');
    }

    /**
     * Find token password reset
     *
     * @param  [string] $token
     * @return [string] message
     * @return [json] passwordReset object
     */
    public function find($token)
    {
        $passwordReset = CustomerPasswordReset::where('token', $token)->first();

        if (!$passwordReset)
            return sendError('This password reset token is invalid.', [], 404);

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return sendError('This password reset token is invalid.', [], 404);
        }

        return sendResponse($passwordReset, 'Find user by token successfully');
    }

    /**
     * Reset password
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] token
     * @return [string] message
     * @return [json] user object
     */
    public function reset(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string|confirmed|min:6',
            'token' => 'required|string'
        ]);

        if ($validate->fails()) return sendError('Validate Error', $validate->errors(), 422);

        $passwordReset = CustomerPasswordReset::where([
            ['token', $request->token],
            ['email', $request->email]
        ])->first();

        if (!$passwordReset)
            return sendError('This password reset token is invalid.', [], 404);

        $customer = Customer::where('email', $passwordReset->email)->first();

        if (!$customer)
            return sendError("We can't find a user with that e-mail address.", [], 404);

        $customer->password = bcrypt($request->password);
        $customer->save();

        $passwordReset->delete();

        $customer->notify(new CustomerPasswordResetSuccess());

        return sendResponse($customer, 'Reset password successfully!');
    }
}
