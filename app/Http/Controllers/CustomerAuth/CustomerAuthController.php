<?php

namespace App\Http\Controllers\CustomerAuth;

use App\Models\Customer;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class CustomerAuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:customer', ['except' => ['login']]);
    }
    
    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string',
//            'remember' => 'boolean'
        ]);
        if ($validate->fails())
            return sendError('Validation Error', $validate->errors(), 422);


        $credentials = request(['email', 'password']);

        if (!$token = auth('customer')->attempt($credentials)) {
            return sendError('Tai khoan hoac mat khau khong chinh xac', [], 401);
        }

        return sendResponse([
            'token' => $token,
            'token_type' => 'bearer',
//            'expires' => auth('customer')->factory()->getTTL() *1,
        ], 'login success');

    }

    /**
     * Logout user (Revoke the token)
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse [string] message
     */
    public function logout()
    {
        auth('customer')->logout();
        return sendResponse([], 'logout success');
    }

    /**
     * Get the authenticated User
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse [json] user object
     */
    public function getUser()
    {
        $id= auth('customer')->user()->id;
        $user = Customer::select('id','name', 'email', 'address', 'phone')->whereId($id)->first();

        return sendResponse($user, 'Get customer successfully');
    }
}
