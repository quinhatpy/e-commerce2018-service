<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $offset = $request->offset ?? 0;
        $limit = $request->limit ?? PHP_INT_MAX;
        $keyword = $request->keyword ?? '';
        $sort = $request->sort_by ?? '';
        $orderArray = [];

        if ($sort) {
            $sortArray = explode(',', $sort);

            foreach ($sortArray as $sortItem) {
                if (strpos($sortItem, '-') === 0) {
                    $orderArray[substr($sortItem, 1)] = 'DESC';
                } else {
                    $orderArray[trim($sortItem)] = 'ASC';
                }
            }
        }


        $banners = Banner::where('position', 'LIKE', "%$keyword%")->offset($offset)->limit($limit);
        $totalRows = Banner::where('position', 'LIKE', "%$keyword%")->get()->count();

        foreach ($orderArray as $orderColumn => $orderValue) {
            $banners->orderBy($orderColumn, $orderValue);
        }

        $banners = $banners->get();

        return sendResponse([
            'totalRows' => $totalRows,
            'banners' => $banners
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'image' => 'required|file|image',
            'position' => 'required',
//            'link' => 'nullable|url',
            'ordinal' => 'nullable|integer',
            'is_show' => ['nullable', 'integer', Rule::in([0, 1])],
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $imageFile = $request->file('image');

            if ($request->hasFile('image')) {
                $name = time() . '-' . $data['position'] . '-' . $imageFile->getClientOriginalName();
                $data['image'] = $name;
            }

            if (empty($data['ordinal'])) {
                $ordinalMax = Banner::wherePosition($data['position'])->max('ordinal');
                $ordinalMax = $ordinalMax ?? 0;
                $data['ordinal'] = $ordinalMax + 1;
            }

            $banner = Banner::create($data);

            if ($banner && $request->hasFile('image')) {
                $imageFile->move('public/images/banners', $data['image']);
            }


            return sendResponse($banner, 'Create banner successfully', 201);
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $banner = Banner::find($id);
        if (!$banner) return sendError('Not found');
        return sendResponseBasic($banner);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $banner = Banner::find($id);
        if (!$banner) return sendError('Banner not found', [], 400);

        $data = $request->all();

        $validator = Validator::make($data, [
            'image' => 'nullable|file|image',
//            'link' => 'nullable|url',
            'ordinal' => 'nullable|integer',
            'is_show' => ['nullable', 'integer', Rule::in([0, 1])],
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $imageFile = $request->file('image');
            $imageCurrent = '';
            $position = $banner->position;

            if (array_key_exists('position', $data) && !is_null($data['position']))
                $position = $data['position'];

            if ($request->hasFile('image')) {
                $imageCurrent = $banner->getOriginal('image');
                $name = time() . '-' . $position . '-' . $imageFile->getClientOriginalName();

                $data['image'] = $name;
            }

            if (array_key_exists('ordinal', $data) && is_null($data['ordinal'])) {
                $ordinalMax = Banner::wherePosition($position)->whereNotIn('id', [$id])->max('ordinal');
                $ordinalMax = $ordinalMax ?? 0;
                $data['ordinal'] = $ordinalMax + 1;
            }

            $result = $banner->update($data);

            if ($result && $request->hasFile('image')) {
                if (file_exists(public_path() . '/images/banners/' . $imageCurrent)) {
                    File::delete(public_path() . '/images/banners/' . $imageCurrent);
                }

                $imageFile->move('public/images/banners', $data['image']);
            }

            return sendResponse($banner, 'Update banner successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = Banner::find($id);
        if (!$banner) return sendError('Banner not found', [], 400);

        try {
            $imageCurrent = $banner->getOriginal('image');

            $result = $banner->delete();

            if ($result) {
                if (file_exists(public_path() . '/images/banners/' . $imageCurrent)) {
                    File::delete(public_path() . '/images/banners/' . $imageCurrent);
                }
            }

            return sendResponse([], 'Delete banner successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    public function getBanners(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'limit' => 'nullable|integer'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $limit = $request->limit ?? PHP_INT_MAX;
        $position = $request->position;

        $banners = Banner::select('id', 'image', 'link')->wherePosition($position)->whereIsShow(1)->orderBy('ordinal')->limit($limit)->get();

        return sendResponse($banners);
    }
}
