<?php

namespace App\Http\Controllers\UserAuth;

use App\Models\PasswordReset;
use App\Models\User;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PasswordResetController extends Controller
{
    /**
     * Create token password reset
     *
     * @param  [string] email
     * @return [string] message
     */
    public function create(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'email' => 'required|string|email',
        ]);

        if ($validate->fails())
            return sendError('Validation Error', $validate->errors(), 422);

        $user = User::where('email', $request->email)->first();
        if (!$user)
            return sendError("We can't find a user with that e-mail address.", [], 404);

        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => str_random(60)
            ]
        );

        if ($user && $passwordReset)
            $user->notify(new PasswordResetRequest($passwordReset->token));

        return sendResponse([], 'We have e-mailed your password reset link!');
    }

    /**
     * Find token password reset
     *
     * @param  [string] $token
     * @return [string] message
     * @return [json] passwordReset object
     */
    public function find($token)
    {
        $passwordReset = PasswordReset::where('token', $token)->first();

        if (!$passwordReset)
            return sendError('This password reset token is invalid.', [], 404);

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return sendError('This password reset token is invalid.', [], 404);
        }

        return sendResponse($passwordReset, 'Find user by token successfully');
    }

    /**
     * Reset password
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] token
     * @return [string] message
     * @return [json] user object
     */
    public function reset(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string|confirmed',
            'token' => 'required|string'
        ]);

        if ($validate->fails()) return sendError('Validate Error', $validate->errors(), 422);

        $passwordReset = PasswordReset::where([
            ['token', $request->token],
            ['email', $request->email]
        ])->first();

        if (!$passwordReset)
            return sendError('This password reset token is invalid.', [], 404);

        $user = User::where('email', $passwordReset->email)->first();

        if (!$user)
            return sendError("We can't find a user with that e-mail address.", [], 404);

        $user->password = bcrypt($request->password);
        $user->save();

        $passwordReset->delete();

        $user->notify(new PasswordResetSuccess());

        return sendResponse($user, 'Reset password successfully!');
    }
}
