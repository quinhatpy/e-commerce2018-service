<?php

namespace App\Http\Controllers\UserAuth;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string',
//            'remember' => 'boolean'
        ]);
        if ($validate->fails())
            return sendError('Validation Error', $validate->errors(), 422);


        $credentials = request(['email', 'password']);

        if (!$token = auth('api')->attempt($credentials)) {
            return sendError('Unauthorized user', [], 401);
        }

        return sendResponse([
            'token' => $token,
            'token_type' => 'bearer',
//            'expires' => auth('api')->factory()->getTTL() *1,
        ], 'login success');

    }

    /**
     * Logout user (Revoke the token)
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse [string] message
     */
    public function logout()
    {
        auth('api')->logout();
        return sendResponse([], 'logout success');
    }

    /**
     * Get the authenticated User
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse [json] user object
     */
    public function getUser()
    {
        $id= auth('api')->user()->id;
        $user= User::with('role')->find($id);

        return sendResponse($user, 'Get user successfully');
    }
}
