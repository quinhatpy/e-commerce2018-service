<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $offset = $request->offset ?? 0;
        $limit = $request->limit ?? PHP_INT_MAX;
        $keyword = $request->keyword ?? '';
        $sort = $request->sort_by ?? '';
        $orderArray = [];

        if ($sort) {
            $sortArray = explode(',', $sort);

            foreach ($sortArray as $sortItem) {
                if (strpos($sortItem, '-') === 0) {
                    $orderArray[substr($sortItem, 1)] = 'DESC';
                } else {
                    $orderArray[trim($sortItem)] = 'ASC';
                }
            }
        }


        $galleries = Gallery::with(['user' => function($query) {
            $query->select('id', 'name');
        }])->where('title', 'LIKE', "%$keyword%")->offset($offset)->limit($limit);

        $totalRows = Gallery::where('title', 'LIKE', "%$keyword%")->get()->count();

        foreach ($orderArray as $orderColumn => $orderValue) {
            $galleries->orderBy($orderColumn, $orderValue);
        }

        $galleries = $galleries->get()->makeHidden('user_id');

        return sendResponse([
            'totalRows' => $totalRows,
            'galleries' => $galleries
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'file' => 'required|file|image',
            'link' => 'nullable|url',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $file = $request->file('file');
            $folderName = Carbon::now()->format('Y-m');

            if ($request->hasFile('file')) {
                $name = time() . '-' . $file->getClientOriginalName();

                $data['path'] = $folderName . '/' . $name;
            }

            $data['user_id'] = $request->user()->id;
            unset($data['file']);

            $gallery = Gallery::create($data);

            if ($gallery && $request->hasFile('file')) {
                if (!File::exists(public_path('uploads/') . $folderName)) {
                    File::makeDirectory(public_path('uploads/') . $folderName);
                }

                $file->move(public_path('uploads/') . $folderName, $data['path']);
            }


            return sendResponse($gallery, 'Upload file successfully', 201);
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gallery = Gallery::with(['user' => function($query) {
            $query->select('id', 'name');
        }])->find($id);
        if (!$gallery) return sendError('Not found');
        return sendResponse($gallery->makeHidden('user_id'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gallery = Gallery::find($id);
        if (!$gallery) return sendError('File not found', [], 400);

        $data = $request->all();

        $validator = Validator::make($data, [
            'file' => 'nullable|file|image',
            'link' => 'nullable|url',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $file = $request->file('file');
            $fileCurrent = '';
            $folderName = Carbon::now()->format('Y-m');

            if ($request->hasFile('file')) {
                $fileCurrent = $gallery->getOriginal('path');
                $name = time() . '-' . $file->getClientOriginalName();

                $data['path'] = $folderName . '/' . $name;
            }

            $data['user_id'] = $request->user()->id;
            unset($data['file']);

            $result = $gallery->update($data);

            if ($result && $request->hasFile('file')) {
                if (file_exists(public_path('uploads/') . $fileCurrent)) {
                    File::delete(public_path('uploads/') . $fileCurrent);
                }

                if (!File::exists(public_path('uploads/') . $folderName)) {
                    File::makeDirectory(public_path('uploads/') . $folderName);
                }

                $file->move(public_path('uploads/') . $folderName, $data['path']);
            }

            return sendResponse($gallery, 'Update file successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery = Gallery::find($id);
        if (!$gallery) return sendError('File not found', [], 400);

        try {
            $fileCurrent = $gallery->getOriginal('path');

            $result = $gallery->delete();

            if ($result) {
                if (file_exists(public_path('uploads/') . $fileCurrent)) {
                    File::delete(public_path('uploads/') . $fileCurrent);
                }
            }

            return sendResponse([], 'Delete file successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }
}
