<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $offset = $request->offset ?? 0;
        $limit = $request->limit ?? PHP_INT_MAX;
        $keyword = $request->keyword ?? '';
        $sort = $request->sort_by ?? '';
        $orderArray = [];

        if ($sort) {
            $sortArray = explode(',', $sort);

            foreach ($sortArray as $sortItem) {
                if (strpos($sortItem, '-') === 0) {
                    $orderArray[substr($sortItem, 1)] = 'DESC';
                } else {
                    $orderArray[trim($sortItem)] = 'ASC';
                }
            }
        }


        $brands = Brand::where('name', 'LIKE', "%$keyword%")->offset($offset)->limit($limit);
        $totalRows = Brand::where('name', 'LIKE', "%$keyword%")->get()->count();

        foreach ($orderArray as $orderColumn => $orderValue) {
            $brands->orderBy($orderColumn, $orderValue);
        }

        $brands = $brands->get();

        return sendResponse([
            'totalRows' => $totalRows,
            'brands' => $brands
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required',
            'slug' => 'required|max:150|unique:brands,slug',
            'logo' => 'required|file|image',
            'is_show' => ['nullable', 'integer', Rule::in([0, 1])],
            'seo_title' => 'nullable|max:60'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $logoFile = $request->file('logo');

            if ($request->hasFile('logo')) {
                $name = time() . '-' . str_slug($data['name']) . '-' . $logoFile->getClientOriginalName();
                $data['logo'] = $name;
            }

            $brand = Brand::create($data);

            if ($brand && $request->hasFile('logo')) {
                $logoFile->move('public/images/brands', $data['logo']);
            }


            return sendResponse($brand, 'Create brand successfully', 201);
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $brand = Brand::find($id);
        if (!$brand) return sendError('Not found');
        return sendResponse($brand);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = Brand::find($id);
        if (!$brand) return sendError('Brand not found', [], 400);

        $data = $request->all();

        $validator = Validator::make($data, [
            'slug' => 'required|max:150|unique:brands,slug,' . $brand->id,
            'logo' => 'nullable|file|image',
            'is_show' => ['nullable', 'integer', Rule::in([0, 1])],
            'seo_title' => 'nullable|max:60'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $logoFile = $request->file('logo');
            $imageCurrent = '';

            if ($request->hasFile('logo')) {
                $imageCurrent = $brand->getOriginal('logo');
                $name = time() . '-' . str_slug($data['name']) . '-' . $logoFile->getClientOriginalName();

                $data['logo'] = $name;
            }

            $result = $brand->update($data);

            if ($result && $request->hasFile('logo')) {
                if (file_exists(public_path() . '/images/brands/' . $imageCurrent)) {
                    File::delete(public_path() . '/images/brands/' . $imageCurrent);
                }

                $logoFile->move('public/images/brands', $data['logo']);
            }


            return sendResponse($brand, 'Update brand successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brand::find($id);
        if (!$brand) return sendError('Brand not found', [], 400);

        try {
            $imageCurrent = $brand->getOriginal('logo');

            $result = $brand->delete();

            if ($result) {
                if (file_exists(public_path() . '/images/brands/' . $imageCurrent)) {
                    File::delete(public_path() . '/images/brands/' . $imageCurrent);
                }
            }

            return sendResponse([], 'Delete brand successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    public function getBrandsInHome(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $limit = $request->limit ?? PHP_INT_MAX;

        $brands = Brand::join('products', 'brands.id', '=', 'products.brand_id')
            ->select('brands.id', 'brands.name', 'brands.slug', 'brands.logo')
            ->groupBy('brands.id', 'brands.name', 'brands.slug', 'brands.logo')
            ->orderBy(DB::raw('count(e2018_brands.id)'), 'DESC')
            ->limit($limit)->get();

//        $brandIdArray = [];
//        foreach ($brandIds as $brandId)
//            $brandIdArray[] = $brandId->id;
//
//
//
//        $brands = Brand::whereIn('id', $brandIdArray)->get();

        return sendResponse($brands);
    }
}
