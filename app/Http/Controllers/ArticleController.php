<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Customer;
use App\Notifications\MessageNotification;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $offset = $request->offset ?? 0;
        $limit = $request->limit ?? PHP_INT_MAX;
        $keyword = $request->keyword ?? '';
        $sort = $request->sort_by ?? '';
        $orderArray = [];

        if ($sort) {
            $sortArray = explode(',', $sort);

            foreach ($sortArray as $sortItem) {
                if (strpos($sortItem, '-') === 0) {
                    $orderArray[substr($sortItem, 1)] = 'DESC';
                } else {
                    $orderArray[trim($sortItem)] = 'ASC';
                }
            }
        }

        $articles = Article::with(['user' => function ($query) {
            $query->select('id', 'name');
        }])->where('name', 'LIKE', "%$keyword%")->offset($offset)->limit($limit);

        foreach ($orderArray as $orderColumn => $orderValue) {
            $articles->orderBy($orderColumn, $orderValue);
        }

        $articles = $articles->get()->makeHidden('user_id');

        $totalRows = Article::where('name', 'LIKE', "%$keyword%")->get()->count();

        return sendResponse([
            'totalRows' => $totalRows,
            'articles' => $articles
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required',
            'content' => 'required',
            'slug' => 'required|max:150|unique:articles,slug',
            'thumbnail' => 'nullable|file|image',
            'is_show' => ['nullable', 'integer', Rule::in([0, 1])],
            'seo_title' => 'nullable|max:60'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $thumbnailFile = $request->file('thumbnail');

            if ($request->hasFile('thumbnail')) {
                $name = time() . '-' . str_slug($data['name']) . '-' . $thumbnailFile->getClientOriginalName();
                $data['thumbnail'] = $name;
            }

            $data['user_id'] = $request->user()->id;

            $article = Article::create($data);

            if ($article && $request->hasFile('thumbnail')) {
                $thumbnailFile->move('public/images/articles', $data['thumbnail']);
            }

            $articleCreated = Article::with(['user' => function ($query) {
                $query->select('id', 'name');
            }])->find($article->id)->makeHidden('user_id');

            $customer = Customer::all();
            $body = 'There is a new article';
            $data =['openUrl' => '/tin/'.$articleCreated->slug, 'image' => $articleCreated->thumbnail];
            Notification::send($customer, new MessageNotification($body, $data));

            return sendResponse($articleCreated, 'Create article successfully', 201);
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::with(['user' => function ($query) {
            $query->select('id', 'name');
        }])->find($id);

        if (!$article) return sendError('Not found');

        return sendResponse($article->makeHidden('user_id'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $article = Article::find($id);
        if (!$article) return sendError('Article not found', [], 400);

        $data = $request->all();

        $validator = Validator::make($data, [
            'slug' => 'nullable|max:150|unique:articles,slug,' . $article->id,
            'thumbnail' => 'nullable|file|image',
            'is_show' => ['nullable', 'integer', Rule::in([0, 1])],
            'seo_title' => 'nullable|max:60'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $thumbnail = $request->file('thumbnail');
            $imageCurrent = '';

            if ($request->hasFile('thumbnail')) {
                $imageCurrent = $article->getOriginal('thumbnail');
                $name = time() . '-' . str_slug($data['name']) . '-' . $thumbnail->getClientOriginalName();

                $data['thumbnail'] = $name;
            }

            $result = $article->update($data);

            if ($result && $request->hasFile('thumbnail')) {
                if (file_exists(public_path() . '/images/articles/' . $imageCurrent)) {
                    File::delete(public_path() . '/images/articles/' . $imageCurrent);
                }

                $thumbnail->move('public/images/articles', $data['thumbnail']);
            }

            $articleUpdated = Article::with(['user' => function ($query) {
                $query->select('id', 'name');
            }])->find($article->id)->makeHidden('user_id');

            $customer = Customer::all();
            $body = 'There is a recently updated article';
            $data =['openUrl' => '/tin/'.$articleUpdated->slug, 'image' => $articleUpdated->thumbnail];
            Notification::send($customer, new MessageNotification($body, $data));

            return sendResponse($articleUpdated, 'Update article successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::find($id);
        if (!$article) return sendError('Article not found', [], 400);

        try {
            $imageCurrent = $article->getOriginal('thumbnail');

            $result = $article->delete();

            if ($result) {
                if (file_exists(public_path() . '/images/articles/' . $imageCurrent)) {
                    File::delete(public_path() . '/images/articles/' . $imageCurrent);
                }
            }

            return sendResponse([], 'Delete article successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    public function countArticle() {
        $count = Article::get()->count();
        return sendResponse($count);
    }

    public function getArticles(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $offset = $request->offset ?? 0;
        $limit = $request->limit ?? PHP_INT_MAX;


        $articles = Article::with(['user' => function ($query) {
            $query->select('id', 'name');
        }])->select('id', 'name', 'short_description', 'slug', 'thumbnail', 'created_at', 'user_id')
            ->whereIsShow(1)
            ->offset($offset)->limit($limit)->get()->makeHidden('user_id');

        $totalRows = Article::all()->count();

        return sendResponse([
            'totalRows' => $totalRows,
            'articles' => $articles
        ]);
    }

    public function getArticle(Request $request)
    {
        $article = Article::with(['user' => function ($query) {
            $query->select('id', 'name');
        }])->select('id', 'name', 'short_description', 'slug', 'thumbnail', 'created_at', 'user_id', 'seo_description', 'seo_title', 'content')
            ->whereSlug($request->slug)->first()->makeHidden('user_id');


        return sendResponse($article);
    }
}
