<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $offset
     * @param int $limit
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $offset = $request->offset ?? 0;
        $limit = $request->limit ?? PHP_INT_MAX;
        $keyword = $request->keyword ?? '';
        $sort = $request->sort_by ?? '';
        $orderArray = [];

        if ($sort) {
            $sortArray = explode(',', $sort);

            foreach ($sortArray as $sortItem) {
                if (strpos($sortItem, '-') === 0) {
                    $orderArray[substr($sortItem, 1)] = 'DESC';
                } else {
                    $orderArray[trim($sortItem)] = 'ASC';
                }
            }
        }

        $users = User::with(['role'])->where('name', 'LIKE', "%$keyword%")->offset($offset)->limit($limit);

        foreach ($orderArray as $orderColumn => $orderValue) {
            $users->orderBy($orderColumn, $orderValue);
        }

        $users = $users->get();

        $totalRows = User::where('name', 'LIKE', "%$keyword%")->get()->count();

        return sendResponse([
            'totalRows' => $totalRows,
            'users' => $users
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed|min:6',
            'gender' => ['required', 'integer', Rule::in([0, 1]),],
            'birthday' => 'nullable|date_format:Y-m-d',
            'avatar' => 'nullable|file|image',
            'role_id' => 'required|integer|exists:roles,id'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $data['password'] = bcrypt($data['password']);

        unset($data['password_confirmation']);

        try {
            $avatarFile = $request->file('avatar');

            if ($request->hasFile('avatar')) {
                $name = time() . '-' . str_slug($data['name']) . '-' . $avatarFile->getClientOriginalName();
                $data['avatar'] = $name;
            }

            $user = User::create($data);

            if ($user && $request->hasFile('avatar')) {
                $avatarFile->move('public/images/users', $data['avatar']);
            }

            $userCreated = User::with('role')->find($user->id);

            return sendResponse($userCreated, 'Create user successfully', 201);
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ['id' => $id];

        $validator = Validator::make($data, [
            'id' => 'required|integer|exists:users'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $user = User::with('role')->find($id);

        return sendResponseBasic($user);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except(['email']);
        $data['id'] = $id;

        $validator = Validator::make($data, [
            'id' => 'required|integer|exists:users',
            'password' => 'nullable|confirmed|min:6',
            'gender' => ['required', 'integer', Rule::in([0, 1]),],
            'birthday' => 'nullable|date_format:Y-m-d',
            'avatar' => 'nullable|file|image',
            'role_id' => 'required|integer|exists:roles,id'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        if (!empty($data['password']))
            $data['password'] = bcrypt($data['password']);
        else unset($data['password']);

        unset($data['password_confirmation']);
        unset($data['id']);

        try {
            $user = User::find($id);
            $avatarFile = $request->file('avatar');
            $imageCurrent = '';

            if ($request->hasFile('avatar')) {
                $imageCurrent = $user->getOriginal('avatar');
                $name = time() . '-' . str_slug($data['name']) . '-' . $avatarFile->getClientOriginalName();

                $data['avatar'] = $name;
            }

            $result = $user->update($data);

            if ($result && $request->hasFile('avatar')) {
                if (file_exists(public_path() . '/images/users/' . $imageCurrent)) {
                    File::delete(public_path() . '/images/users/' . $imageCurrent);
                }

                $avatarFile->move('public/images/users', $data['avatar']);
            }

            $userUpdated = User::with('role')->find($user->id);

            return sendResponse($userUpdated, 'Update user successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = ['id' => $id];

        $validator = Validator::make($data, [
            'id' => 'required|integer|exists:users'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $user = User::find($id);

        try {
            $imageCurrent = $user->getOriginal('avatar');

            $result = $user->delete();

            if ($result) {
                if (file_exists(public_path() . '/images/users/' . $imageCurrent)) {
                    File::delete(public_path() . '/images/users/' . $imageCurrent);
                }
            }

            return sendResponse([], 'Delete user successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request)
    {
        $data = $request->except(['email']);
        $id = auth('api')->user()->id;
        $data['id'] = $id;

        $validator = Validator::make($data, [
            'password' => 'nullable|confirmed|min:6',
            'gender' => ['required', 'integer', Rule::in([0, 1]),],
            'birthday' => 'nullable|date_format:Y-m-d',
            'avatar' => 'nullable|file|image',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        if (!empty($data['password']))
            $data['password'] = bcrypt($data['password']);
        else unset($data['password']);

        unset($data['password_confirmation']);
        unset($data['id']);

        try {
            $user = User::find($id);
            $avatarFile = $request->file('avatar');
            $imageCurrent = '';

            if ($request->hasFile('avatar')) {
                $imageCurrent = $user->getOriginal('avatar');
                $name = time() . '-' . str_slug($data['name']) . '-' . $avatarFile->getClientOriginalName();

                $data['avatar'] = $name;
            }

            $result = $user->update($data);

            if ($result && $request->hasFile('avatar')) {
                if (file_exists(public_path() . '/images/users/' . $imageCurrent)) {
                    File::delete(public_path() . '/images/users/' . $imageCurrent);
                }

                $avatarFile->move('public/images/users', $data['avatar']);
            }

            $userUpdated = User::with('role')->find($user->id);

            return sendResponse($userUpdated, 'Update profile successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }
}
