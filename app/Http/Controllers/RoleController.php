<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    private $permissionsModel = ['attribute', 'article', 'banner', 'branch', 'brand', 'category', 'category_banner', 'customer', 'delivery_method', 'district', 'menu', 'order', 'product', 'province',
        'role', 'user', 'ward', 'wishlist'];
    private $permissionAction = ['index', 'show', 'create', 'update', 'delete'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        return sendResponse($roles);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $dataValidate = [
            'name' => 'required|unique:roles,name',
        ];
        foreach ($this->permissionsModel as $model)
            $dataValidate[$model] = 'nullable|array';

        $validator = Validator::make($data, $dataValidate);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);


        $name = $request->name;
        $dataPermission = $request->only($this->permissionsModel);


        $permissions = [];

        foreach ($this->permissionsModel as $model) {
            foreach ($this->permissionAction as $action) {
                if (isset($dataPermission[$model][$action]) && $dataPermission[$model][$action] == 'on')
                    $permissions[$model . '.' . $action] = true;
                else $permissions[$model . '.' . $action] = false;
            }

        }

        try {
            $role = Role::create([
                'name' => $name,
                'permissions' => $permissions
            ]);
            return sendResponse($role, 'Create role successfully', 201);
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ['id' => $id];

        $validator = Validator::make($data, [
            'id' => 'required|integer|exists:roles'
        ]);
        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $role = Role::find($id);

        return sendResponse($role);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['id'] = $id;
        $dataValidate = [
            'id' => 'required|integer|exists:roles',
            'name' => 'required|unique:roles,name,' . $id,
        ];

        foreach ($this->permissionsModel as $model)
            $dataValidate[$model] = 'nullable|array';

        $validator = Validator::make($data, $dataValidate);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);


        $name = $request->name;
        $dataPermission = $request->only($this->permissionsModel);


        $permissions = [];

        foreach ($this->permissionsModel as $model) {
            foreach ($this->permissionAction as $action) {
                if (isset($dataPermission[$model][$action]) && $dataPermission[$model][$action] == 'on')
                    $permissions[$model . '.' . $action] = true;
                else $permissions[$model . '.' . $action] = false;
            }

        }

        try {
            $role = Role::find($id);
            $role->update([
                'name' => $name,
                'permissions' => $permissions
            ]);

            return sendResponse($role, 'Update role successfully', 201);
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = ['id' => $id];

        $validator = Validator::make($data, [
            'id' => 'required|integer|exists:roles'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $role = Role::find($id);
            $role->delete();

            return sendResponse([], 'Delete role successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }
}
