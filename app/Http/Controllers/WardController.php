<?php

namespace App\Http\Controllers;

use App\Models\District;
use App\Models\Ward;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'province_id' => 'nullable|integer|exists:provinces,id',
            'district_id' => 'nullable|integer|exists:districts,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $offset = $request->offset ?? 0;
        $limit = $request->limit ?? PHP_INT_MAX;
        $sort = $request->sort_by ?? '';
        $orderArray = [];

        if ($sort) {
            $sortArray = explode(',', $sort);

            foreach ($sortArray as $sortItem) {
                if (strpos($sortItem, '-') === 0) {
                    $orderArray[substr($sortItem, 1)] = 'DESC';
                } else {
                    $orderArray[trim($sortItem)] = 'ASC';
                }
            }
        }

        $districtIdArray = [];

        if (!empty($request->province_id)) {
            $districts = District::select('id')->whereProvinceId($request->province_id)->get();
            foreach ($districts as $district)
                $districtIdArray[] = $district->id;
        }

        if (!empty($request->district_id))
            $districtIdArray = [$request->district_id];

        $wardsQuery = Ward::with(['district' => function ($query) {
            $query->select('id', 'name');
        }]);

        if (!empty($districtIdArray)) {
            $wards = $wardsQuery->whereIn('district_id', $districtIdArray)->offset($offset)->limit($limit);
            $totalRows = Ward::whereIn('district_id', $districtIdArray)->get()->count();
        } else {
            $wards = $wardsQuery->offset($offset)->limit($limit);
            $totalRows = Ward::get()->count();
        }

        foreach ($orderArray as $orderColumn => $orderValue) {
            $wards->orderBy($orderColumn, $orderValue);
        }

        $wards = $wards->get()->makeHidden('district_id');

        return sendResponse([
            'totalRows' => $totalRows,
            'wards' => $wards
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required',
            'type' => 'required',
            'district_id' => 'required|integer|exists:districts,id'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $ward = Ward::create($data);
            $wardCreated = Ward::with([
                'district' => function ($district) {
                    $district->select('id', 'name', 'province_id')->with(['province' => function($query) {
                        $query->select('id', 'name');
                    }]);
                }
            ])->find($ward->id)->makeHidden('district_id');

            $wardCreated->district->makeHidden('province_id');

            return sendResponse($wardCreated, 'Create ward successfully', 201);
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ward = Ward::with(['district' => function($query){
            $query->select('id', 'name');
        }])->find($id);

        if (!$ward) return sendError('Not found');
        return sendResponse($ward->makeHidden('district_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ward = Ward::find($id);
        if (!$ward) return sendError('Ward not found', [], 400);

        $data = $request->all();

        $validator = Validator::make($data, [
            'district_id' => 'nullable|integer|exists:districts,id'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $ward->update($data);
            $wardUpdated = Ward::with([
                'district' => function ($district) {
                    $district->select('id', 'name', 'province_id')->with(['province' => function($query) {
                        $query->select('id', 'name');
                    }]);
                }
            ])->find($ward->id)->makeHidden('district_id');

            $wardUpdated->district->makeHidden('province_id');

            return sendResponse($wardUpdated, 'Update ward successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ward = Ward::find($id);
        if (!$ward) return sendError('Ward not found', [], 400);

        try {
            $ward->delete();

            return sendResponse([], 'Delete ward successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }
}
