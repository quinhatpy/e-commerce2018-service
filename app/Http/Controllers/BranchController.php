<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\District;
use App\Models\Ward;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ward_id' => 'nullable|integer|exists:wards,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $offset = $request->offset ?? 0;
        $limit = $request->limit ?? PHP_INT_MAX;
        $sort = $request->sort_by ?? '';
        $orderArray = [];

        if ($sort) {
            $sortArray = explode(',', $sort);

            foreach ($sortArray as $sortItem) {
                if (strpos($sortItem, '-') === 0) {
                    $orderArray[substr($sortItem, 1)] = 'DESC';
                } else {
                    $orderArray[trim($sortItem)] = 'ASC';
                }
            }
        }

        $wardIdArray = [];


        if (!empty($request->province_id)) {
            $districtIdArray = [];
            $districts = District::select('id')->whereProvinceId($request->province_id)->get();
            foreach ($districts as $district)
                $districtIdArray[] = $district->id;

            $wards = Ward::select('id')->whereIn('district_id', $districtIdArray)->get();
            foreach ($wards as $ward)
                $wardIdArray[] = $ward->id;
        }

        if (!empty($request->district_id)) {
            $wardIdArray = [];
            $wards = Ward::select('id')->whereDistrictId($request->district_id)->get();
            foreach ($wards as $ward)
                $wardIdArray[] = $ward->id;
        }

        if (!empty($request->ward_id)) {
            $wardIdArray = [$request->ward_id];
        }

        $branchesQuery = Branch::with(['ward' => function ($query) {
            $query->select('id', 'name', 'type');
        }]);

        if (!empty($wardIdArray)) {
            $branches = $branchesQuery->whereIn('ward_id', $wardIdArray)->offset($offset)->limit($limit);
            $totalRows = Branch::whereIn('ward_id', $wardIdArray)->get()->count();
        } else {
            $branches = $branchesQuery->offset($offset)->limit($limit);
            $totalRows = Branch::get()->count();
        }

        foreach ($orderArray as $orderColumn => $orderValue) {
            $branches->orderBy($orderColumn, $orderValue);
        }

        $branches = $branches->get()->makeHidden('ward_id');

        return sendResponse([
            'totalRows' => $totalRows,
            'branches' => $branches
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required|unique:branches,name',
            'address' => 'required',
            'ward_id' => 'required|integer|exists:wards,id'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $branch = Branch::create($data);
            $branchCreated = Branch::with(['ward' => function ($ward) {
                $ward->select('id', 'name', 'type', 'district_id')->with(['district' => function ($district) {
                    $district->select('id', 'name', 'type', 'province_id')->with(['province' => function($province) {
                        $province->select('id', 'name', 'type');
                    }]);
                }]);
            }])->find($branch->id)->makeHidden('ward_id');

            $branchCreated->ward->makeHidden('district_id');
            $branchCreated->ward->district->makeHidden('province_id');

            return sendResponse($branchCreated, 'Create branch successfully', 201);
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $branch = Branch::with(['ward' => function ($query) {
            $query->select('id', 'name', 'type', 'district_id')->with(['district' => function($queryDistrict) {
                $queryDistrict->select('id', 'name', 'type', 'province_id')->with(['province' => function($queryProvince) {
                    $queryProvince->select('id', 'name', 'type');
                }]);
            }]);
        }])->find($id);
        $branch->ward->makeHidden('district_id');
        $branch->ward->district->makeHidden('province_id');
        if (!$branch) return sendError('Not found');
        return sendResponse($branch->makeHidden('ward_id'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $branch = Branch::find($id);
        if (!$branch) return sendError('Branch not found', [], 400);

        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required|unique:branches,name,' . $branch->id,
            'ward_id' => 'required|integer|exists:wards,id'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $branch->update($data);

            $branchUpdated = Branch::with(['ward' => function ($ward) {
                $ward->select('id', 'name', 'type', 'district_id')->with(['district' => function ($district) {
                    $district->select('id', 'name', 'type', 'province_id')->with(['province' => function($province) {
                        $province->select('id', 'name', 'type');
                    }]);
                }]);
            }])->find($branch->id)->makeHidden('ward_id');

            $branchUpdated->ward->makeHidden('district_id');
            $branchUpdated->ward->district->makeHidden('province_id');

            return sendResponse($branchUpdated, 'Update branch successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $branch = Branch::find($id);
        if (!$branch) return sendError('Branch not found', [], 400);

        try {
            $branch->delete();

            return sendResponse([], 'Delete branch successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }
}
