<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class OrderController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $offset = $request->offset ?? 0;
        $limit = $request->limit ?? PHP_INT_MAX;
        $keyword = $request->keyword ?? '';
        $sort = $request->sort_by ?? '';
        $orderArray = [];

        if ($sort) {
            $sortArray = explode(',', $sort);

            foreach ($sortArray as $sortItem) {
                if (strpos($sortItem, '-') === 0) {
                    $orderArray[substr($sortItem, 1)] = 'DESC';
                } else {
                    $orderArray[trim($sortItem)] = 'ASC';
                }
            }
        }

        $ordersQuery = Order::with(
            ['customer' => function ($query) {
                $query->select('id', 'name');
            }, 'user' => function ($query) {
                $query->select('id', 'name');
            }, 'order_details' => function ($query) {
                $query->select('product_id', 'order_id', 'quantity', 'price');
            }]
        );

        $orders = $ordersQuery->where('code', 'LIKE', "%$keyword%")->offset($offset)->limit($limit);

        foreach ($orderArray as $orderColumn => $orderValue) {
            $orders->orderBy($orderColumn, $orderValue);
        }

        $orders = $orders->get()->makeHidden(['customer_id', 'user_id']);

        foreach ($orders as $order) {
            foreach ($order->order_details as $orderDetail)
                $orderDetail->makeHidden('order_id');
        }

        $totalRows = Order::where('code', 'LIKE', "%$keyword%")->get()->count();

        return sendResponse([
            'totalRows' => $totalRows,
            'orders' => $orders
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only(['code', 'receiver_name', 'receiver_phone', 'receiver_address', 'status', 'is_paid', 'customer_id', 'note']);

        $validator = Validator::make($request->all(), [
            'code' => 'required|max:10|unique:orders,code',
            'receiver_name' => 'required',
            'receiver_phone' => 'required',
//            'receiver_address' => 'required',
            'status' => ['nullable', 'integer', Rule::in([0, 1, 2, 3, 4]),],
            'is_paid' => ['nullable', 'integer', Rule::in([0, 1]),],
            'customer_id' => 'nullable|integer|exists:customers,id',
            'carts' => 'required|array',
//            'carts.*.product_id' => 'required|integer|exists:products,id',
            'carts.*' => 'required|integer|min:1',
//            'carts.*.price' => 'required|integer'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $data['user_id'] = $request->user()->id;

            $order = Order::create($data);

            if ($order) {
                $carts = $request->carts;

                foreach ($carts as $productId => $quantity) {
                    $product = Product::find($productId);
                    $dataOrderDetail = [
                        'order_id' => $order->id,
                        'product_id' => $productId,
                        'quantity' => $quantity,
                        'price' => $product->price
                    ];

                    // update sold
                    $product->increment('sold', $quantity);
                    OrderDetail::create($dataOrderDetail);
                }
            }

            $orderCreated = Order::with(
                ['customer' => function ($query) {
                    $query->select('id', 'name');
                }, 'user' => function ($query) {
                    $query->select('id', 'name');
                }, 'order_details' => function ($query) {
                    $query->select('product_id', 'order_id', 'quantity', 'price');
                }]
            )->find($order->id)->makeHidden([ 'customer_id', 'user_id']);

            foreach ($orderCreated->order_details as $orderDetail)
                $orderDetail->makeHidden('order_id');

            return sendResponse($orderCreated, 'Create order successfully', 201);
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::with(
            ['customer' => function ($query) {
                $query->select('id', 'name');
            }, 'user' => function ($query) {
                $query->select('id', 'name');
            }, 'order_details' => function ($query) {
                $query->select('product_id', 'order_id', 'quantity', 'price')->with(['product' => function($queryProduct) {
                    $queryProduct->select('id', 'name', 'thumbnail');
                }]);
            }]
        )->find($id);

        if (!$order) return sendError('Not found');

        foreach ($order->order_details as $orderDetail)
            $orderDetail->makeHidden('order_id');

        return sendResponse($order->makeHidden(['customer_id', 'user_id']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        if (!$order) return sendError('Order not found', [], 400);

        $data = $request->only(['receiver_name', 'receiver_phone', 'receiver_address', 'status', 'is_paid', 'customer_id', 'note']);

        $validator = Validator::make($request->all(), [
            'status' => ['nullable', 'integer', Rule::in([0, 1, 2, 3, 4]),],
            'is_paid' => ['nullable', 'integer', Rule::in([0, 1]),],
            'customer_id' => 'nullable|integer|exists:customers,id',
            'carts' => 'nullable|array',
            'carts.*' => 'required|integer|min:1',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $data['user_id'] = $request->user()->id;

            $result = $order->update($data);
            if ($result && $request->has('carts')) {
                $carts = $request->carts;

                if (count($carts) > 0)
                    OrderDetail::whereOrderId($order->id)->delete();

                foreach ($carts as $productId => $quantity) {
                    $product = Product::find($productId);
                    $dataOrderDetail = [
                        'order_id' => $order->id,
                        'product_id' => $productId,
                        'quantity' => $quantity,
                        'price' => $product->price
                    ];

                    OrderDetail::create($dataOrderDetail);
                }
            }

            $orderUpdated = Order::with(
                ['customer' => function ($query) {
                    $query->select('id', 'name');
                }, 'user' => function ($query) {
                    $query->select('id', 'name');
                }, 'order_details' => function ($query) {
                    $query->select('product_id', 'order_id', 'quantity', 'price');
                }]
            )->find($order->id)->makeHidden(['customer_id', 'user_id']);

            foreach ($orderUpdated->order_details as $orderDetail)
                $orderDetail->makeHidden('order_id');

            return sendResponse($orderUpdated, 'Update order successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $order = Order::find($id);
        if (!$order) return sendError('Order not found', [], 400);

        try {
            $order->delete();

            return sendResponse([], 'Delete order successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    public function placeOrder(Request $request)
    {
        $data = $request->only(['receiver_name', 'receiver_phone', 'receiver_address', 'customer_id', 'note']);

        $validator = Validator::make($request->all(), [
            'receiver_name' => 'required',
            'receiver_phone' => 'required',
            'receiver_address' => 'required',
            'customer_id' => 'nullable|integer|exists:customers,id',
            'carts' => 'required|array',
            'carts.*' => 'required|integer|min:1',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $data['code'] = 'DH-' . time();
            $order = Order::create($data);

            if ($order) {
                $carts = $request->carts;

                foreach ($carts as $productId => $quantity) {
                    $product = Product::find($productId);
                    $dataOrderDetail = [
                        'order_id' => $order->id,
                        'product_id' => $productId,
                        'quantity' => $quantity,
                        'price' => $product->price
                    ];

                    // update sold
                    $product->increment('sold', $quantity);
                    OrderDetail::create($dataOrderDetail);
                }
            }

            $orderCreated = Order::with(
                ['customer' => function ($query) {
                    $query->select('id', 'name');
                }, 'order_details' => function ($query) {
                    $query->select('product_id', 'order_id', 'quantity', 'price');
                }]
            )->find($order->id)->makeHidden(['customer_id', 'user_id']);

            foreach ($orderCreated->order_details as $orderDetail)
                $orderDetail->makeHidden('order_id');

            return sendResponse($orderCreated, 'Place order successfully', 201);
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    public function getOrdersOfCustomer()
    {
        $customer = auth('customer')->user();

        if (!$customer) return sendError('Validate error', 'Customer not found', 422);
        $customerId = $customer->id;

        $orders = Order::select('code', 'created_at', 'status')
            ->withCount(['order_details as quantity' => function ($query) {
                $query->select(DB::raw('SUM(quantity)'));
            }])
            ->withCount(['order_details as total' => function ($query) {
                $query->select(DB::raw('SUM(quantity * price)'));
            }])
            ->whereCustomerId($customerId)
            ->get();
        return sendResponse($orders, 200);
    }

    public function getOrderByCode(Request $request)
    {
        $customer = auth('customer')->user();

        if (!$customer) return sendError('Validate error', 'Customer not found', 422);
        $customerId = $customer->id;

        $validator = Validator::make($request->all(), [
            'code' => 'required',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $order = Order::with(
            ['order_details' => function ($query) {
                $query->select('product_id', 'order_id', 'quantity', 'price')
                    ->with(['product' => function ($query) {
                        $query->select('id', 'name', 'slug', 'thumbnail');
                    }]);
            }]
        )->whereCode($request->code)->whereCustomerId($customerId)->first();

        if (!$order) return sendError('Not found');

        foreach ($order->order_details as $orderDetail)
            $orderDetail->makeHidden('order_id');

        return sendResponse($order->makeHidden(['customer_id', 'user_id']));
    }

    public function cancelOrder(Request $request)
    {
        $order = Order::whereCode($request->code)->first();
        if($order->status != 1)  return sendError('Action error', 'Order cannot cancel', 422);

        $orderUpdate = Order::whereCode($request->code)->update(['status' => 0]);

        return sendResponse($orderUpdate, 'Cancel order successfully');
    }

    public function countOrder() {
        $count = Order::get()->count();
        return sendResponse($count);
    }
}
