<?php

namespace App\Http\Controllers;

use App\Models\Province;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProvinceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $offset = $request->offset ?? 0;
        $limit = $request->limit ?? PHP_INT_MAX;
        $keyword = $request->keyword ?? '';
        $sort = $request->sort_by ?? '';
        $orderArray = [];

        if ($sort) {
            $sortArray = explode(',', $sort);

            foreach ($sortArray as $sortItem) {
                if (strpos($sortItem, '-') === 0) {
                    $orderArray[substr($sortItem, 1)] = 'DESC';
                } else {
                    $orderArray[trim($sortItem)] = 'ASC';
                }
            }
        }


        $provinces = Province::where('name', 'LIKE', "%$keyword%")->offset($offset)->limit($limit);
        $totalRows = Province::where('name', 'LIKE', "%$keyword%")->get()->count();

        foreach ($orderArray as $orderColumn => $orderValue) {
            $provinces->orderBy($orderColumn, $orderValue);
        }

        $provinces = $provinces->get();

        return sendResponse([
            'totalRows' => $totalRows,
            'provinces' => $provinces
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required',
            'type' => 'required',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $province = Province::create($data);

            return sendResponse($province, 'Create province successfully', 201);
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $province = Province::find($id);
        if (!$province) return sendError('Not found');
        return sendResponse($province);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $province = Province::find($id);
        if (!$province) return sendError('Province not found', [], 400);

        $data = $request->all();

        try {
            $province->update($data);

            return sendResponse($province, 'Update province successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $province = Province::find($id);
        if (!$province) return sendError('Province not found', [], 400);

        try {
           $province->delete();

            return sendResponse([], 'Delete province successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }
}
