<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $offset = $request->offset ?? 0;
        $limit = $request->limit ?? PHP_INT_MAX;
        $keyword = $request->keyword ?? '';
        $sort = $request->sort_by ?? '';
        $orderArray = [];

        if ($sort) {
            $sortArray = explode(',', $sort);

            foreach ($sortArray as $sortItem) {
                if (strpos($sortItem, '-') === 0) {
                    $orderArray[substr($sortItem, 1)] = 'DESC';
                } else {
                    $orderArray[trim($sortItem)] = 'ASC';
                }
            }
        }

        $menus = Menu::with(['parent' => function ($query) {
            $query->select('id', 'name');
        }])->where('name', 'LIKE', "%$keyword%")->offset($offset)->limit($limit);

        foreach ($orderArray as $orderColumn => $orderValue) {
            $menus->orderBy($orderColumn, $orderValue);
        }

        $menus = $menus->get()->makeHidden('parent_id');

        $totalRows = Menu::where('name', 'LIKE', "%$keyword%")->get()->count();

        return sendResponse([
            'totalRows' => $totalRows,
            'menus' => $menus
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required',
            'icon' => 'nullable|file|image',
            'link' => 'required',
            'ordinal' => 'nullable|integer',
            'is_show' => ['nullable', 'integer', Rule::in([0, 1])],
            'parent_id' => 'nullable|exists:menus,id',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $iconFile = $request->file('icon');

            if ($request->hasFile('icon')) {
                $name = time() . '-' . str_slug($data['name']) . '-' . $iconFile->getClientOriginalName();
                $data['icon'] = $name;
            }

            if (empty($data['ordinal'])) {
                $ordinalMax = Menu::whereParentId($data['parent_id'])->max('ordinal');
                $ordinalMax = $ordinalMax ?? 0;
                $data['ordinal'] = $ordinalMax + 1;
            }

            $menu = Menu::create($data);

            if ($menu && $request->hasFile('icon')) {
                $iconFile->move('public/images/menus', $data['icon']);
            }

            $menuCreated = Menu::with(['parent' => function ($query) {
                $query->select('id', 'name');
            }])->find($menu->id)->makeHidden('parent_id');

            return sendResponse($menuCreated, 'Create menu successfully', 201);
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $menu = Menu::with(['parent' => function ($query) {
            $query->select('id', 'name');
        }])->find($id);
        if (!$menu) return sendError('Not found');
        return sendResponse($menu->makeHidden('parent_id'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $menu = Menu::find($id);
        if (!$menu) return sendError('Menu not found', [], 400);

        $data = $request->all();

        $validator = Validator::make($data, [
            'icon' => 'nullable|file|image',
            'ordinal' => 'nullable|integer',
            'is_show' => ['nullable', 'integer', Rule::in([0, 1])],
            'parent_id' => 'nullable|exists:menus,id'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $iconFile = $request->file('icon');
            $imageCurrent = '';
            $parentId = $menu->parent_id;

            if (array_key_exists('parent_id', $data) && !is_null($data['parent_id']))
                $parentId = $data['parent_id'];

            if ($request->hasFile('icon')) {
                $imageCurrent = $menu->getOriginal('icon');
                $name = time() . '-' . str_slug($data['name']) . '-' . $iconFile->getClientOriginalName();

                $data['icon'] = $name;
            }

            if (array_key_exists('ordinal', $data) && is_null($data['ordinal'])) {
                $ordinalMax = Menu::whereParentId($parentId)->whereNotIn('id', [$id])->max('ordinal');
                $ordinalMax = $ordinalMax ?? 0;
                $data['ordinal'] = $ordinalMax + 1;
            }

            $result = $menu->update($data);

            if ($result && $request->hasFile('icon')) {
                if (file_exists(public_path() . '/images/menus/' . $imageCurrent)) {
                    File::delete(public_path() . '/images/menus/' . $imageCurrent);
                }

                $iconFile->move('public/images/menus', $data['icon']);
            }

            $menuUpdated = Menu::with(['parent' => function ($query) {
                $query->select('id', 'name');
            }])->find($menu->id)->makeHidden('parent_id');

            return sendResponse($menuUpdated, 'Update menu successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = Menu::find($id);
        if (!$menu) return sendError('Menu not found', [], 400);

        try {
            $imageCurrent = $menu->getOriginal('icon');

            $result = $menu->delete();

            if ($result) {
                if (file_exists(public_path() . '/images/menus/' . $imageCurrent)) {
                    File::delete(public_path() . '/images/menus/' . $imageCurrent);
                }
            }

            return sendResponse([], 'Delete menu successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    public function getMenus()
    {
        $menus = Menu::select(['id', 'name', 'link', 'parent_id'])->whereIsShow(1)->orderBy('ordinal')->get();
        return sendResponse($menus);
    }
}
