<?php

namespace App\Http\Controllers;

use App\Models\Attribute;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'nullable|integer|exists:categories,id',
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $offset = $request->offset ?? 0;
        $limit = $request->limit ?? PHP_INT_MAX;
        $sort = $request->sort_by ?? '';
        $orderArray = [];

        if ($sort) {
            $sortArray = explode(',', $sort);

            foreach ($sortArray as $sortItem) {
                if (strpos($sortItem, '-') === 0) {
                    $orderArray[substr($sortItem, 1)] = 'DESC';
                } else {
                    $orderArray[trim($sortItem)] = 'ASC';
                }
            }
        }

        $attributeQuery = Attribute::with(['category' => function ($query) {
            $query->select('id', 'name');
        }]);

        if (!empty($request->category_id)) {
            $attributes = $attributeQuery->whereCategoryId($request->category_id)->offset($offset)->limit($limit);
            $totalRows = Attribute::whereCategoryId($request->category_id)->get()->count();
        } else {
            $attributes = $attributeQuery->offset($offset)->limit($limit);
            $totalRows = Attribute::get()->count();
        }

        foreach ($orderArray as $orderColumn => $orderValue) {
            $attributes->orderBy($orderColumn, $orderValue);
        }

        $attributes = $attributes->get()->makeHidden('category_id');

        return sendResponse([
            'totalRows' => $totalRows,
            'attributes' => $attributes
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'required',
            'data_type' => 'nullable|in:string,number,text',
            'is_required' => ['nullable', 'integer', Rule::in([0, 1])],
            'is_show' => ['nullable', 'integer', Rule::in([0, 1])],
            'ordinal' => 'nullable|integer',
            'category_id' => 'required|integer|exists:categories,id'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            if (empty($data['ordinal'])) {
                $ordinalMax = Attribute::whereCategoryId($data['category_id'])->max('ordinal');
                $ordinalMax = $ordinalMax ?? 0;
                $data['ordinal'] = $ordinalMax + 1;
            }

            $attribute = Attribute::create($data);

            $attributeCreated = Attribute::with(['category' => function ($query) {
                $query->select('id', 'name');
            }])->find($attribute->id)->makeHidden('category_id');

            return sendResponse($attributeCreated, 'Create attribute successfully', 201);
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $attribute = Attribute::with(['category' => function ($query) {
            $query->select('id', 'name');
        }])->find($id);

        if (!$attribute) return sendError('Not found');

        return sendResponse($attribute->makeHidden('category_id'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attribute = Attribute::find($id);
        if (!$attribute) return sendError('Attribute not found', [], 400);

        $data = $request->all();

        $validator = Validator::make($data, [
            'data_type' => 'nullable|in:string,number,text',
            'is_required' => ['nullable', 'integer', Rule::in([0, 1])],
            'is_show' => ['nullable', 'integer', Rule::in([0, 1])],
            'ordinal' => 'nullable|integer',
            'category_id' => 'nullable|integer|exists:categories,id'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            if (array_key_exists('ordinal', $data) && is_null($data['ordinal'])) {
                $ordinalMax = Attribute::whereCategoryId($data['category_id'])->whereNotIn('id', [$id])->max('ordinal');
                $ordinalMax = $ordinalMax ?? 0;
                $data['ordinal'] = $ordinalMax + 1;
            }

            $attribute->update($data);
            $attributeUpdated = Attribute::with(['category' => function ($query) {
                $query->select('id', 'name');
            }])->find($attribute->id)->makeHidden('category_id');

            return sendResponse($attributeUpdated, 'Update attribute successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $attribute = Attribute::find($id);
        if (!$attribute) return sendError('Attribute not found', [], 400);

        try {
            $attribute->delete();

            return sendResponse([], 'Delete attribute successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }
}
