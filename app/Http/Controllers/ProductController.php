<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Customer;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\ProductImage;
use App\Models\ProductPromotion;
use App\Models\ProductQuantity;
use App\Notifications\MessageNotification;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $offset = $request->offset ?? 0;
        $limit = $request->limit ?? PHP_INT_MAX;
        $keyword = $request->keyword ?? '';
        $sort = $request->sort_by ?? '';
        $orderArray = [];

        if ($sort) {
            $sortArray = explode(',', $sort);

            foreach ($sortArray as $sortItem) {
                if (strpos($sortItem, '-') === 0) {
                    $orderArray[substr($sortItem, 1)] = 'DESC';
                } else {
                    $orderArray[trim($sortItem)] = 'ASC';
                }
            }
        }

        $products = Product::with([
            'user' => function ($query) {
                $query->select('id', 'name');
            },
            'category' => function ($query) {
                $query->select('id', 'name');
            },
            'brand' => function ($query) {
                $query->select('id', 'name');
            }
        ])->where('name', 'LIKE', "%$keyword%");

        $totalRows = Product::where('name', 'LIKE', "%$keyword%");

        if (!empty($request->category_id)) {
            $categories = Category::select('id', 'parent_id')->get();
            $children = getAllChildren($categories, $request->category_id);

            $categoryIdArray = [$request->category_id];
            foreach ($children as $child)
                $categoryIdArray[] = $child->id;

            $products->whereIn('category_id', $categoryIdArray);
            $totalRows->whereIn('category_id', $categoryIdArray);
        }

        $products->offset($offset)->limit($limit);

        foreach ($orderArray as $orderColumn => $orderValue) {
            $products->orderBy($orderColumn, $orderValue);
        }

        $products = $products->get()->makeHidden(['user_id', 'category_id', 'brand_id']);

       $totalRows = $totalRows->get()->count();

        return sendResponse([
            'totalRows' => $totalRows,
            'products' => $products
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only(['code', 'name', 'price', 'thumbnail', 'content', 'description', 'slug', 'is_show', 'seo_title', 'seo_description', 'brand_id', 'category_id']);

        $validator = Validator::make($request->all(), [
            'code' => 'required|max:30|unique:products,code',
            'name' => 'required',
            'price' => 'required|integer',
            'thumbnail' => 'nullable|file|image',
            'content' => 'required',
            'slug' => 'required|max:150|unique:products,slug',
            'is_show' => ['nullable', 'integer', Rule::in([0, 1])],
            'seo_title' => 'nullable|max:60',
            'brand_id' => 'required|integer|exists:brands,id',
            'category_id' => 'required|integer|exists:categories,id',
            'images.*' => 'nullable|file|image',
            'new_price' => 'nullable|integer',
            'attributes' => 'nullable|array',
            'quantities' => 'nullable|array',
            'quantities.*' => 'nullable|integer'
        ]);
        //            'attributes.*' => 'nullable|integer|exists:attributes,id'
        // can validate key cua array attribute xem co ton tai trong ban attribute hay khong
        // can validate key cua array quantities xem co ton tai trong ban branch hay khong

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $thumbnailFile = $request->file('thumbnail');

            $category = Category::find($data['category_id']);
            $folderName = $category->id;


            if ($request->hasFile('thumbnail')) {
                $name = time() . '-' . str_slug($data['name']) . '-' . $thumbnailFile->getClientOriginalName();
                $data['thumbnail'] = $folderName . '/' . $name;
            }

            $data['user_id'] = $request->user()->id;

            $product = Product::create($data);

            if ($product) {
                if ($request->hasFile('thumbnail')) {
                    if (!File::exists(public_path('images/products/thumbnail/') . $folderName)) {
                        File::makeDirectory(public_path('images/products/thumbnail/') . $folderName);
                    }

                    $thumbnailFile->move(public_path('images/products/thumbnail/') . $folderName, $data['thumbnail']);
                }

                if ($request->hasFile('images')) {
                    $images = $request->file('images');

                    foreach ($images as $image) {
                        $dataImage = [
                            'product_id' => $product->id,
                            'is_show' => 1
                        ];

                        $name = time() . '-' . str_slug($data['name']) . '-' . $image->getClientOriginalName();
                        $dataImage['path'] = $folderName . '/' . $name;

                        $productImage = ProductImage::create($dataImage);

                        if ($productImage) {
                            if (!File::exists(public_path('images/products/detail/') . $folderName)) {
                                File::makeDirectory(public_path('images/products/detail/') . $folderName);
                            }

                            $image->move(public_path('images/products/detail/') . $folderName, $dataImage['path']);
                        }
                    }
                }

                $attributes = $request->get('attributes');
                foreach ($attributes as $attributeId => $attributeValue) {
                    $dataAttribute = [
                        'content' => $attributeValue,
                        'is_show' => 1,
                        'attribute_id' => $attributeId,
                        'product_id' => $product->id
                    ];

                    ProductAttribute::create($dataAttribute);
                }

                if ($request->has('new_price') && !is_null($request->new_price)) {
                    $dataPromotion = [
                        'new_price' => $request->new_price,
                        'product_id' => $product->id,
                        'is_show' => 1,
                        'start' => $request->promotion_start,
                        'expire' => $request->promotion_expire
                    ];

                    ProductPromotion::create($dataPromotion);
                }

                $quantities = $request->quantities;
                foreach ($quantities as $branchId => $quantity) {
                    $dataQuantity = [
                        'quantity' => $quantity,
                        'product_id' => $product->id,
                        'branch_id' => $branchId,
                    ];

                    ProductQuantity::create($dataQuantity);
                }
            }

            $productCreated = Product::with([
                'user' => function ($query) {
                    $query->select('id', 'name');
                },
                'category' => function ($query) {
                    $query->select('id', 'name');
                },
                'brand' => function ($query) {
                    $query->select('id', 'name');
                },
                'attributes' => function ($query) {
                    $query->select('id', 'content', 'is_show', 'product_id', 'attribute_id');
                },
                'images' => function ($query) {
                    $query->select('id', 'path', 'is_show', 'product_id');
                },
                'quantities' => function ($query) {
                    $query->select('id', 'quantity', 'product_id', 'branch_id');
                },
                'promotion' => function ($query) {
                    $query->select('id', 'new_price', 'product_id', 'is_show', 'start', 'expire');
                }
            ])->withCount([
                'ratings as rating' => function ($query) {
                    $query->select(DB::raw('AVG(star) as rating'));
                }
            ])->find($product->id)->makeHidden(['user_id', 'category_id', 'brand_id']);

            foreach ($productCreated->attributes as $attribute) {
                $attribute->makeHidden('product_id');
            }

            foreach ($productCreated->images as $image) {
                $image->makeHidden('product_id');
            }

            foreach ($productCreated->quantities as $quantity) {
                $quantity->makeHidden('product_id');
            }

            if ($productCreated->promotion)
                $productCreated->promotion->makeHidden('product_id');

            $customer = Customer::all();
            $body = 'There is a new product';
            $data =['openUrl' => '/san-pham/'.$productCreated->slug, 'image' => $productCreated->thumbnail];
            Notification::send($customer, new MessageNotification($body, $data));

            return sendResponse($productCreated, 'Create product successfully', 201);
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::with([
            'user' => function ($query) {
                $query->select('id', 'name');
            },
            'category' => function ($query) {
                $query->select('id', 'name');
            },
            'brand' => function ($query) {
                $query->select('id', 'name');
            },
            'attributes' => function ($query) {
                $query->select('id', 'content', 'is_show', 'product_id', 'attribute_id');
            },
            'images' => function ($query) {
                $query->select('id', 'path', 'is_show', 'product_id');
            },
            'quantities' => function ($query) {
                $query->select('id', 'quantity', 'product_id', 'branch_id');
            },
            'promotion' => function ($query) {
                $query->select('id', 'new_price', 'product_id', 'is_show', 'start', 'expire');
            }
        ])->withCount([
            'ratings as rating' => function ($query) {
                $query->select(DB::raw('AVG(star) as rating'));
            }
        ])->find($id);


        if (!$product) return sendError('Not found');

        foreach ($product->attributes as $attribute) {
            $attribute->makeHidden('product_id');
        }

        foreach ($product->images as $image) {
            $image->makeHidden('product_id');
        }

        foreach ($product->quantities as $quantity) {
            $quantity->makeHidden('product_id');
        }

        if ($product->promotion)
            $product->promotion->makeHidden('product_id');

        return sendResponse($product->makeHidden(['user_id', 'category_id', 'brand_id']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        if (!$product) return sendError('File not found', [], 400);

        $data = $request->only(['code', 'name', 'price', 'thumbnail', 'content', 'description', 'slug', 'is_show', 'seo_title', 'seo_description', 'brand_id', 'category_id']);

        $validator = Validator::make($request->all(), [
            'code' => 'nullable|max:30|unique:products,code,' . $product->id,
            'price' => 'nullable|integer',
            'thumbnail' => 'nullable|file|image',
            'slug' => 'nullable|max:150|unique:products,slug,' . $product->id,
            'is_show' => ['nullable', 'integer', Rule::in([0, 1])],
            'seo_title' => 'nullable|max:60',
            'brand_id' => 'nullable|integer|exists:brands,id',
            'category_id' => 'nullable|integer|exists:categories,id',
            'images.*' => 'nullable|file|image',
            'new_price' => 'nullable|integer',
            'attributes' => 'nullable|array',
            'quantities' => 'nullable|array',
            'quantities.*' => 'nullable|integer'
        ]);
        //            'attributes.*' => 'nullable|integer|exists:attributes,id'
        // can validate key cua array attribute xem co ton tai trong ban attribute hay khong
        // can validate key cua array quantities xem co ton tai trong ban branch hay khong

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        try {
            $thumbnailFile = $request->file('thumbnail');

            $category = Category::find($product->category_id);
            $folderName = $category->id;

            if ($data['category_id'] != $product->category_id) {
                $category = Category::find($data['category_id']);
                $folderName = $category->id;
            }

            $fileCurrent = '';

            if ($request->hasFile('thumbnail')) {
                $fileCurrent = $product->getOriginal('thumbnail');
                $name = time() . '-' . str_slug($data['name']) . '-' . $thumbnailFile->getClientOriginalName();

                $data['thumbnail'] = $folderName . '/' . $name;
            }

            $data['user_id'] = $request->user()->id;

            $result = $product->update($data);

            if ($result) {
                if ($request->hasFile('thumbnail')) {
                    if (file_exists(public_path('images/products/thumbnail/') . $fileCurrent)) {
                        File::delete(public_path('images/products/thumbnail/') . $fileCurrent);
                    }

                    if (!File::exists(public_path('images/products/thumbnail/') . $folderName)) {
                        File::makeDirectory(public_path('images/products/thumbnail/') . $folderName);
                    }

                    $thumbnailFile->move(public_path('images/products/thumbnail/') . $folderName, $data['thumbnail']);
                }

                if ($request->hasFile('images')) {
                    $images = $request->file('images');

                    foreach ($images as $image) {
                        $dataImage = [
                            'product_id' => $product->id,
                            'is_show' => 1
                        ];

                        $name = time() . '-' . str_slug($data['name']) . '-' . $image->getClientOriginalName();
                        $dataImage['path'] = $folderName . '/' . $name;

                        $productImage = ProductImage::create($dataImage);

                        if ($productImage) {
                            if (!File::exists(public_path('images/products/detail/') . $folderName)) {
                                File::makeDirectory(public_path('images/products/detail/') . $folderName);
                            }

                            $image->move(public_path('images/products/detail/') . $folderName, $dataImage['path']);
                        }
                    }
                }

                $attributes = $request->get('attributes');
                if (!is_array($attributes)) {
                    $attributes = [];
                    ProductAttribute::whereProductId($product->id)->delete();
                }

                foreach ($attributes as $attributeId => $attributeValue) {
                    $dataAttribute = [
                        'content' => $attributeValue,
                        'is_show' => 1,
                        'attribute_id' => $attributeId,
                        'product_id' => $product->id
                    ];

                    ProductAttribute::updateOrCreate([
                        'attribute_id' => $attributeId,
                        'product_id' => $product->id
                    ], $dataAttribute);
                }

                if ($request->has('new_price')) {
                    if (!is_null($request->new_price)) {
                        $dataPromotion = [
                            'new_price' => $request->new_price,
                            'product_id' => $product->id,
                            'is_show' => 1,
                            'start' => $request->promotion_start,
                            'expire' => $request->promotion_expire
                        ];

                        ProductPromotion::updateOrCreate([
                            'product_id' => $product->id
                        ], $dataPromotion);
                    } else ProductPromotion::whereProductId($product->id)->delete();
                }

                $quantities = $request->quantities;
                if ($quantities && count($quantities) > 0) {
                    ProductQuantity::whereProductId($product->id)->delete();

                    foreach ($quantities as $branchId => $quantity) {
                        $dataQuantity = [
                            'quantity' => $quantity,
                            'product_id' => $product->id,
                            'branch_id' => $branchId,
                        ];

                        ProductQuantity::create($dataQuantity);
                    }
                }
            }

            $productUpdated = Product::with([
                'user' => function ($query) {
                    $query->select('id', 'name');
                },
                'category' => function ($query) {
                    $query->select('id', 'name');
                },
                'brand' => function ($query) {
                    $query->select('id', 'name');
                },
                'attributes' => function ($query) {
                    $query->select('id', 'content', 'is_show', 'product_id', 'attribute_id');
                },
                'images' => function ($query) {
                    $query->select('id', 'path', 'is_show', 'product_id');
                },
                'quantities' => function ($query) {
                    $query->select('id', 'quantity', 'product_id', 'branch_id');
                },
                'promotion' => function ($query) {
                    $query->select('id', 'new_price', 'product_id', 'is_show', 'start', 'expire');
                }
            ])->withCount([
                'ratings as rating' => function ($query) {
                    $query->select(DB::raw('AVG(star) as rating'));
                }
            ])->find($product->id)->makeHidden(['user_id', 'category_id', 'brand_id']);

            foreach ($productUpdated->attributes as $attribute) {
                $attribute->makeHidden('product_id');
            }

            foreach ($productUpdated->images as $image) {
                $image->makeHidden('product_id');
            }

            foreach ($productUpdated->quantities as $quantity) {
                $quantity->makeHidden('product_id');
            }

            if ($productUpdated->promotion)
                $productUpdated->promotion->makeHidden('product_id');

            $customer = Customer::all();
            $body = 'There is a recently updated product';
            $data =['openUrl' => '/san-pham/'.$productUpdated->slug, 'image' => $productUpdated->thumbnail];
            Notification::send($customer, new MessageNotification($body, $data));

            return sendResponse($productUpdated, 'Update product successfully', 201);
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if (!$product) return sendError('Product not found', [], 400);

        try {
            $thumbnailCurrent = $product->getOriginal('thumbnail');
            $result = $product->delete();

            if ($result) {
                if (file_exists(public_path() . '/images/products/thumbnail/' . $thumbnailCurrent)) {
                    File::delete(public_path() . '/images/products/thumbnail/' . $thumbnailCurrent);
                }
            }

            return sendResponse([], 'Delete product successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    public function countProduct() {
        $count = Product::get()->count();
        return sendResponse($count);
    }

    public function getSuperDeals(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'limit' => 'nullable|integer'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $limit = $request->limit ?? PHP_INT_MAX;
        $dateCurrent = Carbon::now()->format('Y-m-d H:i:s');

        $products = Product::join('product_promotions', 'products.id', '=', 'product_promotions.product_id')
            ->select('products.id', 'code', 'name', 'price', 'thumbnail', 'slug', 'new_price')
            ->where([
                ['products.is_show', 1],
                ['product_promotions.is_show', 1],
                ['new_price', '<>', 0]
            ])
            ->whereDate('start', '<=', $dateCurrent)
            ->whereDate('expire', '>=', $dateCurrent)
            ->orderByRaw('(price / new_price) * 100 DESC')
            ->orderBy('product_promotions.updated_at')
            ->limit($limit)->get();

        return sendResponse($products);
    }

    public function getHotProducts(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'limit' => 'nullable|integer'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $limit = $request->limit ?? PHP_INT_MAX;


        $products = Product::with(['promotion' => function ($query) {
            $dateCurrent = Carbon::now()->format('Y-m-d H:i:s');

            $query->select('new_price', 'product_id')
                ->whereDate('start', '<=', $dateCurrent)
                ->whereDate('expire', '>=', $dateCurrent)
                ->where('is_show', 1);
        }])->select('id', 'code', 'name', 'price', 'thumbnail', 'slug')
            ->withCount([
                'ratings as rating' => function ($query) {
                    $query->select(DB::raw('AVG(star) as rating'));
                }
            ])->orderBy('view', 'DESC')
            ->orderBy('sold', 'DESC')
            ->limit($limit)->get();

        foreach ($products as $product) {
            if ($product->promotion)
                $product->promotion->makeHidden('product_id');
        }


        return sendResponse($products);
    }


    public function getProductsBoxes(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $limit = $request->limit ?? PHP_INT_MAX;

        $categories = Category::select('id', 'parent_id')->whereIsShow(1)->get();

        $children = [];
        $parentCategories = explode(',', $request->parent_categories);

        if ($parentCategories[0] === "") $parentCategories = [];

        foreach ($parentCategories as $parentCategory) {

            $children[$parentCategory] = getAllChildren($categories, $parentCategory);
        }

        $data = [];

        foreach ($children as $categoryId => $child) {
            $categoryIdArray = [];

            foreach ($child as $item) {
                $categoryIdArray[] = $item->id;
            }

            $sellingProducts = $this->getSellingProducts($categoryIdArray, $limit);
            $newProducts = $this->getNewProducts($categoryIdArray, $limit);
            $promotionProducts = $this->getPromotionProducts($categoryIdArray, $limit);

            $data[$categoryId] = [
                'selling_products' => $sellingProducts,
                'new_products' => $newProducts,
                'promotion_products' => $promotionProducts
            ];
        }
        return sendResponse($data);
    }

    private function getSellingProducts($categoryIdArray, $limit)
    {
        $products = Product::with(['promotion' => function ($query) {
            $dateCurrent = Carbon::now()->format('Y-m-d H:i:s');

            $query->select('new_price', 'product_id')
                ->whereDate('start', '<=', $dateCurrent)
                ->whereDate('expire', '>=', $dateCurrent)
                ->where('is_show', 1);
        }])->select('id', 'code', 'name', 'price', 'thumbnail', 'slug')
            ->withCount([
                'ratings as rating' => function ($query) {
                    $query->select(DB::raw('AVG(star) as rating'));
                }
            ])->whereIn('category_id', $categoryIdArray)->orderBy('sold', 'DESC')->limit($limit)->get();

        return $products;
    }

    private function getNewProducts($categoryIdArray, $limit)
    {
        $products = Product::with(['promotion' => function ($query) {
            $dateCurrent = Carbon::now()->format('Y-m-d H:i:s');

            $query->select('new_price', 'product_id')
                ->whereDate('start', '<=', $dateCurrent)
                ->whereDate('expire', '>=', $dateCurrent)
                ->where('is_show', 1);
        }])->select('id', 'code', 'name', 'price', 'thumbnail', 'slug')
            ->withCount([
                'ratings as rating' => function ($query) {
                    $query->select(DB::raw('AVG(star) as rating'));
                }
            ])->whereIn('category_id', $categoryIdArray)->orderBy('created_at', 'DESC')->limit($limit)->get();

        return $products;
    }

    private function getPromotionProducts($categoryIdArray, $limit)
    {
        $dateCurrent = Carbon::now()->format('Y-m-d H:i:s');

        $products = Product::join('product_promotions', 'products.id', '=', 'product_promotions.product_id')
            ->select('products.id', 'code', 'name', 'price', 'thumbnail', 'slug', 'new_price')
            ->where([
                ['products.is_show', 1],
                ['product_promotions.is_show', 1],
                ['new_price', '<>', 0]
            ])
            ->whereDate('start', '<=', $dateCurrent)
            ->whereDate('expire', '>=', $dateCurrent)
            ->withCount([
                'ratings as rating' => function ($query) {
                    $query->select(DB::raw('AVG(star) as rating'));
                }
            ])->whereIn('category_id', $categoryIdArray)
            ->orderBy('product_promotions.updated_at')
            ->limit($limit)->get();

        return $products;
    }

    public function getBestSellingByCategory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $limit = $request->limit ?? PHP_INT_MAX;

        $categories = Category::select('id', 'parent_id')->whereIsShow(1)->get();
        $category = Category::select('id')->whereSlug($request->slug)->first();

        $children = getAllChildren($categories, $category->id);

        $categoryIdArray = [$category->id];
        foreach ($children as $child)
            $categoryIdArray[] = $child->id;


        $products = $this->getSellingProducts($categoryIdArray, $limit);

        return sendResponse($products);
    }

    public function getProductsByCategory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
            'slug' => 'required'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $offset = $request->offset ?? 0;
        $limit = $request->limit ?? PHP_INT_MAX;
        $slug = $request->slug;
        $sort = $request->sort_by ?? '';
        $orderArray = [];

        if ($sort) {
            $sortArray = explode(',', $sort);

            foreach ($sortArray as $sortItem) {
                if (strpos($sortItem, '-') === 0) {
                    $orderArray[substr($sortItem, 1)] = 'DESC';
                } else {
                    $orderArray[trim($sortItem)] = 'ASC';
                }
            }
        }
        if (count($orderArray) == 0)
            $orderArray['id'] = 'DESC';

        $categories = Category::select('id', 'parent_id')->whereIsShow(1)->get();
        $category = Category::select('id')->whereSlug($slug)->first();

        $children = getAllChildren($categories, $category->id);

        $categoryIdArray = [$category->id];
        foreach ($children as $child)
            $categoryIdArray[] = $child->id;

        $products = Product::with(['promotion' => function ($query) {
            $dateCurrent = Carbon::now()->format('Y-m-d H:i:s');

            $query->select('new_price', 'product_id')
                ->whereDate('start', '<=', $dateCurrent)
                ->whereDate('expire', '>=', $dateCurrent)
                ->where('is_show', 1);
        }])->select('id', 'code', 'name', 'price', 'thumbnail', 'slug')
            ->withCount([
                'ratings as rating' => function ($query) {
                    $query->select(DB::raw('AVG(star) as rating'));
                }
            ])->whereIn('category_id', $categoryIdArray)->offset($offset)->limit($limit);

        foreach ($orderArray as $orderColumn => $orderValue) {
            $products->orderBy($orderColumn, $orderValue);
        }

        $products = $products->get();

        $totalRows = Product::whereIn('category_id', $categoryIdArray)->get()->count();

        return sendResponse([
            'totalRows' => $totalRows,
            'products' => $products
        ]);
    }

    public function getBreadcrumb(Request $request)
    {
        $product = Product::select(['id', 'name', 'category_id', 'seo_title', 'seo_description'])->whereSlug($request->slug)->first();
        $categories = Category::select(['id', 'name', 'slug', 'parent_id'])->whereIsShow(1)->get();

        $categoryParents = getCategoryParents($categories, $product->category_id);
        $breadcrumb = array_reverse($categoryParents);
        $breadcrumb[] = $product;

        return sendResponse($breadcrumb);
    }

    public function getSimilarProducts(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $limit = $request->limit ?? PHP_INT_MAX;

        $product = Product::select('category_id')->whereSlug($request->slug)->first();

        $products = Product::with(['promotion' => function ($query) {
            $dateCurrent = Carbon::now()->format('Y-m-d H:i:s');

            $query->select('new_price', 'product_id')
                ->whereDate('start', '<=', $dateCurrent)
                ->whereDate('expire', '>=', $dateCurrent)
                ->where('is_show', 1);
        }])->select('id', 'code', 'name', 'price', 'thumbnail', 'slug')
            ->whereCategoryId($product->category_id)
            ->withCount([
                'ratings as rating' => function ($query) {
                    $query->select(DB::raw('AVG(star) as rating'));
                }
            ])->orderBy('view', 'DESC')
            ->orderBy(DB::raw('RAND()'))
            ->limit($limit)->get();

        foreach ($products as $product) {
            if ($product->promotion)
                $product->promotion->makeHidden('product_id');
        }

        return sendResponse($products);
    }

    public function getBestSellingProductSameCategory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $limit = $request->limit ?? PHP_INT_MAX;

        $product = Product::select('category_id')->whereSlug($request->slug)->first();

        $products = $this->getSellingProducts([$product->category_id], $limit);

        return sendResponse($products);
    }

    public function getProduct(Request $request)
    {
        $product = Product::with([
            'attributes' => function ($query) {
                $query->join('attributes', 'attribute_id', '=', 'attributes.id')
                    ->select('content', 'product_id', 'attributes.id', 'name');
            },
            'images' => function ($query) {
                $query->select('id', 'path', 'product_id')
                    ->where('is_show', 1);
            },
            'quantities' => function ($query) {
                $query->join('branches', 'branch_id', '=', 'branches.id')
                    ->join('wards', 'ward_id', '=', 'wards.id')
                    ->join('districts', 'district_id', '=', 'districts.id')
                    ->join('provinces', 'province_id', '=', 'provinces.id')
                    ->select([
                        'product_id',
                        'provinces.id as province_id',
                        'provinces.name as province_name',
                        'provinces.type as province_type',
                        'districts.name as district_name',
                        'districts.type as district_type',
                        'wards.name as ward_name',
                        'wards.type as ward_type',
                        'branches.name as branch_name',
                        'branches.address',
                        'product_quantities.id',
                        'quantity'] /*'branch_id', 'quantity', 'product_id', 'name'*/
                    /*'id', 'quantity', 'product_id', 'branch_id'*/)
                    ->where('quantity', '>', 0);
            },
            'promotion' => function ($query) {
                $dateCurrent = Carbon::now()->format('Y-m-d H:i:s');

                $query->select('new_price', 'product_id')
                    ->whereDate('start', '<=', $dateCurrent)
                    ->whereDate('expire', '>=', $dateCurrent)
                    ->where('is_show', 1);
            }
        ])
            ->select(['id', 'code', 'name', 'price', 'thumbnail', 'description', 'content', 'view', 'slug', 'seo_title', 'seo_description'])
            ->withCount([
                'ratings as rating' => function ($query) {
                    $query->select(DB::raw('AVG(star) as rating'));
                }
            ])->whereSlug($request->slug)
            ->whereIsShow(1)->first();


        if (!$product) return sendError('Not found');

        foreach ($product->attributes as $attribute) {
            $attribute->makeHidden('product_id');
        }

        foreach ($product->images as $image) {
            $image->makeHidden('product_id');
        }

        foreach ($product->quantities as $quantity) {
            $quantity->makeHidden('product_id');
        }

        if ($product->promotion)
            $product->promotion->makeHidden('product_id');

        // update view
        Product::find($product->id)->increment('view');

        return sendResponse($product->makeHidden(['user_id', 'category_id', 'brand_id']));
    }

    public function searchProducts(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $offset = $request->offset ?? 0;
        $limit = $request->limit ?? PHP_INT_MAX;
        $keyword = $request->keyword;
        $sort = $request->sort_by ?? '';
        $categoryId = $request->category_id ?? 0;
        $orderArray = [];

        if ($sort) {
            $sortArray = explode(',', $sort);

            foreach ($sortArray as $sortItem) {
                if (strpos($sortItem, '-') === 0) {
                    $orderArray[substr($sortItem, 1)] = 'DESC';
                } else {
                    $orderArray[trim($sortItem)] = 'ASC';
                }
            }
        }
        if (count($orderArray) == 0)
            $orderArray['id'] = 'DESC';
        $totalRows = Product::where('name', 'LIKE', "%$keyword%");

        $products = Product::with(['promotion' => function ($query) {
            $dateCurrent = Carbon::now()->format('Y-m-d H:i:s');

            $query->select('new_price', 'product_id')
                ->whereDate('start', '<=', $dateCurrent)
                ->whereDate('expire', '>=', $dateCurrent)
                ->where('is_show', 1);
        }])->select('id', 'code', 'name', 'price', 'thumbnail', 'slug', 'description')
            ->withCount([
                'ratings as rating' => function ($query) {
                    $query->select(DB::raw('AVG(star) as rating'));
                }
            ])->where('name', 'LIKE', "%$keyword%");

        if ($categoryId != 0) {
            $categories = Category::select('id', 'parent_id')->whereIsShow(1)->get();

            $children = getAllChildren($categories, $categoryId);
            $categoryIdArray = [$categoryId];
            foreach ($children as $child)
                $categoryIdArray[] = $child->id;

            $totalRows->whereIn('category_id', $categoryIdArray);
            $products->whereIn('category_id', $categoryIdArray);
        }

        foreach ($orderArray as $orderColumn => $orderValue) {
            $products->orderBy($orderColumn, $orderValue);
        }

        $products->offset($offset)->limit($limit);
        $products = $products->get();

        $totalRows = $totalRows->get()->count();

        return sendResponse([
            'totalRows' => $totalRows,
            'products' => $products
        ]);
    }

    /*function test()
    {
        set_time_limit(0);

        $products = Product::with(['category', 'images'])->select(['id', 'thumbnail', 'category_id'])->get();
        foreach ($products as $product) {
            $folderName = $product->category->id;

            if (!File::exists(public_path('images/products/detail/') . $folderName)) {
                File::makeDirectory(public_path('images/products/detail/') . $folderName);
            }

            if (!File::exists(public_path('images/products/thumbnail/') . $folderName)) {
                File::makeDirectory(public_path('images/products/thumbnail/') . $folderName);
            }

            if (file_exists(public_path('images/products/dummy/thumbnail/') . $product->getOriginal('thumbnail'))) {
                copy(public_path('images/products/dummy/thumbnail/') . $product->getOriginal('thumbnail'), public_path('images/products/thumbnail/' . $folderName . '/') . $product->getOriginal('thumbnail'));
                File::delete(public_path('images/products/dummy/thumbnail/') . $product->getOriginal('thumbnail'));
            }


            DB::table('products')
                ->where('id', $product->id)
                ->update(['thumbnail' => $folderName . '/' . $product->getOriginal('thumbnail')]);

            foreach ($product->images as $image) {
                if (file_exists(public_path('images/products/dummy/detail/') . $image->path)) {
                    copy(public_path('images/products/dummy/detail/') . $image->path, public_path('images/products/detail/' . $folderName . '/') . $image->path);
                    File::delete(public_path('images/products/dummy/detail/') . $image->path);
                }


                DB::table('product_images')
                    ->where('id', $image->id)
                    ->update(['path' => $folderName . '/' . $image->path]);
            }
        }
//        return response($products);

    }*/
}
