<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $offset = $request->offset ?? 0;
        $limit = $request->limit ?? PHP_INT_MAX;
        $keyword = $request->keyword ?? '';
        $sort = $request->sort_by ?? '';
        $orderArray = [];

        if ($sort) {
            $sortArray = explode(',', $sort);

            foreach ($sortArray as $sortItem) {
                if (strpos($sortItem, '-') === 0) {
                    $orderArray[substr($sortItem, 1)] = 'DESC';
                } else {
                    $orderArray[trim($sortItem)] = 'ASC';
                }
            }
        }

        $customer = Customer::where('name', 'LIKE', "%$keyword%")->offset($offset)->limit($limit);

        foreach ($orderArray as $orderColumn => $orderValue) {
            $customer->orderBy($orderColumn, $orderValue);
        }

        $customer = $customer->get();

        $totalRows = Customer::where('name', 'LIKE', "%$keyword%")->get()->count();

        return sendResponseBasic([
            'totalRows' => $totalRows,
            'customers' => $customer
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required',
            'email' => 'required|email|unique:customers,email',
            'password' => 'required|confirmed|min:6',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $data['password'] = bcrypt($data['password']);

        unset($data['password_confirmation']);

        try {
            $customer = Customer::create($data);

            return sendResponseBasic($customer, 'Create customer successfully', 201);
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ['id' => $id];

        $validator = Validator::make($data, [
            'id' => 'required|integer|exists:customers'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $customer = Customer::find($id);

        return sendResponseBasic($customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except(['email']);
        $data['id'] = $id;

        $validator = Validator::make($data, [
            'id' => 'required|integer|exists:customers',
            'name' => 'required',
            'password' => 'nullable|confirmed|min:6',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        if (!empty($data['password']))
            $data['password'] = bcrypt($data['password']);
        else unset($data['password']);

        unset($data['password_confirmation']);
        unset($data['id']);

        try {
            $customer = Customer::find($id);
            $customer->update($data);

            return sendResponseBasic($customer, 'Update customer successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = ['id' => $id];

        $validator = Validator::make($data, [
            'id' => 'required|integer|exists:customers'
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        $customer = Customer::find($id);

        if (!$customer) return sendError('Customer not found', [], 400);

        try {
            $customer->delete();

            return sendResponseBasic([], 'Delete customer successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    public function updateByToken(Request $request)
    {
        $customerId = auth('customer')->user()->id;

        $customer = Customer::find($customerId);
//        if (!$customer) return sendError('Customer not found', [], 400);

        $data = $request->except(['email']);

        $validator = Validator::make($data, [
            'name' => 'required',
            'password' => 'nullable|confirmed|min:6',
        ]);

        if ($validator->fails()) return sendError('Validate error', $validator->errors(), 422);

        if (!empty($data['password']))
            $data['password'] = bcrypt($data['password']);
        else unset($data['password']);

        unset($data['password_confirmation']);

        try {
            $customer->update($data);

            return sendResponse($customer, 'Update customer successfully');
        } catch (QueryException $exception) {
            return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }
    }

    public function countCustomer() {
        $count = Customer::get()->count();
        return sendResponse($count);
    }
}
