<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Facades\JWTAuth;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

//        $time = date('d-m-Y H:i:s', auth('customer')->payload()('exp'));
//        dd(auth('customer')->check());
//        if(!auth()->check())
//            return sendError('Token invalid', [], 401);
        return $next($request);
    }
}
