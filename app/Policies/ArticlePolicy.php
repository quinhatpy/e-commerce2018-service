<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Article;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the blog.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->hasAccess(['article.index']);
    }

    /**
     * Determine whether the user can view the blog.
     *
     * @param  \App\Models\User $user
     * @param $id
     * @return mixed
     */
    public function show(User $user, $id)
    {
        $model = Article::find($id);
        if (!$model) return true; // continue controller

        return $user->hasAccess(['article.show']) or $user->id == $model->user_id;
    }

    /**
     * Determine whether the user can create blogs.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAccess(['article.create']);
    }

    /**
     * Determine whether the user can update the blog.
     *
     * @param  \App\Models\User $user
     * @param $id
     * @return mixed
     */
    public function update(User $user, $id)
    {
        $model = Article::find($id);
        if (!$model) return true;

        return $user->hasAccess(['article.update']) or $user->id == $model->user_id;
    }

    /**
     * Determine whether the user can delete the blog.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Article  $blog
     * @return mixed
     */
    public function delete(User $user, $id)
    {
        $model = Article::find($id);
        if (!$model) return true;

        return $user->hasAccess(['article.delete']) or $user->id == $model->user_id;
    }

    /**
     * Determine whether the user can restore the blog.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Article  $blog
     * @return mixed
     */
    public function restore(User $user, Article $blog)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the blog.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Article  $blog
     * @return mixed
     */
    public function forceDelete(User $user, Article $blog)
    {
        //
    }
}
