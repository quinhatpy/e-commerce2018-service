<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Order;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->hasAccess(['order.index']);
    }

    /**
     * Determine whether the user can view the order.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function show(User $user)
    {
        return $user->hasAccess(['order.show']);
    }

    /**
     * Determine whether the user can create orders.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAccess(['order.create']);
    }

    /**
     * Determine whether the user can update the order.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->hasAccess(['order.update']);
    }

    /**
     * Determine whether the user can delete the order.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->hasAccess(['order.delete']);
    }

    /**
     * Determine whether the user can restore the order.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Order $order
     * @return mixed
     */
    public function restore(User $user, Order $order)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the order.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Order $order
     * @return mixed
     */
    public function forceDelete(User $user, Order $order)
    {
        //
    }
}
