<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Gallery;
use Illuminate\Auth\Access\HandlesAuthorization;

class GalleryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the galleries.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->hasAccess(['gallery.index']);
    }

    /**
     * Determine whether the user can view the gallery.
     *
     * @param  \App\Models\User $user
     * @param $id
     * @return mixed
     */
    public function view(User $user, $id)
    {
        $model = Gallery::find($id);
        if(!$model) return true; // continue controller

        return $user->hasAccess(['gallery.show']) or $user->id == $model->user_id;
    }

    /**
     * Determine whether the user can create galleries.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAccess(['gallery.create']);
    }

    /**
     * Determine whether the user can update the gallery.
     *
     * @param  \App\Models\User $user
     * @param $id
     * @return mixed
     */
    public function update(User $user, $id)
    {
        $model = Gallery::find($id);
        if(!$model) return true;

        return $user->hasAccess(['gallery.update']) or $user->id == $model->user_id;
    }

    /**
     * Determine whether the user can delete the gallery.
     *
     * @param  \App\Models\User $user
     * @param $id
     * @return mixed
     */
    public function delete(User $user, $id)
    {
        $model = Gallery::find($id);
        if (!$model) return true;

        return $user->hasAccess(['gallery.delete']) and $user->id == $model->user_id;
    }

    /**
     * Determine whether the user can restore the gallery.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Gallery  $gallery
     * @return mixed
     */
    public function restore(User $user, Gallery $gallery)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the gallery.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Gallery  $gallery
     * @return mixed
     */
    public function forceDelete(User $user, Gallery $gallery)
    {
        //
    }
}
