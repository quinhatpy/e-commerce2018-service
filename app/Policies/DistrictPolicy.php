<?php

namespace App\Policies;

use App\Models\User;
use App\Models\District;
use Illuminate\Auth\Access\HandlesAuthorization;

class DistrictPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->hasAccess(['district.index']);
    }

    /**
     * Determine whether the user can view the category.
     *
     * @param  \App\Models\User $user
     * @param $id
     * @return mixed
     */
    public function show(User $user)
    {
        return $user->hasAccess(['district.show']);
    }

    /**
     * Determine whether the user can create categories.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAccess(['district.create']);
    }

    /**
     * Determine whether the user can update the category.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->hasAccess(['district.update']);
    }

    /**
     * Determine whether the user can delete the category.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->hasAccess(['district.delete']);
    }

    /**
     * Determine whether the user can restore the district.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\District $district
     * @return mixed
     */
    public function restore(User $user, District $district)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the district.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\District $district
     * @return mixed
     */
    public function forceDelete(User $user, District $district)
    {
        //
    }
}
