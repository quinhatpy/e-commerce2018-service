<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Banner;
use Illuminate\Auth\Access\HandlesAuthorization;

class BannerPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->hasAccess(['banner.index']);
    }

    /**
     * Determine whether the user can view the category.
     *
     * @param  \App\Models\User $user
     * @param $id
     * @return mixed
     */
    public function show(User $user)
    {
        return $user->hasAccess(['banner.show']);
    }

    /**
     * Determine whether the user can create categories.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAccess(['banner.create']);
    }

    /**
     * Determine whether the user can update the category.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->hasAccess(['banner.update']);
    }

    /**
     * Determine whether the user can delete the category.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->hasAccess(['banner.delete']);
    }

    /**
     * Determine whether the user can restore the banner.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Banner  $banner
     * @return mixed
     */
    public function restore(User $user, Banner $banner)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the banner.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Banner  $banner
     * @return mixed
     */
    public function forceDelete(User $user, Banner $banner)
    {
        //
    }
}
