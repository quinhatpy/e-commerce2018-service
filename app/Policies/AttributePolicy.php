<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Attribute;
use Illuminate\Auth\Access\HandlesAuthorization;

class AttributePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->hasAccess(['attribute.index']);
    }

    /**
     * Determine whether the user can view the attribute.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function show(User $user)
    {
        return $user->hasAccess(['attribute.show']);
    }

    /**
     * Determine whether the user can create attributes.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAccess(['attribute.create']);
    }

    /**
     * Determine whether the user can update the attribute.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->hasAccess(['attribute.update']);
    }

    /**
     * Determine whether the user can delete the attribute.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->hasAccess(['attribute.delete']);
    }

    /**
     * Determine whether the user can restore the attribute.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Attribute $attribute
     * @return mixed
     */
    public function restore(User $user, Attribute $attribute)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the attribute.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Attribute $attribute
     * @return mixed
     */
    public function forceDelete(User $user, Attribute $attribute)
    {
        //
    }
}
