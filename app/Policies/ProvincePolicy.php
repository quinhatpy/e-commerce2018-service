<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Province;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProvincePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->hasAccess(['province.index']);
    }

    /**
     * Determine whether the user can view the category.
     *
     * @param  \App\Models\User $user
     * @param $id
     * @return mixed
     */
    public function show(User $user)
    {
        return $user->hasAccess(['province.show']);
    }

    /**
     * Determine whether the user can create categories.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAccess(['province.create']);
    }

    /**
     * Determine whether the user can update the category.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->hasAccess(['province.update']);
    }

    /**
     * Determine whether the user can delete the category.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->hasAccess(['province.delete']);
    }

    /**
     * Determine whether the user can restore the province.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Province  $province
     * @return mixed
     */
    public function restore(User $user, Province $province)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the province.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Province  $province
     * @return mixed
     */
    public function forceDelete(User $user, Province $province)
    {
        //
    }
}
