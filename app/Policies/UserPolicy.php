<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->hasAccess(['user.index']);
    }


    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User $user
     * @param $id
     * @return mixed
     */
    public function show(User $user, $id)
    {
        $model = User::find($id);
        if (!$model) return true; // continue controller

        return $user->hasAccess(['user.show']) or $user->id == $model->id;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAccess(['user.create']);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User $user
     * @param $id
     * @return mixed
     */
    public function update(User $user, $id)
    {
        $model = User::find($id);
        if (!$model) return true;

        return $user->hasAccess(['user.update']) or $user->id == $model->id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User $user
     * @param  \App\User $model
     * @return mixed
     */
    public function delete(User $user, $id)
    {
        $model = User::find($id);
        if (!$model) return true;

        return $user->hasAccess(['user.delete']) and $user->id != $model->id;
    }
}
