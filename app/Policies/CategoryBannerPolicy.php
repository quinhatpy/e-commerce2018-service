<?php

namespace App\Policies;

use App\Models\User;
use App\Models\CategoryBanner;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryBannerPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->hasAccess(['category_banner.index']);
    }

    /**
     * Determine whether the user can view the category.
     *
     * @param  \App\Models\User $user
     * @param $id
     * @return mixed
     */
    public function show(User $user)
    {
        return $user->hasAccess(['category_banner.show']);
    }

    /**
     * Determine whether the user can create categories.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasAccess(['category_banner.create']);
    }

    /**
     * Determine whether the user can update the category.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->hasAccess(['category_banner.update']);
    }

    /**
     * Determine whether the user can delete the category.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->hasAccess(['category_banner.delete']);
    }

    /**
     * Determine whether the user can restore the category banner.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\CategoryBanner  $categoryBanner
     * @return mixed
     */
    public function restore(User $user, CategoryBanner $categoryBanner)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the category banner.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\CategoryBanner  $categoryBanner
     * @return mixed
     */
    public function forceDelete(User $user, CategoryBanner $categoryBanner)
    {
        //
    }
}
