<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryBanner extends Model
{
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function getImageAttribute($value)
    {
        if(!$value)
            return url('/public/images/not-image.png');
        if (file_exists(public_path() . '/images/categories/banners/' . $value))
            return url('/public/images/categories/banners/' . $value);
        return url('/public/images/not-image.png');
    }

    public function setLinkAttribute($value)
    {
        $this->attributes['link'] = $value;
        if ($value == null) $this->attributes['link'] = '';
    }

    public function setIsShowAttribute($value)
    {
        $this->attributes['is_show'] = $value;
        if ($value == null) $this->attributes['is_show'] = 0;
    }

    public function setIsMainAttribute($value)
    {
        $this->attributes['is_main'] = $value;
        if ($value == null) $this->attributes['is_main'] = 0;
    }
}
