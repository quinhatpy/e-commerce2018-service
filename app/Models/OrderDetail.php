<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $guarded = [];

//    protected $hidden = ['product_id'];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
