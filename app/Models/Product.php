<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];

    public function getThumbnailAttribute($value)
    {
        if(!$value)
            return url('/public/images/not-image.png');
        if (file_exists(public_path() . '/images/products/thumbnail/' . $value))
            return url('/public/images/products/thumbnail/' . $value);
        return url('/public/images/not-image.png');
    }

    public function setThumbnailAttribute($value)
    {
        $this->attributes['thumbnail'] = $value;
        if ($value == null) $this->attributes['thumbnail'] = '';
    }

    public function setIsShowAttribute($value)
    {
        $this->attributes['is_show'] = $value;
        if ($value == null) $this->attributes['is_show'] = 0;
    }

    public function setSeoTitleAttribute($value)
    {
        $this->attributes['seo_title'] = $value;
        if ($value == null) $this->attributes['seo_title'] = '';
    }

    public function setSeoDescriptionAttribute($value)
    {
        $this->attributes['seo_description'] = $value;
        if ($value == null) $this->attributes['seo_description'] = '';
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function attributes()
    {
        return $this->hasMany(ProductAttribute::class);
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function quantities()
    {
        return $this->hasMany(ProductQuantity::class);
    }

    public function promotion()
    {
        return $this->hasOne(ProductPromotion::class);
    }

    public function ratings()
    {
        return $this->hasMany(ProductRating::class);
    }
}
