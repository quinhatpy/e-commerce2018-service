<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];


    public function getIconAttribute($value)
    {
        if ($value)
            return url('/public/images/categories/icons/' . $value);
        return $value;
    }


    public function setIconAttribute($value)
    {
        $this->attributes['icon'] = $value;
        if ($value == null) $this->attributes['icon'] = '';
    }

    public function setIsShowAttribute($value)
    {
        $this->attributes['is_show'] = $value;
        if ($value == null) $this->attributes['is_show'] = 0;
    }

    public function setSeoTitleAttribute($value)
    {
        $this->attributes['seo_title'] = $value;
        if ($value == null) $this->attributes['seo_title'] = '';
    }

    public function setSeoDescriptionAttribute($value)
    {
        $this->attributes['seo_description'] = $value;
        if ($value == null) $this->attributes['seo_description'] = '';
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }
}
