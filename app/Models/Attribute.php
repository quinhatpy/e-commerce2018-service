<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function setIsRequiredAttribute($value)
    {
        $this->attributes['is_required'] = $value;
        if ($value == null) $this->attributes['is_required'] = 0;
    }

    public function setDefaultValueAttribute($value)
    {
        $this->attributes['default_value'] = $value;
        if ($value == null) $this->attributes['default_value'] = '';
    }

    public function setIsShowAttribute($value)
    {
        $this->attributes['is_show'] = $value;
        if ($value == null) $this->attributes['is_show'] = 0;
    }
}
