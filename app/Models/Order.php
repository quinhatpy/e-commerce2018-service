<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];

    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = $value;
        if ($value == null) $this->attributes['status'] = 1;
    }

    public function setIsPaidAttribute($value)
    {
        $this->attributes['is_paid'] = $value;
        if ($value == null) $this->attributes['is_paid'] = 0;
    }

    public function setNoteAttribute($value)
    {
        $this->attributes['note'] = $value;
        if ($value == null) $this->attributes['note'] = '';
    }
    public function setReceiverAddressAttribute($value)
    {
        $this->attributes['receiver_address'] = $value;
        if ($value == null) $this->attributes['receiver_address'] = '';
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function order_details()
    {
        return $this->hasMany(OrderDetail::class);
    }
}
