<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $guarded = [];

    public function getLogoAttribute($value)
    {
        if(!$value)
            return url('/public/images/not-image.png');
        if (file_exists(public_path() . '/images/brands/' . $value))
            return url('/public/images/brands/' . $value);
        return url('/public/images/not-image.png');
    }

    public function setIsShowAttribute($value)
    {
        $this->attributes['is_show'] = $value;
        if ($value == null) $this->attributes['is_show'] = 0;
    }

    public function setSeoTitleAttribute($value)
    {
        $this->attributes['seo_title'] = $value;
        if ($value == null) $this->attributes['seo_title'] = '';
    }

    public function setSeoDescriptionAttribute($value)
    {
        $this->attributes['seo_description'] = $value;
        if ($value == null) $this->attributes['seo_description'] = '';
    }

    public function products() {
        return $this->hasMany(Product::class);
    }
}
