<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $guarded = [];

    public function getThumbnailAttribute($value)
    {
        if ($value)
            return url('/public/images/articles/' . $value);
        return $value;
    }

//    public function setThumbnailAttribute($value)
//    {
//        $this->attributes['thumbnail'] = $value;
//        if ($value == null) $this->attributes['thumbnail'] = '';
//    }


    public function getShortDescriptionAttribute($value)
    {
        if (!$value) return '';
        return $value;
    }

    public function setShortDescriptionAttribute($value)
    {
        $this->attributes['short_description'] = $value;
        if ($value == null) $this->attributes['short_description'] = '';
    }

    public function setIsShowAttribute($value)
    {
        $this->attributes['is_show'] = $value;
        if ($value == null) $this->attributes['is_show'] = 0;
    }

    public function setSeoTitleAttribute($value)
    {
        $this->attributes['seo_title'] = $value;
        if ($value == null) $this->attributes['seo_title'] = '';
    }

    public function setSeoDescriptionAttribute($value)
    {
        $this->attributes['seo_description'] = $value;
        if ($value == null) $this->attributes['seo_description'] = '';
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
