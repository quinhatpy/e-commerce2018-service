<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    protected $guarded = [];

    public function setContentAttribute($value)
    {
        $this->attributes['content'] = $value;
        if ($value == null) $this->attributes['content'] = '';
    }
}
