<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use NotificationChannels\WebPush\HasPushSubscriptions;

class Customer extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use HasPushSubscriptions;

    protected $guarded = [];
    protected $hidden = [
        'password', 'remember_token', 'activation_token'
    ];

    public function setAddressAttribute($value)
    {
        $this->attributes['address'] = $value;
        if ($value == null) $this->attributes['address'] = '';
    }

    public function getAddressAttribute($value)
    {
        if ($value == null) return '';
        return $value;
    }

    public function setPhoneAttribute($value)
    {
        $this->attributes['phone'] = $value;
        if ($value == null) $this->attributes['phone'] = '';
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
