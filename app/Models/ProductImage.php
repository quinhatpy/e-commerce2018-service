<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $guarded = [];

    public function getPathAttribute($value)
    {
        if(!$value)
            return url('/public/images/not-image.png');
        if (file_exists(public_path() . '/images/products/detail/' . $value))
            return url('/public/images/products/detail/' . $value);
        return url('/public/images/not-image.png');
    }
}
