<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

//    protected $dates = ['deleted_at'];

//    protected $appends = ['role'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [
//        'name', 'email', 'password', 'email_verified_at', 'activation_token'
//    ];
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'activation_token', 'role_id'
    ];

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }


    /**
     * Checks if User has access to $permissions.
     * @param array $permissions
     * @return bool
     */
    public function hasAccess(array $permissions): bool
    {

        // check if the permission is available in any role

        if ($this->role->hasAccess($permissions)) {
            return true;
        }
        return false;
    }

    /**
     * Checks if the user belongs to role.
     * @param string $roleId
     * @return bool
     */
    public function inRole(string $roleId)
    {
        return $this->roles()->whereId($roleId)->count() == 1;
    }


    public function getAvatarAttribute($value)
    {
        if ($value)
            return url('/public/images/users/' . $value);
        return $value;
    }

//
//    public function getRoleAttribute()
//    {
//        return $this->withRole;
//    }

    public function setPhoneAttribute($value)
    {
        $this->attributes['phone'] = $value;
        if ($value == null) $this->attributes['phone'] = '';
    }

    public function setAddressAttribute($value)
    {
        $this->attributes['address'] = $value;
        if ($value == null) $this->attributes['address'] = '';
    }


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
