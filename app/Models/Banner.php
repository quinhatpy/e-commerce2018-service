<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $guarded = [];

    public function getImageAttribute($value)
    {
        if(!$value)
            return url('/public/images/not-image.png');
        if (file_exists(public_path() . '/images/banners/' . $value))
            return url('/public/images/banners/' . $value);
        return url('/public/images/not-image.png');
    }

    public function setLinkAttribute($value)
    {
        $this->attributes['link'] = $value;
        if ($value == null) $this->attributes['link'] = '';
    }

    public function setIsShowAttribute($value)
    {
        $this->attributes['is_show'] = $value;
        if ($value == null) $this->attributes['is_show'] = 0;
    }
}
