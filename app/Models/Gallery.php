<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $guarded = [];

    public function getPathAttribute($value)
    {
        if(!$value)
            return url('/public/images/not-image.png');
        if (file_exists(public_path() . '/uploads/' . $value))
            return url('/public/uploads/' . $value);
        return url('/public/images/not-image.png');
    }

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        if ($value == null) $this->attributes['title'] = '';
    }

    public function setAltAttribute($value)
    {
        $this->attributes['alt'] = $value;
        if ($value == null) $this->attributes['alt'] = '';
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
