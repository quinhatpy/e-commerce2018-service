<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $guarded = [];

    public function getIconAttribute($value)
    {
        if(!$value)
            return url('/public/images/not-image.png');
        if (file_exists(public_path() . '/images/menus/' . $value))
            return url('/public/images/menus/' . $value);
        return url('/public/images/not-image.png');
    }


    public function setIconAttribute($value)
    {
        $this->attributes['icon'] = $value;
        if ($value == null) $this->attributes['icon'] = '';
    }

    public function setIsShowAttribute($value)
    {
        $this->attributes['is_show'] = $value;
        if ($value == null) $this->attributes['is_show'] = 0;
    }


    public function parent()
    {
        return $this->belongsTo(Menu::class, 'parent_id');
    }
}
