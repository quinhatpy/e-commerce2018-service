<?php
/**
 * Created by PhpStorm.
 * User: NhatLe
 * Date: 01-Oct-18
 * Time: 10:19
 */


/**
 * Success response method
 * @param $result
 * @param $message
 * @param int $code
 * @return \Illuminate\Http\JsonResponse
 */
function sendResponse($result, $message = '', $code = 200)
{
    $response = [
        'success' => true,
        'result' => $result,
        'message' => $message
    ];

    return response()->json($response, $code, [], JSON_NUMERIC_CHECK);
}

/**
 * Success response method
 * @param $result
 * @param $message
 * @param int $code
 * @return \Illuminate\Http\JsonResponse
 */
function sendResponseBasic($result, $message = '', $code = 200)
{
    $response = [
        'success' => true,
        'result' => $result,
        'message' => $message
    ];

    return response()->json($response, $code);
}

/**
 * Error response method
 * @param $error
 * @param array $errorMessage
 * @param int $code
 * @return \Illuminate\Http\JsonResponse
 */
function sendError($error, $errorMessage = [], $code = 404)
{
    $response = [
        'success' => false,
        'message' => $error
    ];

    if (!empty($errorMessage)) {
        $response['data'] = $errorMessage;
    }

    return response()->json($response, $code);
}

function getAllChildren($categories, $parentId)
{
    $children = [];
    // grab the children
    $posts = [];
    foreach ($categories as $category) {
        if ($category->parent_id == $parentId)
            $posts[] = $category;
    }

    // now grab the grand children
    foreach ($posts as $child) {
        // recursion!! hurrah
        $gchildren = getAllChildren($categories, $child->id);

        // merge the grand children into the children array
        if (!empty($gchildren)) {
            $children = array_merge($children, $gchildren);
        }
    }

    // merge in the direct descendants we found earlier
    $children = array_merge($children, $posts);
    return $children;
}

function getCategoryParents($categories, $parentId)
{
    $parents = [];
    foreach ($categories as $category) {
        if ($category->id == $parentId) {
            $parents[] = $category;
            $data = getCategoryParents($categories, $category->parent_id);
            $parents = array_merge($parents, $data);
        }
    }

    return $parents;
}