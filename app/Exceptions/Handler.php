<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $contentType = $request->getContentType();
//        if ($contentType === 'json' && $exception->getMessage() === 'Unauthenticated.')
//            return sendError('Authenticated user', [], 401);

        if ($contentType === 'json') {
            if ($exception instanceof AuthenticationException) {
                return sendError($exception->getMessage(), [], 401);
            }
            if ($exception instanceof AuthorizationException)
                return sendError($exception->getMessage(), [], 403);
            if ($exception instanceof QueryException)
                return sendError('Query error', ['errorInfo' => [$exception->errorInfo[2]]], 500);
        }

        if ($exception instanceof UnauthorizedHttpException) {
            $preException = $exception->getPrevious();
            if ($preException instanceof
                \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                return sendError($preException->getMessage(), [], 401);
//                return response()->json(['error' => 'TOKEN_EXPIRED']);
            } else if ($preException instanceof
                \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return sendError($preException->getMessage(), [], 401);
//                return response()->json(['error' => 'TOKEN_INVALID']);
            } else if ($preException instanceof
                \Tymon\JWTAuth\Exceptions\TokenBlacklistedException) {
                return sendError($preException->getMessage(), [], 401);
//                return response()->json(['error' => 'TOKEN_BLACKLISTED']);
            }
            if ($exception->getMessage() === 'Token not provided') {
                return sendError($exception->getMessage(), [], 401);
//                return response()->json(['error' => 'Token not provided']);
            }
        }


        return parent::render($request, $exception);
    }
}
