<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use App\Events\MessagePosted;
use App\Models\User;
use App\Notifications\MessageNotification;

Route::prefix('v1')->group(function () {
    /**
     * Authentication
     */
    Route::group([
        'namespace' => 'UserAuth',
        'prefix' => 'auth',
    ], function () {
//        Route::post('register', 'RegisterController@register');
//        Route::get('register/activate/{token}', 'RegisterController@registerActivate');
        Route::post('login', 'AuthController@login');
        Route::get('logout', 'AuthController@logout')->middleware('jwt.auth');
        Route::prefix('password')->group(function () {
            Route::post('create', 'PasswordResetController@create');
            Route::get('find/{token}', 'PasswordResetController@find');
            Route::post('reset', 'PasswordResetController@reset');
        });
        Route::get('user', 'AuthController@getUser')->middleware('auth:api');
    });


    /**
     * Middleware user
     */
    Route::middleware('auth:api')->group(function () {
        Route::put('profile', 'UserController@updateProfile');

        Route::get('users', 'UserController@index')->middleware(['can:user.index']);
        Route::get('users/{id}', 'UserController@show')->middleware(['can:user.show, id']);
        Route::post('users', 'UserController@store')->middleware(['can:user.create']);
        Route::put('users/{id}', 'UserController@update')->middleware(['can:user.update,id']);
        Route::delete('users/{id}', 'UserController@destroy')->middleware(['can:user.delete,id']);

        Route::get('roles', 'RoleController@index')->middleware(['can:role.index']);
        Route::get('roles/{id}', 'RoleController@show')->middleware(['can:role.show']);
        Route::post('roles', 'RoleController@store')->middleware(['can:role.create']);
        Route::put('roles/{id}', 'RoleController@update')->middleware(['can:role.update']);
        Route::delete('roles/{id}', 'RoleController@destroy')->middleware(['can:role.delete']);

        Route::get('customers', 'CustomerController@index')->middleware(['can:customer.index']);
        Route::get('customers/{id}', 'CustomerController@show')->middleware(['can:customer.show']);
        Route::post('customers', 'CustomerController@store')->middleware(['can:customer.create']);
        Route::put('customers/{id}', 'CustomerController@update')->middleware(['can:customer.update']);
        Route::delete('customers/{id}', 'CustomerController@destroy')->middleware(['can:customer.delete']);
        Route::get('customers-count', 'CustomerController@countCustomer');

        Route::get('categories', 'CategoryController@index')->middleware(['can:category.index']);
        Route::get('categories/{id}', 'CategoryController@show')->middleware(['can:category.show']);
        Route::post('categories', 'CategoryController@store')->middleware(['can:category.create']);
        Route::put('categories/{id}', 'CategoryController@update')->middleware(['can:category.update']);
        Route::delete('categories/{id}', 'CategoryController@destroy')->middleware(['can:category.delete']);

        Route::get('category_banners', 'CategoryBannerController@index')->middleware(['can:category_banner.index']);
        Route::get('category_banners/{id}', 'CategoryBannerController@show')->middleware(['can:category_banner.show']);
        Route::post('category_banners', 'CategoryBannerController@store')->middleware(['can:category_banner.create']);
        Route::put('category_banners/{id}', 'CategoryBannerController@update')->middleware(['can:category_banner.update']);
        Route::delete('category_banners/{id}', 'CategoryBannerController@destroy')->middleware(['can:category_banner.delete']);

        Route::get('attributes', 'AttributeController@index')->middleware(['can:attribute.index']);
        Route::get('attributes/{id}', 'AttributeController@show')->middleware(['can:attribute.show']);
        Route::post('attributes', 'AttributeController@store')->middleware(['can:attribute.create']);
        Route::put('attributes/{id}', 'AttributeController@update')->middleware(['can:attribute.update']);
        Route::delete('attributes/{id}', 'AttributeController@destroy')->middleware(['can:attribute.delete']);

        Route::get('banners', 'BannerController@index')->middleware(['can:banner.index']);
        Route::get('banners/{id}', 'BannerController@show')->middleware(['can:banner.show']);
        Route::post('banners', 'BannerController@store')->middleware(['can:banner.create']);
        Route::put('banners/{id}', 'BannerController@update')->middleware(['can:banner.update']);
        Route::delete('banners/{id}', 'BannerController@destroy')->middleware(['can:banner.delete']);

        Route::get('brands', 'BrandController@index')->middleware(['can:brand.index']);
        Route::get('brands/{id}', 'BrandController@show')->middleware(['can:brand.show']);
        Route::post('brands', 'BrandController@store')->middleware(['can:brand.create']);
        Route::put('brands/{id}', 'BrandController@update')->middleware(['can:brand.update']);
        Route::delete('brands/{id}', 'BrandController@destroy')->middleware(['can:brand.delete']);

        Route::get('provinces', 'ProvinceController@index')->middleware(['can:province.index']);
        Route::get('provinces/{id}', 'ProvinceController@show')->middleware(['can:province.show']);
        Route::post('provinces', 'ProvinceController@store')->middleware(['can:province.create']);
        Route::put('provinces/{id}', 'ProvinceController@update')->middleware(['can:province.update']);
        Route::delete('provinces/{id}', 'ProvinceController@destroy')->middleware(['can:province.delete']);

        Route::get('districts', 'DistrictController@index')->middleware(['can:district.index']);
        Route::get('districts/{id}', 'DistrictController@show')->middleware(['can:district.show']);
        Route::post('districts', 'DistrictController@store')->middleware(['can:district.create']);
        Route::put('districts/{id}', 'DistrictController@update')->middleware(['can:district.update']);
        Route::delete('districts/{id}', 'DistrictController@destroy')->middleware(['can:district.delete']);

        Route::get('wards', 'WardController@index')->middleware(['can:ward.index']);
        Route::get('wards/{id}', 'WardController@show')->middleware(['can:ward.show']);
        Route::post('wards', 'WardController@store')->middleware(['can:ward.create']);
        Route::put('wards/{id}', 'WardController@update')->middleware(['can:ward.update']);
        Route::delete('wards/{id}', 'WardController@destroy')->middleware(['can:ward.delete']);

        Route::get('branches', 'BranchController@index')->middleware(['can:branch.index']);
        Route::get('branches/{id}', 'BranchController@show')->middleware(['can:branch.show']);
        Route::post('branches', 'BranchController@store')->middleware(['can:branch.create']);
        Route::put('branches/{id}', 'BranchController@update')->middleware(['can:branch.update']);
        Route::delete('branches/{id}', 'BranchController@destroy')->middleware(['can:branch.delete']);

        Route::get('menus', 'MenuController@index')->middleware(['can:menu.index']);
        Route::get('menus/{id}', 'MenuController@show')->middleware(['can:menu.show']);
        Route::post('menus', 'MenuController@store')->middleware(['can:menu.create']);
        Route::put('menus/{id}', 'MenuController@update')->middleware(['can:menu.update']);
        Route::delete('menus/{id}', 'MenuController@destroy')->middleware(['can:menu.delete']);

        Route::get('galleries', 'GalleryController@index')->middleware(['can:gallery.index']);
        Route::get('galleries/{id}', 'GalleryController@show')->middleware(['can:gallery.show, id']);
        Route::post('galleries', 'GalleryController@store')->middleware(['can:gallery.create']);
        Route::put('galleries/{id}', 'GalleryController@update')->middleware(['can:gallery.update,id']);
        Route::delete('galleries/{id}', 'GalleryController@destroy')->middleware(['can:gallery.delete,id']);

        Route::get('articles', 'ArticleController@index')->middleware(['can:article.index']);
        Route::get('articles/{id}', 'ArticleController@show')->middleware(['can:article.show, id']);
        Route::post('articles', 'ArticleController@store')->middleware(['can:article.create']);
        Route::put('articles/{id}', 'ArticleController@update')->middleware(['can:article.update,id']);
        Route::delete('articles/{id}', 'ArticleController@destroy')->middleware(['can:article.delete,id']);
        Route::get('articles-count', 'ArticleController@countArticle');

        Route::get('products', 'ProductController@index')->middleware(['can:product.index']);
        Route::get('products/{id}', 'ProductController@show')->middleware(['can:product.show, id']);
        Route::post('products', 'ProductController@store')->middleware(['can:product.create']);
        Route::put('products/{id}', 'ProductController@update')->middleware(['can:product.update,id']);
        Route::delete('products/{id}', 'ProductController@destroy')->middleware(['can:product.delete,id']);
        Route::get('products-count', 'ProductController@countProduct');

        Route::delete('product-images/{id}', 'ProductImageController@destroy')->middleware(['can:product.update,id']);

        Route::get('orders', 'OrderController@index')->middleware(['can:order.index']);
        Route::get('orders/{id}', 'OrderController@show')->middleware(['can:order.show']);
        Route::post('orders', 'OrderController@store')->middleware(['can:order.create']);
        Route::put('orders/{id}', 'OrderController@update')->middleware(['can:order.update']);
        Route::delete('orders/{id}', 'OrderController@destroy')->middleware(['can:order.delete']);
        Route::get('orders-count', 'OrderController@countOrder');
    });


    Route::get('get-menus', 'MenuController@getMenus');
    Route::get('get-categories', 'CategoryController@getCategories');
    Route::get('get-banners', 'BannerController@getBanners');
    Route::get('get-super-deals', 'ProductController@getSuperDeals');
    Route::get('get-hot-products', 'ProductController@getHotProducts');
    Route::get('get-product-boxes', 'ProductController@getProductsBoxes');
    Route::get('get-home-category-banner', 'CategoryBannerController@getCategoryBannersInHome');
    Route::get('get-home-brands', 'BrandController@getBrandsInHome');
    Route::get('get-category-banners', 'CategoryBannerController@getCategoryBannerBySlug');
    Route::get('get-category', 'CategoryController@getCategoryBySlug');
    Route::get('get-breadcrumb-product-list', 'CategoryController@getBreadcrumb');
    Route::get('get-best-selling', 'ProductController@getBestSellingByCategory');
    Route::get('get-products', 'ProductController@getProductsByCategory');
    Route::get('get-breadcrumb-product-detail', 'ProductController@getBreadcrumb');
    Route::get('get-similar-products', 'ProductController@getSimilarProducts');
    Route::get('get-best-selling-products', 'ProductController@getBestSellingProductSameCategory');
    Route::get('get-product', 'ProductController@getProduct');
    Route::get('search-products', 'ProductController@searchProducts');
    Route::get('get-articles', 'ArticleController@getArticles');
    Route::get('get-article', 'ArticleController@getArticle');
    Route::put('update-customer', 'CustomerController@updateByToken');
    Route::get('compare-products', 'ProductController@compareProducts');
    Route::post('place-order', 'OrderController@placeOrder');
    Route::get('my-orders', 'OrderController@getOrdersOfCustomer');
    Route::get('my-order-detail', 'OrderController@getOrderByCode');
    Route::get('cancel-order', 'OrderController@cancelOrder');

    Route::post('subscriptions', 'PushSubscriptionController@update');
    Route::post('subscriptions/delete', 'PushSubscriptionController@destroy');

    Route::post('/messages', function () {

        $customer = \App\Models\Customer::all();
        $body = 'Thank you for using our application API.';
        $data =['openUrl' => 'https://google.com.vn', 'image' => 'https://e2018.aseanskills2018.com/public/images/products/thumbnail/26/10026343-Internet-Tivi-Sony-48-Inch-Kdl-48W650D-01.jpg'];
        Notification::send($customer, new MessageNotification($body, $data));

        return ['status' => 'OK'];
    });

    Route::group([
        'namespace' => 'CustomerAuth',
    ], function () {
        Route::post('register', 'CustomerRegisterController@register');
        Route::post('login', 'CustomerAuthController@login');
        Route::get('logout', 'CustomerAuthController@logout');
        Route::prefix('password')->group(function () {
            Route::post('create', 'CustomerPasswordResetController@create');
            Route::get('find/{token}', 'CustomerPasswordResetController@find');
            Route::post('reset', 'CustomerPasswordResetController@reset');
        });

        Route::get('user', 'CustomerAuthController@getUser');
    });

});
