<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//         $this->call(RolesSeeder::class);
//         $this->call(UsersSeeder::class);
//         $this->call(CustomerSeeder::class);
//         $this->call(CategoryBanner::class);
//         $this->call(BannerSeeder::class);
//         $this->call(BranchSeeder::class);
         $this->call(MenuSeeder::class);
//        $this->call(GallerySeeder::class);
//        $this->call(ArticleSeeder::class);
//        $this->call(OrderSeeder::class);
//        $this->call(ProductPromotion::class);
//        $this->call(ProductQuantity::class);
    }
}
