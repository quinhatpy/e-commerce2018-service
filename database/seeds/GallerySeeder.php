<?php

use Illuminate\Database\Seeder;

class GallerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 110; $i++) {
            \App\Models\Gallery::create([
                'path' => $faker->imageUrl($width = 640, $height = 480),
                'title' => $faker->text(10),
                'alt' => $faker->text(10),
                'reference' => 'blog',
                'user_id' => 1,
            ]);
        }
    }
}
