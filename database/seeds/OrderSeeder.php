<?php

use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 110; $i++) {
            $order = \App\Models\Order::create([
                'code' => strtoupper($faker->text(10)),
                'receiver_name' => $faker->name,
                'receiver_phone' => $faker->e164PhoneNumber,
                'receiver_address' => $faker->address,
                'note' => $faker->paragraph(1),
                'status' => $faker->numberBetween(0, 4),
                'is_paid' => $faker->numberBetween(0, 1),
                'payment_method_id' => 1,
                'delivery_method_id' =>  $faker->numberBetween(1, 3),
                'customer_id' => 3,
            ]);

            $numRand = mt_rand(1, 5);
            for($j = 0; $j < $numRand; $j++) {

                \App\Models\OrderDetail::create([
                    'order_id' => $order->id,
                    'product_id' => $faker->numberBetween(1, 100),
                    'quantity' => $faker->numberBetween(1, 3),
                    'price' => $faker->numberBetween(1000000, 10000000)
                ]);
            }

        }
    }
}
