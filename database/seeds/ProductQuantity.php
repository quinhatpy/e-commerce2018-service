<?php

use Illuminate\Database\Seeder;

class ProductQuantity extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = \App\Models\Product::select('id')->get();

        foreach ($products as $product) {
            $limit = mt_rand(1, 11);
            $branches = \App\Models\Branch::select('id')->limit($limit)->get();

            foreach ($branches as $branch) {
                \App\Models\ProductQuantity::create([
                    'quantity' => mt_rand(1, 20),
                    'product_id' => $product->id,
                    'branch_id' => $branch->id,
                ]);
            }

        }
    }
}
