<?php

use Illuminate\Database\Seeder;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 110; $i++) {
            \App\Models\Banner::create([
                'image' => $faker->imageUrl($width = 640, $height = 480),
                'position' => 'home',
                'link' => $faker->url,
                'ordinal' =>$faker->numberBetween(1, 10),
                'is_show' => $faker->numberBetween(0, 1),
            ]);
        }
    }
}
