<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductPromotion extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = \App\Models\Product::select('id', 'price')->orderBy(DB::raw('RAND()'))->limit(100)->get();

        foreach ($products as $product) {
            $percent = 1 - (mt_rand(10, 50) /100);
            $newPrice = $percent* $product->price ;

            \App\Models\ProductPromotion::create([
                'new_price' => $newPrice,
                'product_id' => $product->id,
                'is_show' => 1,
                'start' => \Carbon\Carbon::now(),
                'expire' => \Carbon\Carbon::now()->addMonth(1),
            ]);
        }
    }
}
