<?php

use Illuminate\Database\Seeder;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 110; $i++) {
            \App\Models\Branch::create([
                'name' => $faker->company,
                'ward_id' => $faker->randomElement([1, 4, 6, 7, 8, 10]),
                'address' => $faker->streetAddress,
            ]);
        }
    }
}
