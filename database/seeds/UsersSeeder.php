<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $user1 = User::create([
            'name' => 'Le Qui Nhat',
            'email' => 'quinhatpy@gmail.com',
            'password' => bcrypt('123456'),
            'gender' => 1,
            'role_id' => 1
        ]);

        $user2 = User::create([
            'name' => 'Nhat Le',
            'email' => 'nhatle.nit@gmail.com',
            'password' => bcrypt('123456'),
            'gender' => 1,
            'role_id' => 2
        ]);

        $user3 = User::create([
            'name' => 'Moderator',
            'email' => 'it.quinhat@gmail.com',
            'password' => bcrypt('123456'),
            'gender' => 1,
            'role_id' => 3
        ]);

        $faker = Faker\Factory::create();
        for ($i = 0; $i < 100; $i++) {
            User::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => bcrypt('123456'),
                'gender' => $faker->numberBetween(0, 1),
                'role_id' => $faker->numberBetween(2, 3)
            ]);
        }
    }
}
