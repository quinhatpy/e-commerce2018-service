<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $supperAdmin = Role::create([
            'name' => 'Super Admin',
            'slug' => 'supper-admin',
            'permissions' => [
                'user.index' => true,
                'user.show' => true,
                'user.create' => true,
                'user.store' => true,
                'user.edit' => true,
                'user.update' => true,
                'user.delete' => true,
            ]
        ]);
        $admin = Role::create([
            'name' => 'Admin',
            'slug' => 'admin',
            'permissions' => [
                'user.index' => true,
                'user.show' => true,
                'user.create' => true,
                'user.store' => true,
                'user.edit' => true,
                'user.update' => true,
                'user.delete' => true,
            ]
        ]);

        $mod = Role::create([
            'name' => 'Moderator',
            'slug' => 'moderator',
            'permissions' => [
                'user.index' => true,
                'user.show' => true,
                'user.edit' => true,
                'user.update' => true,
            ]
        ]);
    }
}
