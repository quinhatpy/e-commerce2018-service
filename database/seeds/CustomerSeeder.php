<?php

use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 110; $i++) {
            \App\Models\Customer::create([
                'name' => $faker->name,
                'email' => $faker->unique()->email,
                'password' => bcrypt('123456'),
                'gender' => $faker->numberBetween(0, 1),
                'phone' => $faker->e164PhoneNumber,
                'address' => $faker->address,
                'birthday' => $faker->date($format = 'Y-m-d', $max = 'now')
            ]);
        }
    }
}
