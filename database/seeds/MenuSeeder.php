<?php

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $categories = \App\Models\Category::whereParentId(13)->get();
        foreach ($categories as $category) {
            $data = [
                'name' => $category->name,
                'icon' => '',
                'link' => 'loai/' . $category->slug,
                'is_show' => $category->is_show,
                'parent_id' => 145

            ];

            \App\Models\Menu::create($data);
        }
    }
}
