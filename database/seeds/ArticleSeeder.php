<?php

use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 110; $i++) {
            $name = $faker->text(100);
            \App\Models\Article::create([

                'name' => $name,
                'short_description' => $faker->text(),
                'content' => $faker->paragraph(),
                'slug' => str_slug($name),
                'thumbnail' => $faker->imageUrl($width = 640, $height = 480),
                'is_show' => $faker->numberBetween(0, 1),
                'seo_title' => $faker->text('60'),
                'seo_description' => $faker->text('150'),
                'user_id' => $faker->numberBetween(1, 3),
            ]);
        }
    }
}
