<?php

use Illuminate\Database\Seeder;

class CategoryBanner extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 110; $i++) {
            \App\Models\CategoryBanner::create([
                'path' => $faker->imageUrl($width = 640, $height = 480),
                'link' => $faker->url,
                'ordinal' =>$faker->numberBetween(1, 10),
                'is_show' => $faker->numberBetween(0, 1),
                'is_main' => 0,
                'category_id' => $faker->numberBetween(5, 10),
            ]);
        }
    }
}
