<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('email', 100)->unique();
            $table->unsignedTinyInteger('is_active')->default(0);
            $table->string('password', 250);
            $table->string('avatar', 250)->nullable();
            $table->string('phone', 15)->nullable();
            $table->text('address')->nullable();
            $table->unsignedTinyInteger('gender');
            $table->date('birthday')->nullable();
            $table->unsignedInteger('role_id');
            $table->string('activation_token', 60)->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
