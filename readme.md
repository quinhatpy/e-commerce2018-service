# Xây dựng web service cho website thương mại điện tử bằng ngôn ngữ PHP sử dụng framework Laravel.
## Dự án gồm các module sau:
1.  **User**: Quản lý thông tin nhân viên của hệ thống.
2.  **User Authentication**: Xác thực tài khoản của nhân viên.
3.  **Customer**: Quản lý thông tin khách hàng.
4.  **Customer Authentication**: Xác thực tài khoản của khách hàng.
5.  **Role**: Quản lý các quyền hạn có trong ứng dụng.
6.  **Category**: Quản lý thông tin các danh mục sản phẩm.
7.  **Category Banner**: Quản lý các hình banner thuộc danh mục sản phẩm.
8.  **Attribute**: Quản lý các thuộc tính của sản phẩm theo từng danh mục sản phẩm.
9.  **Gallery**: Quản lý thư viện hình ảnh.
10.  **Brand**: Quản lý thông tin của nhãn hiệu.
11.  **Branch**: Quản lý thông tin chi nhánh của hệ thống.
12.  **Product**: Quản lý thông tin của sản phẩm.
13.  **Order**: Quản lý đơn hàng.
14.  **Article**: Quản lý các bài viết tin tức của website.
15.  **Banner**: Quản lý banner của website.
16.  **Administrative Units**: Quản lý thông tin đơn vị hành chính của Việt Nam.
17.  **Menu**: Quản lý menu của website.

## Mô tả chi tiết các api
### Kết quả trả về của một api bao gồm

| Tham số| Kiểu | Mô tả|
|--------|------|------|
| success | Boolean| Trạng thái truy vấn dữ liệu |
| result| Object| Dữ liệu truy vấn được |
| message| String| Nội dung tin nhắn của truy vấn |

### 1. User Management
#### 1.1 Lấy tất cả nhân viên `/api/v1/users?[pamrams]`
- Description: Cho phép người dùng có quyền truy cập api có thể lấy tất cả nhân viên có trong database.
- Request method: GET
- Header authorization: bearer token
- Request parameter:

| Tham số| Kiểu | Mô tả|
|--------|------|------|
| offset | Integer | Vị trí bắt đầu của dòng dữ liệu cần lấy |
| limit | Integer | Số lượng dòng kết quả trả về |
| keyword | String | Từ khóa tìm kiếm user theo tên. |
| sort| String | Sắp xếp kết quả theo thứ tự tăng dần/giảm dần dựa theo tên thuộc tính của user (Ví dụ: tăng dần theo id: sort_by=id, giảm dần theo id: sort_by=-id) |

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: ""
            - result:
      
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | totalRows | Integer| Tổng số dòng dữ liệu hiện có |
                | users| Array| Mảng danh sách user lấy được |
                | message| String| Nội dung tin nhắn của truy vấn |
            
                - Mỗi phần tử trong mảng users bao gồm:
                    
                    | Tham số| Kiểu | Mô tả|
                    |--------|------|------|
                    | id | Integer| ID của user |
                    | name| String| Họ và tên của user|
                    | email| String| Email của user |
                    | avatar| String| Đường dẫn đến hình đại diện của user |
                    | phone| String| Số điện thoại của user |
                    | address| String| Địa chỉ của user |
                    | gender| Integer| Giới tính của user (0: Female, 1: Male) |
                    | birthday| String| Ngày sinh của user |
                    | created_at| String| Ngày giờ khi tạo user |
                    | updated_at| String| Ngày giờ khi cập nhật của user |
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": { 
                              "totalRows": 1,
                              "users": [
                                  {  
                                     "id": 1,
                                     "name": "Le Qui Nhat",
                                     "email": "quinhatpy@gmail.com",
                                     "avatar": "http://ecommerce2018.local/public/images/users/1539948386-le-qui-nhat-nhatle.jpg",
                                     "phone": "0968403428",
                                     "address": "Phú Yên",
                                     "gender": 1,
                                     "birthday": "1996-10-24",
                                     "created_at": "2018-10-19 10:09:33",
                                     "updated_at": "2018-12-08 15:21:24",
                                  }
                              ]
                          }
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
    - Nếu tham số không hợp lệ:
        - Header: status 422 Unprocessable Entity
        - Body:
            - success: false
            - message: "Validate error"
            - data: Object chứa thông tin validate không hợp lệ
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Validate error",
                        "data": {
                            "limit": [
                                "The limit must be an integer."
                            ]
                        }
                    }
                ```
    - Nếu có lỗi truy vấn:
        - Header: status 500 Internal Server Error
        - Body:
            - success: false
            - message: "Query error"
            - data: Object chứa thông tin lỗi
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Query error",
                        "data": {
                            "errorInfo": [
                                "You have an error in your SQL syntax"
                            ]
                        }
                    }
                ```
                
#### 1.2 Lấy thông tin của một nhân viên `/api/v1/users/{ID}`
- Description: Cho phép người dùng có quyền truy cập api có thể lấy thông tin của nhân nhân viên dựa vào ID.
- Request method: GET
- Header authorization: bearer token
- Request parameter:

| Tham số| Kiểu | Mô tả|
|--------|------|------|
| ID | Integer | ID của nhân viên |

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: ""
            - result:
            
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | id | Integer| ID của user |
                | name| String| Họ và tên của user|
                | email| String| Email của user |
                | avatar| String| Đường dẫn đến hình đại diện của user |
                | phone| String| Số điện thoại của user |
                | address| String| Địa chỉ của user |
                | gender| Integer| Giới tính của user (0: Female, 1: Male) |
                | birthday| String| Ngày sinh của user |
                | role | Object| Đối tượng chứa thông tin quyền hạn của user |
                | created_at| String| Ngày giờ khi tạo user |
                | updated_at| String| Ngày giờ khi cập nhật của user |
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": { 
                              "totalRows": 1,
                              "users": [
                                  {  
                                     "id": 1,
                                     "name": "Le Qui Nhat",
                                     "email": "quinhatpy@gmail.com",
                                     "avatar": "http://ecommerce2018.local/public/images/users/1539948386-le-qui-nhat-nhatle.jpg",
                                     "phone": "0968403428",
                                     "address": "Phú Yên",
                                     "gender": 1,
                                     "birthday": "1996-10-24",
                                     "role": {
                                        "id": 1,
                                        "role": "Supper Admin"
                                        "permission": {}
                                     }
                                     "created_at": "2018-10-19 10:09:33",
                                     "updated_at": "2018-12-08 15:21:24",
                                  }
                              ]
                          }
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
    - Nếu không tìm thấy:
        - Header: status 404 Not Found
        - Body:
            - success: false
            - message: "Not found"
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Not found"
                    }
                ```
#### 1.3 Thêm một nhân viên `/api/v1/users`
- Description: Cho phép người dùng có quyền truy cập api có thể thêm mới một nhân viên.
- Request method: POST
- Header authorization: bearer token
- Request parameter:
    - Body

        | Tham số| Kiểu | Bắt buộc | Mô tả|
        |--------|------|------|------|
        | name | String | Có | Họ tên |
        | email | String | Có | Địa chỉ Email|
        | password | String | Có | Mật khẩu |
        | password_confirmation | String | Có | Nhập lại mật khẩu |
        | gender | Integer | Không | Giới tính (0: Female, 1: Male) |
        | birthday | String | Không| Ngày sinh |
        | avatar | File | Không| Đường dẫn đến hình đại diện |
        | address | String | Không| Địa chỉ |
        | phone | String | Không| Số điện thoại |
        | role_id | Integer | Có | ID của quyền hạn |

- Response result:
    - Nếu thành công:
        - Header: status 201 Created
        - Body:
            - success: true
            - message: Create user successfully
            - result:
      
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | id | Integer| ID của user |
                | name| String| Họ và tên của user|
                | email| String| Email của user |
                | avatar| String| Đường dẫn đến hình đại diện của user |
                | phone| String| Số điện thoại của user |
                | address| String| Địa chỉ của user |
                | gender| Integer| Giới tính của user (0: Female, 1: Male) |
                | birthday| String| Ngày sinh của user |
                | role | Object| Đối tượng chứa thông tin quyền hạn của user |
                | created_at| String| Ngày giờ khi tạo user |
                | updated_at| String| Ngày giờ khi cập nhật của user |
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": {  
                               "id": 1,
                               "name": "Le Qui Nhat",
                               "email": "quinhatpy@gmail.com",
                               "avatar": "http://ecommerce2018.local/public/images/users/1539948386-le-qui-nhat-nhatle.jpg",
                               "phone": "0968403428",
                               "address": "Phú Yên",
                               "gender": 1,
                               "birthday": "1996-10-24",
                               "role": {
                                  "id": 1,
                                  "role": "Supper Admin"
                                  "permission": {}
                               }
                               "created_at": "2018-10-19 10:09:33",
                               "updated_at": "2018-12-08 15:21:24",
                          },
                           "message": "Create user successfully"
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
    - Nếu tham số không hợp lệ:
        - Header: status 422 Unprocessable Entity
        - Body:
            - success: false
            - message: "Validate error"
            - data: Object chứa thông tin validate không hợp lệ
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Validate error",
                        "data": {
                            "email": [
                                "The email has already been taken."
                            ]
                        }
                    }
                ```
    - Nếu có lỗi truy vấn:
        - Header: status 500 Internal Server Error
        - Body:
            - success: false
            - message: "Query error"
            - data: Object chứa thông tin lỗi
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Query error",
                        "data": {
                            "errorInfo": [
                                "You have an error in your SQL syntax"
                            ]
                        }
                    }
                ```
                
#### 1.4 Cập nhật thông tin một nhân viên `/api/v1/users/{ID}`
- Description: Cho phép người dùng có quyền truy cập api có thể cập nhật thông tin của nhân viên dựa vào ID.
- Request method: POST
- Header authorization: bearer token
- Request parameter:

    | Tham số| Kiểu | Mô tả|
    |--------|------|------|
    | ID | Integer | ID của nhân viên |
    - Body

        | Tham số| Kiểu | Bắt buộc | Mô tả|
        |--------|------|------|------|
        | name | String | Không | Họ tên |
        | email | String | Không| Địa chỉ Email|
        | password | String | Không| Mật khẩu |
        | password_confirmation | String | Không | Nhập lại mật khẩu |
        | gender | Integer | Không | Giới tính (0: Female, 1: Male) |
        | birthday | String | Không| Ngày sinh |
        | avatar | File | Không| Đường dẫn đến hình đại diện |
        | address | String | Không| Địa chỉ |
        | phone | String | Không| Số điện thoại |
        | role_id | Integer | Không | ID của quyền hạn |
        | _method | String| Có | Method: PUT |

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: Update user successfully
            - result:
      
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | id | Integer| ID của user |
                | name| String| Họ và tên của user|
                | email| String| Email của user |
                | avatar| String| Đường dẫn đến hình đại diện của user |
                | phone| String| Số điện thoại của user |
                | address| String| Địa chỉ của user |
                | gender| Integer| Giới tính của user (0: Female, 1: Male) |
                | birthday| String| Ngày sinh của user |
                | role | Object| Đối tượng chứa thông tin quyền hạn của user |
                | created_at| String| Ngày giờ khi tạo user |
                | updated_at| String| Ngày giờ khi cập nhật của user |
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": { 
                               "id": 1,
                               "name": "Le Qui Nhat",
                               "email": "quinhatpy@gmail.com",
                               "avatar": "http://ecommerce2018.local/public/images/users/1539948386-le-qui-nhat-nhatle.jpg",
                               "phone": "0968403428",
                               "address": "Phú Yên",
                               "gender": 1,
                               "birthday": "1996-10-24",
                               "role": {
                                  "id": 1,
                                  "role": "Supper Admin"
                                  "permission": {}
                               }
                               "created_at": "2018-10-19 10:09:33",
                               "updated_at": "2018-12-08 15:21:24",
                          },
                           "message": "Update user successfully"
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
    - Nếu tham số không hợp lệ:
        - Header: status 422 Unprocessable Entity
        - Body:
            - success: false
            - message: "Validate error"
            - data: Object chứa thông tin validate không hợp lệ
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Validate error",
                        "data": {
                            "password": [
                                "The password must be at least 6 characters."
                            ]
                        }
                    }
                ```
    - Nếu có lỗi truy vấn:
        - Header: status 500 Internal Server Error
        - Body:
            - success: false
            - message: "Query error"
            - data: Object chứa thông tin lỗi
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Query error",
                        "data": {
                            "errorInfo": [
                                "Column 'name' cannot be null"
                            ]
                        }
                    }
                ```
                
#### 1.5 Xóa một nhân viên `/api/v1/users/{ID}`
- Description: Cho phép người dùng có quyền truy cập api có thể cập nhật thông tin của nhân viên dựa vào ID.
- Request method: DELETE
- Header authorization: bearer token
- Request parameter:

    | Tham số| Kiểu | Mô tả|
    |--------|------|------|
    | ID | Integer | ID của nhân viên |
    - Body

        | Tham số| Kiểu | Bắt buộc | Mô tả|
        |--------|------|------|------|
        | name | String | Không | Họ tên |
        | email | String | Không| Địa chỉ Email|
        | password | String | Không| Mật khẩu |
        | password_confirmation | String | Không | Nhập lại mật khẩu |
        | gender | Integer | Không | Giới tính (0: Female, 1: Male) |
        | birthday | String | Không| Ngày sinh |
        | avatar | File | Không| Đường dẫn đến hình đại diện |
        | address | String | Không| Địa chỉ |
        | phone | String | Không| Số điện thoại |
        | role_id | Integer | Không | ID của quyền hạn |
        | _method | String| Có | Method: PUT |

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: Delete user successfully
            - result:
      
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | id | Integer| ID của user |
                | name| String| Họ và tên của user|
                | email| String| Email của user |
                | avatar| String| Đường dẫn đến hình đại diện của user |
                | phone| String| Số điện thoại của user |
                | address| String| Địa chỉ của user |
                | gender| Integer| Giới tính của user (0: Female, 1: Male) |
                | birthday| String| Ngày sinh của user |
                | role | Object| Đối tượng chứa thông tin quyền hạn của user |
                | created_at| String| Ngày giờ khi tạo user |
                | updated_at| String| Ngày giờ khi cập nhật của user |
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": { 
                               "id": 1,
                               "name": "Le Qui Nhat",
                               "email": "quinhatpy@gmail.com",
                               "avatar": "http://ecommerce2018.local/public/images/users/1539948386-le-qui-nhat-nhatle.jpg",
                               "phone": "0968403428",
                               "address": "Phú Yên",
                               "gender": 1,
                               "birthday": "1996-10-24",
                               "role": {
                                  "id": 1,
                                  "role": "Supper Admin"
                                  "permission": {}
                               }
                               "created_at": "2018-10-19 10:09:33",
                               "updated_at": "2018-12-08 15:21:24",
                          },
                           "message": "Update user successfully"
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
    - Nếu không tìm thấy:
        - Header: status 404 Not Found
        - Body:
            - success: false
            - message: User not found
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "User not found"
                    }
                ```
    - Nếu có lỗi truy vấn:
        - Header: status 500 Internal Server Error
        - Body:
            - success: false
            - message: "Query error"
            - data: Object chứa thông tin lỗi
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Query error",
                        "data": {}
                    }
                ```
                
                
                
########################################################################            
                
Chức năng của module Customer Management dùng để quản lý thông tin của khách hàng. Gồm các chức năng: Lấy danh sách các khách hàng, lấy thông tin của một khách hàng cụ thể, thêm mới, cập nhật, xóa một khách hàng.
## 1. Lấy tất cả khách hàng `/api/v1/customers?[pamrams]`
- Description: Cho phép người dùng có quyền truy cập api có thể lấy tất cả khách hàng có trong database.
- Request method: GET
- Header authorization: bearer token
- Request parameter:

| Tham số| Kiểu | Mô tả|
|--------|------|------|
| offset | Integer | Vị trí bắt đầu của dòng dữ liệu cần lấy |
| limit | Integer | Số lượng dòng kết quả trả về |
| keyword | String | Từ khóa tìm kiếm theo tên. |
| sort| String | Sắp xếp kết quả theo thứ tự tăng dần/giảm dần dựa theo tên thuộc tính của khách hàng (Ví dụ: tăng dần theo id: sort_by=id, giảm dần theo id: sort_by=-id) |

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: ""
            - result:
      
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | totalRows | Integer| Tổng số dòng dữ liệu hiện có |
                | customers| Array| Danh sách khách hàng lấy được |
                | message| String| Nội dung tin nhắn của truy vấn |
            
                - Mỗi phần tử trong mảng users bao gồm:
                    
                    | Tham số| Kiểu | Mô tả|
                    |--------|------|------|
                    | id | Integer| ID của khách hàng |
                    | name| String| Họ và tên |
                    | email| String| Địa chỉ Email |
                    | phone| String| Số điện thoại |
                    | address| String| Địa chỉ |
                    | created_at| String| Ngày giờ khi tạo khách hàng |
                    | updated_at| String| Ngày giờ khi cập nhật khách hàng |
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": { 
                              "totalRows": 1,
                              "users": [
                                  {  
                                     "id": 1,
                                     "name": "Le Qui Nhat",
                                     "email": "quinhatpy@gmail.com",
                                     "phone": "0968403428",
                                     "address": "Phú Yên",
                                     "created_at": "2018-10-19 10:09:33",
                                     "updated_at": "2018-12-08 15:21:24",
                                  }
                              ]
                          }
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
    - Nếu tham số không hợp lệ:
        - Header: status 422 Unprocessable Entity
        - Body:
            - success: false
            - message: "Validate error"
            - data: Object chứa thông tin validate không hợp lệ
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Validate error",
                        "data": {
                            "limit": [
                                "The limit must be an integer."
                            ]
                        }
                    }
                ```
    - Nếu có lỗi truy vấn:
        - Header: status 500 Internal Server Error
        - Body:
            - success: false
            - message: "Query error"
            - data: Object chứa thông tin lỗi
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Query error",
                        "data": {
                            "errorInfo": [
                                "You have an error in your SQL syntax"
                            ]
                        }
                    }
                ```
                
## 2. Lấy thông tin của một khách hàng `/api/v1/customers/{ID}`
- Description: Cho phép người dùng có quyền truy cập api có thể lấy thông tin của nhân khách hàng dựa vào ID.
- Request method: GET
- Header authorization: bearer token
- Request parameter:

| Tham số| Kiểu | Mô tả|
|--------|------|------|
| ID | Integer | ID của khách hàng |

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: ""
            - result:
            
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | id | Integer| ID của khách hàng |
                | name| String| Họ và tên|
                | email| String| Địa chỉ Email |
                | phone| String| Số điện thoại |
                | address| String| Địa chỉ |
                | created_at| String| Ngày giờ khi tạo khách hàng |
                | updated_at| String| Ngày giờ khi cập nhật khách hàng |
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": {  
                                     "id": 1,
                                     "name": "Le Qui Nhat",
                                     "email": "quinhatpy@gmail.com",
                                     "phone": "0968403428",
                                     "address": "Phú Yên",
                                     "created_at": "2018-10-19 10:09:33",
                                     "updated_at": "2018-12-08 15:21:24",
                                  }
                              ]
                          },
                           "message": "",
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
    - Nếu không tìm thấy:
        - Header: status 404 Not Found
        - Body:
            - success: false
            - message: "Not found"
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Not found"
                    }
                ```
## 3. Thêm một khách hàng `/api/v1/users`
- Description: Cho phép người dùng có quyền truy cập api có thể thêm mới một khách hàng.
- Request method: POST
- Header authorization: bearer token
- Request parameter:
    - Body

        | Tham số| Kiểu | Bắt buộc | Mô tả|
        |--------|------|------|------|
        | name | String | Có | Họ tên |
        | email | String | Có | Địa chỉ Email|
        | password | String | Có | Mật khẩu |
        | password_confirmation | String | Có | Nhập lại mật khẩu |
        | address | String | Không| Địa chỉ |
        | phone | String | Không| Số điện thoại |

- Response result:
    - Nếu thành công:
        - Header: status 201 Created
        - Body:
            - success: true
            - message: Create customer successfully
            - result:
      
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | id | Integer| ID của khách hàng |
                | name| String| Họ và tên|
                | email| String| Địa chỉ Email |
                | phone| String| Số điện thoại |
                | address| String| Địa chỉ |
                | created_at| String| Ngày giờ khi tạo khách hàng |
                | updated_at| String| Ngày giờ khi cập nhật khách hàng |
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": {  
                               "id": 1,
                               "name": "Le Qui Nhat",
                               "email": "quinhatpy@gmail.com",
                               "phone": "0968403428",
                               "address": "Phú Yên",
                               "created_at": "2018-10-19 10:09:33",
                               "updated_at": "2018-12-08 15:21:24",
                          },
                           "message": "Create customer successfully"
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
    - Nếu tham số không hợp lệ:
        - Header: status 422 Unprocessable Entity
        - Body:
            - success: false
            - message: "Validate error"
            - data: Object chứa thông tin validate không hợp lệ
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Validate error",
                        "data": {
                            "email": [
                                "The email has already been taken."
                            ]
                        }
                    }
                ```
    - Nếu có lỗi truy vấn:
        - Header: status 500 Internal Server Error
        - Body:
            - success: false
            - message: "Query error"
            - data: Object chứa thông tin lỗi
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Query error",
                        "data": {
                            "errorInfo": [
                                "You have an error in your SQL syntax"
                            ]
                        }
                    }
                ```
                
## 4. Cập nhật thông tin một khách hàng `/api/v1/customers/{ID}`
- Description: Cho phép người dùng có quyền truy cập api có thể cập nhật thông tin của khách hàng dựa vào ID.
- Request method: POST
- Header authorization: bearer token
- Request parameter:

    | Tham số| Kiểu | Mô tả|
    |--------|------|------|
    | ID | Integer | ID của khách hàng |
    - Body

        | Tham số| Kiểu | Bắt buộc | Mô tả|
        |--------|------|------|------|
        | name | String | Không | Họ tên |
        | email | String | Không| Địa chỉ Email|
        | password | String | Không| Mật khẩu |
        | password_confirmation | String | Không | Nhập lại mật khẩu |
        | address | String | Không| Địa chỉ |
        | phone | String | Không| Số điện thoại |
        | _method | String| Có | Method: PUT |

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: Update customer successfully
            - result:
      
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | id | Integer| ID của khách hàng |
                | name| String| Họ và tên|
                | email| String| Địa chỉ Email |
                | phone| String| Số điện thoại |
                | address| String| Địa chỉ |
                | created_at| String| Ngày giờ khi tạo khách hàng |
                | updated_at| String| Ngày giờ khi cập nhật khách hàng |
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": { 
                               "id": 1,
                               "name": "Le Qui Nhat",
                               "email": "quinhatpy@gmail.com",
                               "phone": "0968403428",
                               "address": "Phú Yên",
                               "created_at": "2018-10-19 10:09:33",
                               "updated_at": "2018-12-08 15:21:24",
                          },
                           "message": "Update customer successfully"
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
    - Nếu tham số không hợp lệ:
        - Header: status 422 Unprocessable Entity
        - Body:
            - success: false
            - message: "Validate error"
            - data: Object chứa thông tin validate không hợp lệ
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Validate error",
                        "data": {
                            "password": [
                                "The password must be at least 6 characters."
                            ]
                        }
                    }
                ```
    - Nếu có lỗi truy vấn:
        - Header: status 500 Internal Server Error
        - Body:
            - success: false
            - message: "Query error"
            - data: Object chứa thông tin lỗi
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Query error",
                        "data": {
                            "errorInfo": [
                                "Column 'name' cannot be null"
                            ]
                        }
                    }
                ```
                
## 5. Xóa một khách hàng `/api/v1/customers/{ID}`
- Description: Cho phép người dùng có quyền truy cập api có thể xóa thông tin của khách hàng dựa vào ID.
- Request method: DELETE
- Header authorization: bearer token
- Request parameter:

    | Tham số| Kiểu | Mô tả|
    |--------|------|------|
    | ID | Integer | ID của khách hàng |

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: Delete customer successfully
            - result: {}
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": {},
                          "message": "Delete customer successfully"
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
    - Nếu không tìm thấy:
        - Header: status 404 Not Found
        - Body:
            - success: false
            - message: Customer not found
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Customer not found"
                    }
                ```
    - Nếu có lỗi truy vấn:
        - Header: status 500 Internal Server Error
        - Body:
            - success: false
            - message: "Query error"
            - data: Object chứa thông tin lỗi
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Query error",
                        "data": {}
                    }
                ```
                
## 6. Khách hàng cập nhật thông tin `/api/v1/update-customer`
- Description: Cho phép khách hàng cập nhật thông tin của chính mình.
- Request method: POST
- Header authorization: bearer token
- Request parameter:
    - Body

        | Tham số| Kiểu | Bắt buộc | Mô tả|
        |--------|------|------|------|
        | name | String | Không | Họ tên |
        | email | String | Không| Địa chỉ Email|
        | password | String | Không| Mật khẩu |
        | password_confirmation | String | Không | Nhập lại mật khẩu |
        | address | String | Không| Địa chỉ |
        | phone | String | Không| Số điện thoại |
        | _method | String| Có | Method: PUT |

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: Update customer successfully
            - result:
      
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | id | Integer| ID của khách hàng |
                | name| String| Họ và tên|
                | email| String| Địa chỉ Email |
                | phone| String| Số điện thoại |
                | address| String| Địa chỉ |
                | created_at| String| Ngày giờ khi tạo khách hàng |
                | updated_at| String| Ngày giờ khi cập nhật khách hàng |
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": { 
                               "id": 1,
                               "name": "Le Qui Nhat",
                               "email": "quinhatpy@gmail.com",
                               "phone": "0968403428",
                               "address": "Phú Yên",
                               "created_at": "2018-10-19 10:09:33",
                               "updated_at": "2018-12-08 15:21:24",
                          },
                           "message": "Update customer successfully"
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
    - Nếu tham số không hợp lệ:
        - Header: status 422 Unprocessable Entity
        - Body:
            - success: false
            - message: "Validate error"
            - data: Object chứa thông tin validate không hợp lệ
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Validate error",
                        "data": {
                            "password": [
                                "The password must be at least 6 characters."
                            ]
                        }
                    }
                ```
    - Nếu có lỗi truy vấn:
        - Header: status 500 Internal Server Error
        - Body:
            - success: false
            - message: "Query error"
            - data: Object chứa thông tin lỗi
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Query error",
                        "data": {
                            "errorInfo": [
                                "Column 'name' cannot be null"
                            ]
                        }
                    }
                ```
                
## 7. Đếm số lượng khách hàng hiện có `/api/v1/customers-count`
- Description: Cho phép người dùng có quyền truy cập api lấy được tổng số khách hàng hiện có trong database.
- Request method: GET
- Header authorization: bearer token

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: 
            - result: Số lượng khách hàng hiện có (Integer)
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": 100,
                           "message": ""
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
                
                

########################################################################

Chức năng của module Role Management dùng để quản lý các nhóm quyền hạn. Gồm các chức năng: Lấy danh sách các quyên hạn, lấy thông tin của một quyền hạn cụ thể, thêm mới, cập nhật, xóa một quyền hạn.
## 1. Lấy tất cả nhóm quyền hạn `/api/v1/roles`
- Description: Cho phép người dùng có quyền truy cập api có thể lấy tất cả các nhóm quyền hạn có trong database.
- Request method: GET
- Header authorization: bearer token

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: ""
            - result: Danh sách các quyền hạn (Array). Mỗi phần tử bao gồm
            
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | id | Integer| ID của quyền hạn |
                | name | String| Tên của nhóm quyền |
                | permission | Object| Chi tiết mỗi quyền hạn |
                | created_at| String| Ngày giờ khi tạo nhóm quyền hạn |
                | updated_at| String| Ngày giờ khi cập nhật nhóm quyền hạn |
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": [
                              {  
                                 "id": 1,
                                 "name": "Admin",
                                 "permissions": {},
                                 "created_at": "2018-10-19 10:09:33",
                                 "updated_at": "2018-12-08 15:21:24",
                              }
                          ]
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
                
## 2. Lấy thông tin của một nhóm quyền `/api/v1/roles/{ID}`
- Description: Cho phép người dùng có quyền truy cập api có thể lấy thông tin của một nhóm quyền dựa vào ID.
- Request method: GET
- Header authorization: bearer token
- Request parameter:

| Tham số| Kiểu | Mô tả|
|--------|------|------|
| ID | Integer | ID của nhóm quyền |

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: ""
            - result:
            
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | id | Integer| ID của khách hàng |
                | name| String| Họ và tên|
                | permission | Object| Chi tiết mỗi quyền hạn |
                | created_at| String| Ngày giờ khi tạo khách hàng |
                | updated_at| String| Ngày giờ khi cập nhật khách hàng |
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": {  
                             "id": 1,
                             "name": "Le Qui Nhat",
                             "permissions": {},
                             "created_at": "2018-10-19 10:09:33",
                             "updated_at": "2018-12-08 15:21:24"
                          },
                           "message": "",
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
    - Nếu không tìm thấy:
        - Header: status 404 Not Found
        - Body:
            - success: false
            - message: "Not found"
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Not found"
                    }
                ```
## 3. Thêm một nhóm quyền `/api/v1/roles`
- Description: Cho phép người dùng có quyền truy cập api có thể thêm mới một nhóm quyền.
- Request method: POST
- Header authorization: bearer token
- Request parameter:
    - Body

        | Tham số| Kiểu | Bắt buộc | Mô tả|
        |--------|------|------|------|
        | name | String | Có | Họ tên |
        | Mảng thông tin quyền | Array | không | Mảng chứa thông tin của mỗi quyền |

- Response result:
    - Nếu thành công:
        - Header: status 201 Created
        - Body:
            - success: true
            - message: Create role successfully
            - result:
      
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | id | Integer| ID của nhóm quyền |
                | name| String| Họ và tên|
                | permission | Object| Chi tiết mỗi quyền hạn |
                | created_at| String| Ngày giờ khi tạo quyền hạn |
                | updated_at| String| Ngày giờ khi cập nhật quyền hạn |
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": {  
                               "id": 1,
                               "name": "Admin",
                               "permissions": {},
                               "created_at": "2018-10-19 10:09:33",
                               "updated_at": "2018-12-08 15:21:24",
                          },
                           "message": "Create role successfully"
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
    - Nếu tham số không hợp lệ:
        - Header: status 422 Unprocessable Entity
        - Body:
            - success: false
            - message: "Validate error"
            - data: Object chứa thông tin validate không hợp lệ
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Validate error",
                        "data": {
                            "email": [
                                "The name has already been taken."
                            ]
                        }
                    }
                ```
    - Nếu có lỗi truy vấn:
        - Header: status 500 Internal Server Error
        - Body:
            - success: false
            - message: "Query error"
            - data: Object chứa thông tin lỗi
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Query error",
                        "data": {
                            "errorInfo": [
                                "You have an error in your SQL syntax"
                            ]
                        }
                    }
                ```
                
## 4. Cập nhật thông tin một nhóm quyền `/api/v1/roles/{ID}`
- Description: Cho phép người dùng có quyền truy cập api có thể cập nhật thông tin của một nhóm quyền dựa vào ID.
- Request method: POST
- Header authorization: bearer token
- Request parameter:

    | Tham số| Kiểu | Mô tả|
    |--------|------|------|
    | ID | Integer | ID của nhóm quyền |
    - Body

        | Tham số| Kiểu | Bắt buộc | Mô tả|
        |--------|------|------|------|
        | name | String | Không | Họ tên |
        | Mảng thông tin quyền | Array | không | Mảng chứa thông tin của mỗi quyền |
        | _method | String| Có | Method: PUT |

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: Update role successfully
            - result:
      
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | id | Integer| ID của nhóm quyền |
                | name| String| Họ và tên|
                | permission | Object| Chi tiết mỗi quyền hạn |
                | created_at| String| Ngày giờ khi tạo quyền hạn |
                | updated_at| String| Ngày giờ khi cập nhật quyền hạn |
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": { 
                              "id": 1,
                              "name": "Admin",
                              "permissions": {},
                              "created_at": "2018-10-19 10:09:33",
                              "updated_at": "2018-12-08 15:21:24"
                          },
                           "message": "Update role successfully"
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
    - Nếu tham số không hợp lệ:
        - Header: status 422 Unprocessable Entity
        - Body:
            - success: false
            - message: "Validate error"
            - data: Object chứa thông tin validate không hợp lệ
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Validate error",
                        "data": {
                            "password": [
                                "The password must be at least 6 characters."
                            ]
                        }
                    }
                ```
    - Nếu có lỗi truy vấn:
        - Header: status 500 Internal Server Error
        - Body:
            - success: false
            - message: "Query error"
            - data: Object chứa thông tin lỗi
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Query error",
                        "data": {
                            "errorInfo": [
                                "Column 'name' cannot be null"
                            ]
                        }
                    }
                ```
                
## 5. Xóa một nhóm quyền `/api/v1/roles/{ID}`
- Description: Cho phép người dùng có quyền truy cập api có thể xóa thông tin của một nhóm quyền dựa vào ID.
- Request method: DELETE
- Header authorization: bearer token
- Request parameter:

    | Tham số| Kiểu | Mô tả|
    |--------|------|------|
    | ID | Integer | ID của nhóm quyền |

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: Delete role successfully
            - result: {}
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": [],
                          "message": "Delete role successfully"
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
    - Nếu không tìm thấy:
        - Header: status 404 Not Found
        - Body:
            - success: false
            - message: Role not found
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Role not found"
                    }
                ```
    - Nếu có lỗi truy vấn:
        - Header: status 500 Internal Server Error
        - Body:
            - success: false
            - message: "Query error"
            - data: Object chứa thông tin lỗi
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Query error",
                        "data": {}
                    }
                ```
                
#################################################################

Chức năng của module Category Management dùng để quản lý thông tin của danh mục sản phẩm. Gồm các chức năng: Lấy danh sách các danh mục sản phẩm, lấy thông tin của một danh mục sản phẩm cụ thể, thêm mới, cập nhật, xóa một danh mục sản phẩm.
## 1. Lấy tất cả danh mục sản phẩm `/api/v1/categories?[pamrams]`
- Description: Cho phép người dùng có quyền truy cập api có thể lấy tất cả danh mục sản phầm có trong database.
- Request method: GET
- Header authorization: bearer token
- Request parameter:

| Tham số| Kiểu | Mô tả|
|--------|------|------|
| offset | Integer | Vị trí bắt đầu của dòng dữ liệu cần lấy |
| limit | Integer | Số lượng dòng kết quả trả về |
| keyword | String | Từ khóa tìm kiếm theo tên. |
| sort| String | Sắp xếp kết quả theo thứ tự tăng dần/giảm dần dựa theo tên thuộc tính của khách hàng (Ví dụ: tăng dần theo id: sort_by=id, giảm dần theo id: sort_by=-id) |

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: ""
            - result:
      
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | totalRows | Integer| Tổng số dòng dữ liệu hiện có |
                | categories| Array| Danh sách danh mục sản phẩm lấy được |
                | message| String| Nội dung tin nhắn của truy vấn |
            
                - Mỗi phần tử trong mảng users bao gồm:
                    
                    | Tham số| Kiểu | Mô tả|
                    |--------|------|------|
                    | id | Integer| ID của danh mục |
                    | name | String| Tên danh mục sản phẩm |
                    | slug | String| Tên không dấu |
                    | icon| String| Đường dẫn đến icon |
                    | is_show| String| Trạng thái ẩn/hiện |
                    | seo_title| String| Tiêu đề của danh mục sản phẩm |
                    | seo_description| String| Mô tả của danh mục sản phẩm |
                    | parent | Object| Thông tin của danh mục sản phẩm cha |
                    | created_at| String| Ngày giờ khi tạo danh mục sản phẩm |
                    | updated_at| String| Ngày giờ khi cập nhật danh mục sản phẩm |
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": { 
                              "totalRows": 1,
                              "categories": [
                                  {
                                      "id": 1,
                                      "name": "Điện lạnh",
                                      "slug": "dien-lanh",
                                      "icon": "http://ecommerce2018.local/public/images/categories/icons/dien-lanh.png",
                                      "is_show": 1,
                                      "seo_title": "",
                                      "seo_description": "",
                                      "created_at": "2018-09-27 21:59:25",
                                      "updated_at": "2018-09-27 21:59:26",
                                      "parent": null
                                  }
                              ]
                          }
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
    - Nếu tham số không hợp lệ:
        - Header: status 422 Unprocessable Entity
        - Body:
            - success: false
            - message: "Validate error"
            - data: Object chứa thông tin validate không hợp lệ
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Validate error",
                        "data": {
                            "limit": [
                                "The limit must be an integer."
                            ]
                        }
                    }
                ```
    - Nếu có lỗi truy vấn:
        - Header: status 500 Internal Server Error
        - Body:
            - success: false
            - message: "Query error"
            - data: Object chứa thông tin lỗi
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Query error",
                        "data": {
                            "errorInfo": [
                                "You have an error in your SQL syntax"
                            ]
                        }
                    }
                ```
                
## 2. Lấy thông tin của một danh mục sản phẩm `/api/v1/catogories/{ID}`
- Description: Cho phép người dùng có quyền truy cập api có thể lấy thông tin của một danh mục sản phẩm dựa vào ID.
- Request method: GET
- Header authorization: bearer token
- Request parameter:

| Tham số| Kiểu | Mô tả|
|--------|------|------|
| ID | Integer | ID của danh mục sản phẩm |

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: ""
            - result:
            
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | id | Integer| ID của danh mục |
                | name | String| Tên danh mục sản phẩm |
                | slug | String| Tên không dấu |
                | icon| String| Đường dẫn đến icon |
                | is_show| String| Trạng thái ẩn/hiện |
                | seo_title| String| Tiêu đề của danh mục sản phẩm |
                | seo_description| String| Mô tả của danh mục sản phẩm |
                | parent | Object| Thông tin của danh mục sản phẩm cha |
                | created_at| String| Ngày giờ khi tạo danh mục sản phẩm |
                | updated_at| String| Ngày giờ khi cập nhật danh mục sản phẩm |
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": {  
                             "id": 1,
                             "name": "Điện lạnh",
                             "slug": "dien-lanh",
                             "icon": "http://ecommerce2018.local/public/images/categories/icons/dien-lanh.png",
                             "is_show": 1,
                             "seo_title": "",
                             "seo_description": "",
                             "created_at": "2018-09-27 21:59:25",
                             "updated_at": "2018-09-27 21:59:26",
                             "parent": null
                          },
                           "message": "",
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
    - Nếu không tìm thấy:
        - Header: status 404 Not Found
        - Body:
            - success: false
            - message: "Not found"
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Not found"
                    }
                ```
## 3. Thêm một danh mục sản phẩm `/api/v1/categories`
- Description: Cho phép người dùng có quyền truy cập api có thể thêm mới một danh mục sản phẩm.
- Request method: POST
- Header authorization: bearer token
- Request parameter:
    - Body

        | Tham số| Kiểu | Bắt buộc | Mô tả|
        |--------|------|------|------|
        | name | String | Có | Tên danh mục sản phẩm |
        | slug | String| Có | Tên không dấu |
        | icon| File| Không | Hình icon |
        | is_show| String| Không | Trạng thái ẩn/hiện |
        | seo_title| String| Không | Tiêu đề của danh mục sản phẩm |
        | seo_description| Không | String| Mô tả của danh mục sản phẩm |
        | parent_id | Integer| Không | ID của danh mục cha |

- Response result:
    - Nếu thành công:
        - Header: status 201 Created
        - Body:
            - success: true
            - message: Create category successfully
            - result:
      
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | id | Integer| ID của danh mục sản phẩm |
                | name | String| Tên danh mục sản phẩm |
                | slug | String| Tên không dấu |
                | icon| String| Đường dẫn đến icon |
                | is_show| String| Trạng thái ẩn/hiện |
                | seo_title| String| Tiêu đề của danh mục sản phẩm |
                | seo_description| String| Mô tả của danh mục sản phẩm |
                | parent | Object| Thông tin của danh mục sản phẩm cha |
                | created_at| String| Ngày giờ khi tạo danh mục sản phẩm |
                | updated_at| String| Ngày giờ khi cập nhật danh mục sản phẩm |
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": {  
                               "id": 1,
                               "name": "Điện lạnh",
                               "slug": "dien-lanh",
                               "icon": "http://ecommerce2018.local/public/images/categories/icons/dien-lanh.png",
                               "is_show": 1,
                               "seo_title": "",
                               "seo_description": "",
                               "created_at": "2018-09-27 21:59:25",
                               "updated_at": "2018-09-27 21:59:26",
                               "parent": null
                          },
                           "message": "Create category successfully"
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
    - Nếu tham số không hợp lệ:
        - Header: status 422 Unprocessable Entity
        - Body:
            - success: false
            - message: "Validate error"
            - data: Object chứa thông tin validate không hợp lệ
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Validate error",
                        "data": {
                            "email": [
                                "The slug has already been taken."
                            ]
                        }
                    }
                ```
    - Nếu có lỗi truy vấn:
        - Header: status 500 Internal Server Error
        - Body:
            - success: false
            - message: "Query error"
            - data: Object chứa thông tin lỗi
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Query error",
                        "data": {
                            "errorInfo": [
                                "You have an error in your SQL syntax"
                            ]
                        }
                    }
                ```
                
## 4. Cập nhật thông tin một khách hàng `/api/v1/categories/{ID}`
- Description: Cho phép người dùng có quyền truy cập api có thể cập nhật thông tin của một khách hàng dựa vào ID.
- Request method: POST
- Header authorization: bearer token
- Request parameter:

    | Tham số| Kiểu | Mô tả|
    |--------|------|------|
    | ID | Integer | ID của danh mục sản phẩm|
    - Body

        | Tham số| Kiểu | Bắt buộc | Mô tả|
        |--------|------|------|------|
        | name | String | Không | Tên danh mục sản phẩm |
        | slug | String| Không | Tên không dấu |
        | icon| File| Không | Hình icon |
        | is_show| String| Không | Trạng thái ẩn/hiện |
        | seo_title| String| Không | Tiêu đề của danh mục sản phẩm |
        | seo_description| Không | String| Mô tả của danh mục sản phẩm |
        | parent_id | Integer| Không | ID của danh mục cha |
        | _method | String| Có | Method: PUT |

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: Update category successfully
            - result:
      
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | id | Integer| ID của danh mục sản phẩm |
                | name | String| Tên danh mục sản phẩm |
                | slug | String| Tên không dấu |
                | icon| String| Đường dẫn đến icon |
                | is_show| String| Trạng thái ẩn/hiện |
                | seo_title| String| Tiêu đề của danh mục sản phẩm |
                | seo_description| String| Mô tả của danh mục sản phẩm |
                | parent | Object| Thông tin của danh mục sản phẩm cha |
                | created_at| String| Ngày giờ khi tạo danh mục sản phẩm |
                | updated_at| String| Ngày giờ khi cập nhật danh mục sản phẩm |
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": { 
                             "id": 1,
                             "name": "Điện lạnh",
                             "slug": "dien-lanh",
                             "icon": "http://ecommerce2018.local/public/images/categories/icons/dien-lanh.png",
                             "is_show": 1,
                             "seo_title": "",
                             "seo_description": "",
                             "created_at": "2018-09-27 21:59:25",
                             "updated_at": "2018-09-27 21:59:26",
                             "parent": null
                          },
                           "message": "Update category successfully"
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
    - Nếu tham số không hợp lệ:
        - Header: status 422 Unprocessable Entity
        - Body:
            - success: false
            - message: "Validate error"
            - data: Object chứa thông tin validate không hợp lệ
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Validate error",
                        "data": {
                            "password": [
                                "The slug has already been taken."
                            ]
                        }
                    }
                ```
    - Nếu có lỗi truy vấn:
        - Header: status 500 Internal Server Error
        - Body:
            - success: false
            - message: "Query error"
            - data: Object chứa thông tin lỗi
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Query error",
                        "data": {
                            "errorInfo": [
                                "Column 'name' cannot be null"
                            ]
                        }
                    }
                ```
                
## 5. Xóa một danh mục sản phẩm `/api/v1/categories/{ID}`
- Description: Cho phép người dùng có quyền truy cập api có thể xóa thông tin của một danh mục sản phẩm dựa vào ID.
- Request method: DELETE
- Header authorization: bearer token
- Request parameter:

    | Tham số| Kiểu | Mô tả|
    |--------|------|------|
    | ID | Integer | ID của danh mục sản phẩm |

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: Delete category successfully
            - result: {}
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": {},
                          "message": "Delete category successfully"
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
    - Nếu không tìm thấy:
        - Header: status 404 Not Found
        - Body:
            - success: false
            - message: Category not found
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Category not found"
                    }
                ```
    - Nếu có lỗi truy vấn:
        - Header: status 500 Internal Server Error
        - Body:
            - success: false
            - message: "Query error"
            - data: Object chứa thông tin lỗi
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Query error",
                        "data": {}
                    }
                ```
                
## 6. Lấy tất cả danh mục sản phẩm có trạng thái hiện `/api/v1/get-categories`
- Description: Cho phép người dùng có lấy tất cả danh mục sản phầm có trạng thái được hiển thị.
- Request method: GET
- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: ""
            - result: Danh sách các danh mục sản phẩm (Array), mỗi phần tử bao gồm
            
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | id | Integer| ID của danh mục |
                | name | String| Tên danh mục sản phẩm |
                | slug | String| Tên không dấu |
                | icon| String| Đường dẫn đến icon |
                | parent_id | Integer | ID của danh mục sản phẩm cha |
                
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result":  [
                                   {
                                      "id": 14,
                                      "name": "Máy Lạnh - Điều hòa",
                                      "slug": "may-lanh-dieu-hoa",
                                      "icon": "",
                                      "parent_id": 1
                                   }
                          ]
                       }
                ```

## 7. Lấy thông tin của một danh mục sản phẩm dựa vào tên không dấu`/api/v1/get-category?slug={slug}`
- Description: Cho phép người dùng có thể lấy thông tin của một danh mục sản phẩm dựa vào tên không dấu (slug) của một danh mục sản phẩm.
- Request method: GET
- Header authorization: bearer token
- Request parameter:

| Tham số| Kiểu | Mô tả|
|--------|------|------|
| slug| String| Tên không dấu của danh mục sản phẩm |

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: ""
            - result:
            
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | id | Integer| ID của danh mục |
                | name | String| Tên danh mục sản phẩm |
                | slug | String| Tên không dấu |
                | icon| String| Đường dẫn đến icon |
                | parent_id | Integer | ID của danh mục sản phẩm cha |
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": {  
                             "id": 5,
                             "name": "Gia dụng",
                             "slug": "gia-dung",
                             "icon": "http://ecommerce2018.local/public/images/categories/icons/gia-dung.png",
                             "parent_id": null
                          },
                           "message": "",
                       }
                ```

## 8. Lấy thông tin breadcrumbs của danh mục sản phẩm hiện tại `/api/v1/get-breadcrumb-product-list?slug={slug}`
- Description: Cho phép người dùng có thể lấy thông tin breadcrumbs của một danh mục sản phẩm dựa vào tên không dấu (slug) của một danh mục sản phẩm.
- Request method: GET
- Header authorization: bearer token
- Request parameter:

| Tham số| Kiểu | Mô tả|
|--------|------|------|
| slug| String| Tên không dấu của danh mục sản phẩm |

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: ""
            - result: Danh sách các breadcrumb (Array), mỗi phần tử bao gồm:
            
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | id | Integer| ID của danh mục |
                | name | String| Tên danh mục sản phẩm |
                | slug | String| Tên không dấu |
                | parent_id | Integer | ID của danh mục sản phẩm cha |
                | seo_title | String | Tiêu đề của danh mục sản phẩm (chỉ có ở phần tử cuối của mảng) |
                | seo_description | String | Mô tả của danh mục sản phẩm (chỉ có ở phần tử cuối của mảng) |
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": [
                              {  
                                 "id": 5,
                                 "name": "Gia dụng",
                                 "slug": "gia-dung",
                                 "parent_id": null,
                                 "seo_title": "",
                                 "seo_description": ""
                              }
                          ],
                           "message": "",
                       }
                ```
                
###########################################################################

Chức năng của module User Authentication dùng để quản lý Xác thực nhân viên. Gồm các chức năng: Đăng nhập, đăng xuất, lấy thông tin của một nhân viên đang đăng nhập, khôi phục mật khẩu.
## 1. Đăng nhập `/api/v1/auth/login`
- Description: Cho phép người dùng lấy được token login khi đăng nhập thành công.
- Request method: POST
- Request parameter:

| Tham số| Kiểu | Mô tả|
|--------|------|------|
| email| String| Địa chỉ email của nhân viên |
| password| String| Mật khẩu |

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: login success'
            - result:
            
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | token | String | Mã token dùng để xác thực người dùng |
                | token_type | String| Kiểu xác thực token |
            - Ví dụ:
                ```json 
                        {
                            "success": true,
                            "result": {
                                "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9lY29tbWVyY2UyMDE4LmxvY2FsXC9hcGlcL3YxXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU0NDUwMTMzMCwibmJmIjoxNTQ0NTAxMzMwLCJqdGkiOiJLbWdDajg2d1hGZWJwckl6Iiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.B_GYZAJSDy-jFyn69l-ThwLu6RUojdxA9_FIvmN1T_Y",
                                "token_type": "bearer"
                            },
                            "message": "login success"
                        }
                ```
    - Nếu đăng nhập thất bại:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: Unauthorized user
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthorized user"
                    }
                ```
                
    - Nếu tham số không hợp lệ:
        - Header: status 422 Unprocessable Entity
        - Body:
            - success: false
            - message: Validate error
            - data: Object chứa thông tin validate không hợp lệ
            - Ví dụ:
                ```json 
                    {
                        "success": false,
                        "message": "Validation Error",
                        "data": {
                            "email": [
                                "The email field is required."
                            ]
                        }
                    }
                ```
                
## 2. Đăng xuất `/api/v1/auth/logout`
- Description: Cho phép người dùng đăng xuất.
- Request method: GET
- Header authorization: bearer token

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: logout success
            - result: []
            - Ví dụ:
                ```json 
                {
                    "success": true,
                    "result": [],
                    "message": "logout success"
                }
                ```
    - Nếu đăng xuất thất bại:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: Unauthorized user
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthorized user"
                    }
                ```

## 3. Lấy thông tin của một nhân viên đã đăng nhập `/api/v1/auth/user`
- Description: Cho phép người dùng có có thể lấy thông tin của mình khi đã đăng nhập.
- Request method: GET
- Header authorization: bearer token

- Response result:
    - Nếu thành công:
        - Header: status 200 OK
        - Body:
            - success: true
            - message: Get user successfully
            - result:
            
                | Tham số| Kiểu | Mô tả|
                |--------|------|------|
                | id | Integer| ID của user |
                | name| String| Họ và tên của user|
                | email| String| Email của user |
                | avatar| String| Đường dẫn đến hình đại diện của user |
                | phone| String| Số điện thoại của user |
                | address| String| Địa chỉ của user |
                | gender| Integer| Giới tính của user (0: Female, 1: Male) |
                | birthday| String| Ngày sinh của user |
                | role | Object| Đối tượng chứa thông tin quyền hạn của user |
                | created_at| String| Ngày giờ khi tạo user |
                | updated_at| String| Ngày giờ khi cập nhật của user |
            - Ví dụ:
                ```json 
                        {
                          "success": true,
                          "result": {
                            "id": 1,
                            "name": "Le Qui Nhat",
                            "email": "quinhatpy@gmail.com",
                            "is_active": 0,
                            "avatar": "http://ecommerce2018.local/public/images/users/1539948386-le-qui-nhat-nhatle.jpg",
                            "phone": 968403428,
                            "address": "Phú Yên",
                            "gender": 1,
                            "birthday": "1996-10-24",
                            "created_at": "2018-10-19 10:09:33",
                            "updated_at": "2018-12-08 15:21:24",
                            "role": {
                                "id": 1,
                                "name": "Super Admin",
                                "permissions": {},
                                "created_at": "2018-10-19 10:01:32",
                                "updated_at": "2018-10-19 18:16:04"
                            }
                        }
                       }
                ```
    - Nếu không có quyền truy cập api:
        - Header: status 401 Unauthorized
        - Body:
            - success: false
            - message: "Unauthenticated."
            - Ví dụ:
                ```json
                    {
                        "success": false,
                        "message": "Unauthenticated."
                    }
                ```
                
